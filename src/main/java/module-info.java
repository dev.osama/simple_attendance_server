module SimpleAttendanceServer {
    requires javafx.controls;
    requires javafx.graphics;
    requires javafx.fxml;
    requires javafx.base;
    requires javafx.media;
    requires java.sql;


    requires spring.context;
    requires spring.boot.autoconfigure;
    requires spring.boot;
    requires spring.web;
    requires spring.core;
    requires spring.beans;
    requires spring.data.commons;
    requires spring.boot.starter;

    requires java.annotation;
    requires com.fasterxml.jackson.module.paramnames;
    requires com.fasterxml.jackson.databind;
    requires spring.messaging;
    requires spring.websocket;

//    exports sa.osama_alharbi.sas.ws;
    exports sa.osama_alharbi.sas.controller;
    exports sa.osama_alharbi.sas.ws.controller;
    exports sa.osama_alharbi.sas.ws.model;
    opens sa.osama_alharbi.sas.ws.config;
    opens sa.osama_alharbi.sas.ws;
//
    opens sa.osama_alharbi.sas.main;
}