package sa.osama_alharbi.sas.ws.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.stereotype.Controller;
import sa.osama_alharbi.sas.inc.Inc;
import sa.osama_alharbi.sas.model.AttendanceModel;
import sa.osama_alharbi.sas.model.ClassesModel;
import sa.osama_alharbi.sas.model.SectionModel;
import sa.osama_alharbi.sas.model.StudentsSectionModel;
import sa.osama_alharbi.sas.ws.config.WsPrincipal;
import sa.osama_alharbi.sas.ws.model.AttendanceDetailsJsonModel;
import sa.osama_alharbi.sas.ws.model.ClassesJsonModel;
import sa.osama_alharbi.sas.ws.model.SectionJsonModel;
import sa.osama_alharbi.sas.ws.model.StudentsJsonModel;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;

@Controller
public class Attendance {
    @Autowired
    public SimpMessagingTemplate messagingTemplate;

    @MessageMapping("/login")
    @SendToUser("/login")
    public String login(@Payload String pass, SimpMessageHeaderAccessor headerAccessor) {
        WsPrincipal user = (WsPrincipal)headerAccessor.getUser();
        if(pass.equals(Inc.model.setting.get.get().getSetting_serverPassword())){
            user.setIsLogin(true);
            return "true";
        }else{
            return "serverPassword_error";
        }
    }

    @MessageMapping("/allClasses")
    @SendToUser("/allClasses")
    public ArrayList<ClassesJsonModel> getAllClasses(SimpMessageHeaderAccessor headerAccessor) {
        WsPrincipal user = (WsPrincipal)headerAccessor.getUser();
        if(user.isIsLogin()){
            ArrayList<ClassesJsonModel> arr = new ArrayList<>();
            for (ClassesModel itm : Inc.model.classes.get.getAllNotDeleted()) {
                arr.add(new ClassesJsonModel(itm));
            }
            return arr;
        }else{
            messagingTemplate.convertAndSendToUser(user.getName(),"/login","serverPassword_error");
            return null;
        }
    }

    @MessageMapping("/allSections")
    @SendToUser("/allSections")
    public ArrayList<SectionJsonModel> getAllSections(@Payload int classes_id,SimpMessageHeaderAccessor headerAccessor) {
        WsPrincipal user = (WsPrincipal)headerAccessor.getUser();
        if(user.isIsLogin()){
            ArrayList<SectionJsonModel> arr = new ArrayList<>();
            for (SectionModel itm : Inc.model.section.get.getAllByClassses_idNotDeleted(classes_id)) {
                arr.add(new SectionJsonModel(itm));
            }
            return arr;
        }else{
            messagingTemplate.convertAndSendToUser(user.getName(),"/login","serverPassword_error");
            return null;
        }
    }

    @MessageMapping("/allStudents")
    @SendToUser("/allStudents")
    public ArrayList<StudentsJsonModel> getAllStudents(@Payload SectionJsonModel sectionJsonModel, SimpMessageHeaderAccessor headerAccessor) {
        WsPrincipal user = (WsPrincipal)headerAccessor.getUser();
        if(user.isIsLogin()){
            SectionModel appSectionModel = Inc.model.section.get.get(sectionJsonModel.section_id);
            if(appSectionModel != null){
                if(appSectionModel.getSection_password().equals(sectionJsonModel.section_password)){
                    ArrayList<StudentsJsonModel> arr = new ArrayList<>();
                    for (StudentsSectionModel itm : Inc.model.studentsSection.get.getAllBySection_idNotDeleted(sectionJsonModel.section_id)) {
                        arr.add(new StudentsJsonModel(itm.getStudentsModel()));
                    }
                    return arr;
                }else{
                    messagingTemplate.convertAndSendToUser(user.getName(),"/allStudents","password_error");
                    return null;
                }
            }else{
                messagingTemplate.convertAndSendToUser(user.getName(),"/allStudents","section_id_error");
                return null;
            }
        }else{
            messagingTemplate.convertAndSendToUser(user.getName(),"/login","serverPassword_error");
            return null;
        }

    }

    @MessageMapping("/addAttendances")
    @SendToUser("/addAttendances")
    public boolean addAttendances(@Payload AttendanceDetailsJsonModel attendanceDetailsJsonModel, SimpMessageHeaderAccessor headerAccessor) {
        WsPrincipal user = (WsPrincipal)headerAccessor.getUser();
        if(user.isIsLogin()){
            SectionModel appSectionModel = Inc.model.section.get.get(attendanceDetailsJsonModel.sectionModel.section_id);
            if(appSectionModel != null){
                if(appSectionModel.getSection_password().equals(attendanceDetailsJsonModel.sectionModel.section_password)){
                    ArrayList<AttendanceModel> finalArr = new ArrayList<>();
                    ArrayList<StudentsSectionModel> tempArr = Inc.model.studentsSection.get.getAllBySection_idNotDeleted(attendanceDetailsJsonModel.sectionModel.section_id);
                    HashMap<Integer,StudentsSectionModel> studentsSectionModelHashMap = new HashMap<>();
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
                    LocalDateTime formatDateTime = LocalDateTime.parse(LocalDateTime.now().format(formatter), formatter);
                    for (StudentsSectionModel i:tempArr) {
                        studentsSectionModelHashMap.put(i.getStudents_id(),i);
                    }
                    for (int id:attendanceDetailsJsonModel.arrPresentStudents_ids) {
                        finalArr.add(new AttendanceModel(0,formatDateTime , false, false, studentsSectionModelHashMap.get(id).getStudents_section_id()));
                    }
                    for (int id:attendanceDetailsJsonModel.arrLateStudents_ids) {
                        finalArr.add(new AttendanceModel(0,formatDateTime , false, true, studentsSectionModelHashMap.get(id).getStudents_section_id()));
                    }
                    for (int id:attendanceDetailsJsonModel.arrAbsenStudents_ids) {
                        finalArr.add(new AttendanceModel(0,formatDateTime , true, false, studentsSectionModelHashMap.get(id).getStudents_section_id()));
                    }
                    Inc.action.attendance.add.add(finalArr);
                }else{
                }
            }else{
            }
        }else{
            messagingTemplate.convertAndSendToUser(user.getName(),"/login","serverPassword_error");
        }
        return true;
    }
}
