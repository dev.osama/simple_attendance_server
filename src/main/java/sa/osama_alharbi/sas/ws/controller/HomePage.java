package sa.osama_alharbi.sas.ws.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomePage {
    @GetMapping({"","/"})
    public String index() {
        return "<center>أهلا وسهلا في برنامج التحظير السهل</center>";
    }

}
