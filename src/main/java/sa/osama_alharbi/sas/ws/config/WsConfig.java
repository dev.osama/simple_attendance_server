package sa.osama_alharbi.sas.ws.config;

import javafx.application.Platform;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.messaging.converter.MessageConverter;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.messaging.SessionConnectEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;
import org.springframework.web.socket.messaging.SessionSubscribeEvent;
import org.springframework.web.socket.server.support.DefaultHandshakeHandler;

import java.security.Principal;
import java.util.List;
import java.util.Map;

@Configuration
@EnableWebSocketMessageBroker
public class WsConfig implements WebSocketMessageBrokerConfigurer{

	@Override
	public boolean configureMessageConverters(List<MessageConverter> messageConverters) {
		return false;
	}

	@Override
	public void registerStompEndpoints(StompEndpointRegistry registry) {
		registry.addEndpoint("/ws/")
		.setHandshakeHandler(new AssignPrincipalHandshakeHandler());
	}
	@Override
	public void configureMessageBroker(MessageBrokerRegistry registry) {
		ThreadPoolTaskScheduler te = new ThreadPoolTaskScheduler();
        te.setPoolSize(1);
        te.setThreadNamePrefix("wss-heartbeat-thread-");
        te.initialize();
		String[] simpleBrokers = {
				"/login",
				"/allClasses",
				"/allSections",
				"/allStudents",
				"/addAttendances"
		};
		registry.enableSimpleBroker(simpleBrokers)
				.setTaskScheduler(te)
				.setHeartbeatValue(new long[] {10000, 10000});

		registry.setApplicationDestinationPrefixes("/s");
		registry.setUserDestinationPrefix("/c");
	}
	
	public class AssignPrincipalHandshakeHandler extends DefaultHandshakeHandler {
        private static final String ATTR_PRINCIPAL = "__principal__";
        private int num = 0;
        @Override
        protected Principal determineUser(ServerHttpRequest request, WebSocketHandler wsHandler,
                                          Map<String, Object> attributes) {
        	
        	String id;
            if (!attributes.containsKey(ATTR_PRINCIPAL)) {
            	id = generateRandomUsername();
            	 attributes.put(ATTR_PRINCIPAL,id);
                
            }else {
            	id = (String)attributes.get(ATTR_PRINCIPAL);
            }
            return new WsPrincipal(id);
//			return null;
        }

        private synchronized String generateRandomUsername() {
        	num++;
            return ""+num;
        }
        
        public Principal getWsPrincipal(){
        	return null;
        }
        
    }
	
	@Autowired
	public SimpMessagingTemplate messagingTemplate;
	
    @EventListener
    public void handleSubscribeEvent(SessionSubscribeEvent event) {
    	System.out.println("<==> handleSubscribeEvent: username="+((WsPrincipal)event.getUser()).getName());
    	System.out.println("<==> handleSubscribeEvent: !!!! ="+event.toString());
    }

    @EventListener
    public void handleConnectEvent(SessionConnectEvent event) {
    	WsPrincipal us = (WsPrincipal)event.getUser();
    	System.out.println("===> handleConnectEvent: username="+us.getName());
    }

    @EventListener
    public void handleDisconnectEventt(SessionDisconnectEvent event) {
    	WsPrincipal us = (WsPrincipal)event.getUser();
    	String name = us.getName();
    	System.out.println("<=== handleDisconnectEvent: username="+us.getName());
    }
}
