package sa.osama_alharbi.sas.ws.config;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;

import java.security.Principal;

public class WsPrincipal implements Principal{
	private BooleanProperty isLogin;
	private String session_id;
	public WsPrincipal(String session_id){
		this.session_id = session_id;
		this.isLogin = new SimpleBooleanProperty(false);
	}
	
	@Override
	public String getName() {
		return this.session_id;
	}
	public String getSession_id() {
		return session_id;
	}
	public void setSession_id(String session_id) {
		this.session_id = session_id;
	}

	public final BooleanProperty isLoginProperty() {
		return this.isLogin;
	}
	public final boolean isIsLogin() {
		return this.isLoginProperty().get();
	}
	public final void setIsLogin(final boolean isLogin) {
		this.isLoginProperty().set(isLogin);
	}

}
