package sa.osama_alharbi.sas.ws.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class AttendanceDetailsJsonModel {
    public ClassesJsonModel classesModel;
    public SectionJsonModel sectionModel;
    public String date;
    public String time;
    public int[] arrPresentStudents_ids;
    public int[] arrLateStudents_ids;
    public int[] arrAbsenStudents_ids;

    public AttendanceDetailsJsonModel(){}

    public AttendanceDetailsJsonModel(ClassesJsonModel classesModel, SectionJsonModel sectionModel, String date, String time, int[] arrPresentStudents_ids, int[] arrLateStudents_ids, int[] arrAbsenStudents_ids) {
        this.classesModel = classesModel;
        this.sectionModel = sectionModel;
        this.date = date;
        this.time = time;
        this.arrPresentStudents_ids = arrPresentStudents_ids;
        this.arrLateStudents_ids = arrLateStudents_ids;
        this.arrAbsenStudents_ids = arrAbsenStudents_ids;
    }

    public ClassesJsonModel getClassesModel() {
        return classesModel;
    }

    public void setClassesModel(ClassesJsonModel classesModel) {
        this.classesModel = classesModel;
    }

    public SectionJsonModel getSectionModel() {
        return sectionModel;
    }

    public void setSectionModel(SectionJsonModel sectionModel) {
        this.sectionModel = sectionModel;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int[] getArrPresentStudents_ids() {
        return arrPresentStudents_ids;
    }

    public void setArrPresentStudents_ids(int[] arrPresentStudents_ids) {
        this.arrPresentStudents_ids = arrPresentStudents_ids;
    }

    public int[] getArrLateStudents_ids() {
        return arrLateStudents_ids;
    }

    public void setArrLateStudents_ids(int[] arrLateStudents_ids) {
        this.arrLateStudents_ids = arrLateStudents_ids;
    }

    public int[] getArrAbsenStudents_ids() {
        return arrAbsenStudents_ids;
    }

    public void setArrAbsenStudents_ids(int[] arrAbsenStudents_ids) {
        this.arrAbsenStudents_ids = arrAbsenStudents_ids;
    }
}
