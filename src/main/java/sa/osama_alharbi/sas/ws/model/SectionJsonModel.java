package sa.osama_alharbi.sas.ws.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import sa.osama_alharbi.sas.model.SectionModel;

public class SectionJsonModel {
    @JsonProperty
    public int section_id;
    @JsonProperty
    public String section_name;
    @JsonProperty
    public String section_password;
    @JsonProperty
    public int class_id;

    public SectionJsonModel(int section_id, String section_name, String section_password, int class_id) {
        this.section_id = section_id;
        this.section_name = section_name;
        this.section_password = section_password;
        this.class_id = class_id;
    }
    public SectionJsonModel(SectionModel sectionModel) {
        this.section_id = sectionModel.getSection_id();
        this.section_name = sectionModel.getSection_name();
        this.section_password = "";
        this.class_id = sectionModel.getClass_id();
    }
    public SectionJsonModel(){}

}
