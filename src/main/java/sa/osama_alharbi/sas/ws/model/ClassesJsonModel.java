package sa.osama_alharbi.sas.ws.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import sa.osama_alharbi.sas.model.ClassesModel;

public class ClassesJsonModel {

    @JsonProperty
    public int class_id;
    @JsonProperty
    public String class_name;

    public ClassesJsonModel(int class_id, String class_name) {
        this.class_id = class_id;
        this.class_name = class_name;
    }
    public ClassesJsonModel(ClassesModel classesModel) {
        this.class_id = classesModel.getClass_id();
        this.class_name = classesModel.getClass_name();
    }
    public ClassesJsonModel(){}
    public int getClass_id() {
        return class_id;
    }
    public void setClass_id(int class_id) {
        this.class_id = class_id;
    }
    public String getClass_name() {
        return class_name;
    }
    public void setClass_name(String class_name) {
        this.class_name = class_name;
    }
}
