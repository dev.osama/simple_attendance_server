package sa.osama_alharbi.sas.ws.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import sa.osama_alharbi.sas.model.StudentsModel;

public class StudentsJsonModel {
    @JsonProperty
    public int students_id;
    @JsonProperty
    public String students_name;

    public StudentsJsonModel(int students_id, String students_name) {
        this.students_id = students_id;
        this.students_name = students_name;
    }
    public StudentsJsonModel(StudentsModel studentsModel){
        this.students_id = studentsModel.getStudents_id();
        this.students_name = studentsModel.getStudents_name();
    }
    public StudentsJsonModel(){}
}
