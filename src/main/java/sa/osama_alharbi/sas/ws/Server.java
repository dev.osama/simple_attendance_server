package sa.osama_alharbi.sas.ws;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.web.bind.annotation.GetMapping;
import sa.osama_alharbi.sas.inc.Inc;

import java.util.HashMap;

@SpringBootApplication
public class Server {

    private ConfigurableApplicationContext wsConfig = null;

    public Server() {
    }


    public void start(int port) {
        stop();
        Inc.model.server.set.setRunningType(Type.SERVER_TRY_START);
        HashMap<String, Object> props = new HashMap<>();
        props.put("server.port", port);
        try{
            wsConfig = new SpringApplicationBuilder()
                    .sources(Server.class)
                    .properties(props)
                    .run();
            Inc.model.server.set.setRunningType(Type.SERVER_STARTED);
        }catch (Exception e){
            stop();
        }
    }



    public void stop() {
        if(wsConfig == null) {
            Inc.model.server.set.setRunningType(Type.SERVER_CLOSED);
            return;
        }
        Inc.model.server.set.setRunningType(Type.SERVER_TRY_CLOSE);
        wsConfig.close();
        wsConfig = null;
        Inc.model.server.set.setRunningType(Type.SERVER_CLOSED);
        System.gc();
    }


    @GetMapping("")
    public String get() {
        return "ASDASDASDASDASDASSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS";
    }




    public class Type{
        public static final int SERVER_TRY_START = 1;
        public static final int SERVER_STARTED 	= 2;
        public static final int SERVER_TRY_CLOSE = 3;
        public static final int SERVER_CLOSED 	= 4;
    }
}
