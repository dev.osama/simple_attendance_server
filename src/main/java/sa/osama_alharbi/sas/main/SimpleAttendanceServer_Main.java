package sa.osama_alharbi.sas.main;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import sa.osama_alharbi.sas.inc.Inc;
import sa.osama_alharbi.sas.view.Index;

import java.util.HashMap;

public class SimpleAttendanceServer_Main extends Application {

    @Override
    public void start(Stage primaryStage){
        Inc.start();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
