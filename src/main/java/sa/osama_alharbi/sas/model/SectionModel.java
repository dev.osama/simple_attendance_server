package sa.osama_alharbi.sas.model;

import javafx.beans.property.*;
import sa.osama_alharbi.sas.inc.Inc;

public class SectionModel {
    private IntegerProperty section_id;
    private StringProperty section_name;
    private StringProperty section_password;
    private BooleanProperty section_isDelete;
    private IntegerProperty class_id;

    public SectionModel(int section_id, String section_name, String section_password, boolean section_isDelete, int class_id) {
        this.section_id = new SimpleIntegerProperty(section_id);
        this.section_name = new SimpleStringProperty(section_name);
        this.section_password = new SimpleStringProperty(section_password);
        this.section_isDelete = new SimpleBooleanProperty(section_isDelete);
        this.class_id = new SimpleIntegerProperty(class_id);
    }

    public int getSection_id() {
        return section_id.get();
    }

    public IntegerProperty section_idProperty() {
        return section_id;
    }

    public void setSection_id(int section_id) {
        this.section_id.set(section_id);
    }

    public String getSection_name() {
        return section_name.get();
    }

    public StringProperty section_nameProperty() {
        return section_name;
    }

    public void setSection_name(String section_name) {
        this.section_name.set(section_name);
    }

    public String getSection_password() {
        return section_password.get();
    }

    public StringProperty section_passwordProperty() {
        return section_password;
    }

    public void setSection_password(String section_password) {
        this.section_password.set(section_password);
    }

    public boolean isSection_isDelete() {
        return section_isDelete.get();
    }

    public BooleanProperty section_isDeleteProperty() {
        return section_isDelete;
    }

    public void setSection_isDelete(boolean section_isDelete) {
        this.section_isDelete.set(section_isDelete);
    }

    public int getClass_id() {
        return class_id.get();
    }

    public IntegerProperty class_idProperty() {
        return class_id;
    }

    public void setClass_id(int class_id) {
        this.class_id.set(class_id);
    }

    public ClassesModel getClassesModel() {
        return Inc.model.classes.get.get(getClass_id());
    }
}
