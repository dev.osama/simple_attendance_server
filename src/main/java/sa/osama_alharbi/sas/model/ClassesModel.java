package sa.osama_alharbi.sas.model;

import javafx.beans.property.*;

public class ClassesModel {
    private IntegerProperty class_id;
    private StringProperty class_name;
    private BooleanProperty class_isDelete;

    public ClassesModel(int class_id, String class_name, boolean class_isDelete) {
        this.class_id = new SimpleIntegerProperty(class_id);
        this.class_name = new SimpleStringProperty(class_name);
        this.class_isDelete = new SimpleBooleanProperty(class_isDelete);
    }

    public int getClass_id() {
        return class_id.get();
    }

    public IntegerProperty class_idProperty() {
        return class_id;
    }

    public void setClass_id(int class_id) {
        this.class_id.set(class_id);
    }

    public String getClass_name() {
        return class_name.get();
    }

    public StringProperty class_nameProperty() {
        return class_name;
    }

    public void setClass_name(String class_name) {
        this.class_name.set(class_name);
    }

    public boolean isClass_isDelete() {
        return class_isDelete.get();
    }

    public BooleanProperty class_isDeleteProperty() {
        return class_isDelete;
    }

    public void setClass_isDelete(boolean class_isDelete) {
        this.class_isDelete.set(class_isDelete);
    }
}
