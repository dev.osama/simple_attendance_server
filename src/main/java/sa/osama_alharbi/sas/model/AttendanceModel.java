package sa.osama_alharbi.sas.model;

import javafx.beans.property.*;
import javafx.beans.value.ObservableValue;
import sa.osama_alharbi.sas.inc.Inc;

import java.time.LocalDateTime;

public class AttendanceModel {
    private IntegerProperty attendance_id;
    private ObjectProperty<LocalDateTime> attendance_dateTime;
    private BooleanProperty attendance_isAbsent;
    private BooleanProperty attendance_isLate;
    private IntegerProperty students_section_id;

    public AttendanceModel(int attendance_id, LocalDateTime attendance_dateTime, boolean attendance_isAbsent, boolean attendance_isLate, int students_section_id) {
        this.attendance_id = new SimpleIntegerProperty(attendance_id);
        this.attendance_dateTime = new SimpleObjectProperty<LocalDateTime>(attendance_dateTime);
        this.attendance_isAbsent = new SimpleBooleanProperty(attendance_isAbsent);
        this.attendance_isLate = new SimpleBooleanProperty(attendance_isLate);
        this.students_section_id = new SimpleIntegerProperty(students_section_id);
    }

    public int getAttendance_id() {
        return attendance_id.get();
    }

    public IntegerProperty attendance_idProperty() {
        return attendance_id;
    }

    public void setAttendance_id(int attendance_id) {
        this.attendance_id.set(attendance_id);
    }

    public LocalDateTime getAttendance_dateTime() {
        return attendance_dateTime.get();
    }

    public ObjectProperty<LocalDateTime> attendance_dateTimeProperty() {
        return attendance_dateTime;
    }

    public void setAttendance_dateTime(LocalDateTime attendance_dateTime) {
        this.attendance_dateTime.set(attendance_dateTime);
    }

    public boolean isAttendance_isAbsent() {
        return attendance_isAbsent.get();
    }

    public BooleanProperty attendance_isAbsentProperty() {
        return attendance_isAbsent;
    }

    public void setAttendance_isAbsent(boolean attendance_isAbsent) {
        this.attendance_isAbsent.set(attendance_isAbsent);
    }

    public boolean isAttendance_isLate() {
        return attendance_isLate.get();
    }

    public BooleanProperty attendance_isLateProperty() {
        return attendance_isLate;
    }

    public void setAttendance_isLate(boolean attendance_isLate) {
        this.attendance_isLate.set(attendance_isLate);
    }

    public int getStudents_section_id() {
        return students_section_id.get();
    }

    public IntegerProperty students_section_idProperty() {
        return students_section_id;
    }

    public void setStudents_section_id(int students_section_id) {
        this.students_section_id.set(students_section_id);
    }

    public StudentsSectionModel getStudentsSectionModel() {
        return Inc.model.studentsSection.get.get(getStudents_section_id());
    }

    public StudentsModel getStudentsModel() {
        return getStudentsSectionModel().getStudentsModel();
    }
}
