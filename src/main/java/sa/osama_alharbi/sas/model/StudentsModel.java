package sa.osama_alharbi.sas.model;

import javafx.beans.property.*;

public class StudentsModel {
    private IntegerProperty students_id;
    private StringProperty students_name;
    private BooleanProperty students_isDelete;

    public StudentsModel(int students_id, String students_name, boolean students_isDelete) {
        this.students_id = new SimpleIntegerProperty(students_id);
        this.students_name = new SimpleStringProperty(students_name);
        this.students_isDelete = new SimpleBooleanProperty(students_isDelete);
    }

    public int getStudents_id() {
        return students_id.get();
    }

    public IntegerProperty students_idProperty() {
        return students_id;
    }

    public void setStudents_id(int students_id) {
        this.students_id.set(students_id);
    }

    public String getStudents_name() {
        return students_name.get();
    }

    public StringProperty students_nameProperty() {
        return students_name;
    }

    public void setStudents_name(String students_name) {
        this.students_name.set(students_name);
    }

    public boolean isStudents_isDelete() {
        return students_isDelete.get();
    }

    public BooleanProperty students_isDeleteProperty() {
        return students_isDelete;
    }

    public void setStudents_isDelete(boolean students_isDelete) {
        this.students_isDelete.set(students_isDelete);
    }
}
