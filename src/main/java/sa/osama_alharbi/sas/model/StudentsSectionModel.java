package sa.osama_alharbi.sas.model;

import javafx.beans.property.*;
import sa.osama_alharbi.sas.inc.Inc;

public class StudentsSectionModel {
    private IntegerProperty students_section_id;
    private BooleanProperty students_section_isDelete;
    private IntegerProperty students_id;
    private IntegerProperty section_id;

    public StudentsSectionModel(int students_section_id, boolean students_section_isDelete, int students_id, int section_id) {
        this.students_section_id = new SimpleIntegerProperty(students_section_id);
        this.students_section_isDelete = new SimpleBooleanProperty(students_section_isDelete);
        this.students_id = new SimpleIntegerProperty(students_id);
        this.section_id = new SimpleIntegerProperty(section_id);
    }

    public int getStudents_section_id() {
        return students_section_id.get();
    }

    public IntegerProperty students_section_idProperty() {
        return students_section_id;
    }

    public void setStudents_section_id(int students_section_id) {
        this.students_section_id.set(students_section_id);
    }

    public boolean isStudents_section_isDelete() {
        return students_section_isDelete.get();
    }

    public BooleanProperty students_section_isDeleteProperty() {
        return students_section_isDelete;
    }

    public void setStudents_section_isDelete(boolean students_section_isDelete) {
        this.students_section_isDelete.set(students_section_isDelete);
    }

    public int getStudents_id() {
        return students_id.get();
    }

    public IntegerProperty students_idProperty() {
        return students_id;
    }

    public void setStudents_id(int students_id) {
        this.students_id.set(students_id);
    }

    public int getSection_id() {
        return section_id.get();
    }

    public IntegerProperty section_idProperty() {
        return section_id;
    }

    public void setSection_id(int section_id) {
        this.section_id.set(section_id);
    }

    public SectionModel getSectionModel(){
        return Inc.model.section.get.get(getSection_id());
    }

    public StudentsModel getStudentsModel() {
        return Inc.model.students.get.get(getStudents_id());
    }
}
