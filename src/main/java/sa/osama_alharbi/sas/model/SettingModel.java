package sa.osama_alharbi.sas.model;

import javafx.beans.property.*;

public class SettingModel {
    private IntegerProperty setting_id;
    private StringProperty setting_username;
    private StringProperty setting_password;
    private StringProperty setting_serverPassword;
    private IntegerProperty setting_serverPort;

    public SettingModel(int setting_id, String setting_username, String setting_password, String setting_serverPassword, int setting_serverPort) {
        this.setting_id = new SimpleIntegerProperty(setting_id);
        this.setting_username = new SimpleStringProperty(setting_username);
        this.setting_password = new SimpleStringProperty(setting_password);
        this.setting_serverPassword = new SimpleStringProperty(setting_serverPassword);
        this.setting_serverPort = new SimpleIntegerProperty(setting_serverPort);
    }

    public int getSetting_id() {
        return setting_id.get();
    }

    public IntegerProperty setting_idProperty() {
        return setting_id;
    }

    public void setSetting_id(int setting_id) {
        this.setting_id.set(setting_id);
    }

    public String getSetting_username() {
        return setting_username.get();
    }

    public StringProperty setting_usernameProperty() {
        return setting_username;
    }

    public void setSetting_username(String setting_username) {
        this.setting_username.set(setting_username);
    }

    public String getSetting_password() {
        return setting_password.get();
    }

    public StringProperty setting_passwordProperty() {
        return setting_password;
    }

    public void setSetting_password(String setting_password) {
        this.setting_password.set(setting_password);
    }

    public String getSetting_serverPassword() {
        return setting_serverPassword.get();
    }

    public StringProperty setting_serverPasswordProperty() {
        return setting_serverPassword;
    }

    public void setSetting_serverPassword(String setting_serverPassword) {
        this.setting_serverPassword.set(setting_serverPassword);
    }

    public int getSetting_serverPort() {
        return setting_serverPort.get();
    }

    public IntegerProperty setting_serverPortProperty() {
        return setting_serverPort;
    }

    public void setSetting_serverPort(int setting_serverPort) {
        this.setting_serverPort.set(setting_serverPort);
    }
}
