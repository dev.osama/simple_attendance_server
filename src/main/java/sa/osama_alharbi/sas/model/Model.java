package sa.osama_alharbi.sas.model;

import javafx.application.Platform;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.MapChangeListener;
import javafx.collections.ObservableMap;
import sa.osama_alharbi.sas.inc.Inc;
import sa.osama_alharbi.sas.ws.Server;

import java.util.ArrayList;
import java.util.HashMap;

public class Model {

    private ObservableMap<Integer, StudentsModel> studentsM;

    private ObservableMap<Integer, ClassesModel> classesM;

    private ObservableMap<Integer, SectionModel> sectionM;
    private HashMap<Integer,ArrayList<SectionModel>> arr_sectionM_via_class_id;

    private ObservableMap<Integer, StudentsSectionModel> studentsSectionM;
    private HashMap<Integer,ArrayList<StudentsSectionModel>> arr_studentsSectionM_via_section_id;
    private HashMap<Integer,ArrayList<StudentsSectionModel>> arr_studentsSectionM_via_students_id;

    private ObjectProperty<SettingModel> settingM;

    //StudentsSectionModel

    public Students students;
    public Classes classes;
    public Section section;
    public StudentsSection studentsSection;
    public Setting setting;
    public Server server;
    public Model(){
        this.studentsM = FXCollections.observableHashMap();
        this.classesM = FXCollections.observableHashMap();
        this.sectionM = FXCollections.observableHashMap();
        this.arr_sectionM_via_class_id = new HashMap<>();
        this.studentsSectionM = FXCollections.observableHashMap();
        this.arr_studentsSectionM_via_section_id = new HashMap<>();
        this.arr_studentsSectionM_via_students_id = new HashMap<>();
        this.settingM = new SimpleObjectProperty<>(null);


        this.students = new Students();
        this.classes = new Classes();
        this.section = new Section();
        this.studentsSection = new StudentsSection();
        this.setting = new Setting();
        this.server = new Server();
    }

    public class Students{

        public Add add = null;
        public Get get = null;
        public Edit edit = null;
        public Delete delete = null;

        public Students(){
            this.add = new Add();
            this.get = new Get();
            this.edit = new Edit();
            this.delete = new Delete();
            studentsM.addListener(new MapChangeListener<Integer, StudentsModel>() {
                @Override
                public void onChanged(Change<? extends Integer, ? extends StudentsModel> m) {
                    if(m.wasAdded()){
                        StudentsModel model = m.getValueAdded();

                        Inc.action.lisner.students.onAddModel(model);
                    }
                    if(m.wasRemoved()){
                        StudentsModel model = m.getValueRemoved();

                        Inc.action.lisner.students.onRemoveModel(model);
                    }
                }
            });
        }

        public class Add{
            public void add(StudentsModel model){
                if(model == null)return;
                if(!studentsM.containsKey(model.getStudents_id())){
                    studentsM.put(model.getStudents_id(),model);
                }
            }
            public void add(ArrayList<StudentsModel> arr){
                if(arr == null)return;
                if(arr.size() <= 0)return;
                for (StudentsModel studentsModel : arr) {
                    add(studentsModel);
                }
            }
        }

        public class Get{
            public ObservableMap<Integer, StudentsModel> getObservableMap(){
                return studentsM;
            }
            public StudentsModel get(int students_id){
                if(studentsM.containsKey(students_id)){
                    return studentsM.get(students_id);
                }else{
                    StudentsModel model = Inc.db.tables.students.select.select(students_id);
                    if(model == null)return null;
                    studentsM.put(model.getStudents_id(), model);
                    return model;
                }
            }

            public ArrayList<StudentsModel> getAll() {
                return new ArrayList<StudentsModel>(studentsM.values());
            }
        }

        public class Edit{
            public boolean editStudents_name(StudentsModel model,String students_name){
                if(studentsM.containsKey(model.getStudents_id())){
                    studentsM.get(model.getStudents_id()).setStudents_name(students_name);
                    return true;
                }else{
                    return false;
                }
            }
//            public boolean editStudents_isDelete(StudentsModel model,boolean students_isDelete){
//                if(studentsM.containsKey(model.getStudents_id())){
//                    studentsM.get(model.getStudents_id()).setStudents_isDelete(students_isDelete);
//                    return true;
//                }else{
//                    return false;
//                }
//            }
        }

        public class Delete{
            public void delete(int students_id){
                if(studentsM.containsKey(students_id)){
                    studentsM.remove(students_id);
                }
            }
            public void delete(StudentsModel studentsModel){
                if(studentsModel == null)return;
                delete(studentsModel.getStudents_id());
            }
        }
    }

    public class Classes{

        public Add add = null;
        public Get get = null;
        public Edit edit = null;
        public Delete delete = null;

        public Classes(){
            this.add = new Add();
            this.get = new Get();
            this.edit = new Edit();
            this.delete = new Delete();
            classesM.addListener(new MapChangeListener<Integer, ClassesModel>() {
                @Override
                public void onChanged(Change<? extends Integer, ? extends ClassesModel> m) {
                    if(m.wasAdded()){
                        ClassesModel model = m.getValueAdded();

                        Inc.action.lisner.classes.onAddModel(model);
                    }
                    if(m.wasRemoved()){
                        ClassesModel model = m.getValueRemoved();

                        Inc.action.lisner.classes.onRemoveModel(model);
                    }
                }
            });
        }

        public class Add{
            public void add(ClassesModel model){
                if(model == null)return;
                if(!classesM.containsKey(model.getClass_id())){
                    classesM.put(model.getClass_id(),model);
                }
            }
            public void add(ArrayList<ClassesModel> arr){
                if(arr == null)return;
                if(arr.size() <= 0)return;
                for (ClassesModel classesModel : arr) {
                    add(classesModel);
                }
            }
        }

        public class Get{
            public ObservableMap<Integer, ClassesModel> getObservableMap(){
                return classesM;
            }
            public ClassesModel get(int classes_id){
                if(classesM.containsKey(classes_id)){
                    return classesM.get(classes_id);
                }else{
                    ClassesModel model = Inc.db.tables.class_.select.select(classes_id);
                    if(model == null)return null;
                    classesM.put(model.getClass_id(), model);
                    return model;
                }
            }

            public ArrayList<ClassesModel> getAllClassesByStudents_id(int students_id) {
                ArrayList<StudentsSectionModel> arr = studentsSection.get.getAllByStudents_id(students_id);

                ArrayList<ClassesModel> arrResult = new ArrayList<>();
                for (StudentsSectionModel i:arr) {
                    if(!arrResult.contains(i.getSectionModel().getClassesModel())){
                        if(!i.isStudents_section_isDelete()){
                            arrResult.add(i.getSectionModel().getClassesModel());
                        }
                    }
                }
                return arrResult;
            }

            public ArrayList<ClassesModel> getAll() {
                return new ArrayList<ClassesModel>(classesM.values());
            }

            public ArrayList<ClassesModel> getAllNotDeleted() {
                ArrayList<ClassesModel> arr = new ArrayList<>();
                for (ClassesModel i:classesM.values()) {
                    if(!i.isClass_isDelete()){
                        arr.add(i);
                    }
                }
                return arr;
            }
        }

        public class Edit{
            public boolean editClasses_name(ClassesModel model,String classes_name){
                if(classesM.containsKey(model.getClass_id())){
                    classesM.get(model.getClass_id()).setClass_name(classes_name);
                    return true;
                }else{
                    return false;
                }
            }
            public boolean editClasses_isDelete(ClassesModel model,boolean classes_isDelete){
                if(classesM.containsKey(model.getClass_id())){
                    classesM.get(model.getClass_id()).setClass_isDelete(classes_isDelete);
                    return true;
                }else{
                    return false;
                }
            }
        }

        public class Delete{
            public void forceDelete(int classes_id){
                if(classesM.containsKey(classes_id)){
                    classesM.remove(classes_id);
                }
            }
            public void forceDelete(ClassesModel classesModel){
                if(classesModel == null)return;
                forceDelete(classesModel.getClass_id());
            }
        }
    }

    public class Section{

        public Add add = null;
        public Get get = null;
        public Edit edit = null;
        public Delete delete = null;

        public Section(){
            this.add = new Add();
            this.get = new Get();
            this.edit = new Edit();
            this.delete = new Delete();
            sectionM.addListener(new MapChangeListener<Integer, SectionModel>() {
                @Override
                public void onChanged(Change<? extends Integer, ? extends SectionModel> m) {
                    if(m.wasAdded()){
                        SectionModel model = m.getValueAdded();

                        if(!arr_sectionM_via_class_id.containsKey(model.getClass_id())){
                            arr_sectionM_via_class_id.put(model.getClass_id(),new ArrayList<>());
                        }
                        arr_sectionM_via_class_id.get(model.getClass_id()).add(model);

                        Inc.action.lisner.section.onAddModel(model);
                    }
                    if(m.wasRemoved()){
                        SectionModel model = m.getValueRemoved();

                        if(arr_sectionM_via_class_id.containsKey(model.getClass_id())){
                            SectionModel oldModel = null;
                            for (SectionModel i:arr_sectionM_via_class_id.get(model.getClass_id())) {
                                if(i.getSection_id() == model.getSection_id()){
                                    oldModel = i;
                                    break;
                                }
                            }
                            if(oldModel != null){
                                arr_sectionM_via_class_id.get(model.getClass_id()).remove(oldModel);
                            }
                        }

                        Inc.action.lisner.section.onRemoveModel(model);
                    }
                }
            });
        }

        public class Add{
            public void add(SectionModel model){
                if(model == null)return;
                if(!sectionM.containsKey(model.getSection_id())){
                    sectionM.put(model.getSection_id(),model);
                }
            }
            public void add(ArrayList<SectionModel> arr){
                if(arr == null)return;
                if(arr.size() <= 0)return;
                for (SectionModel sectionModel : arr) {
                    add(sectionModel);
                }
            }
        }

        public class Get{
            public ObservableMap<Integer, SectionModel> getObservableMap(){
                return sectionM;
            }
            public SectionModel get(int section_id){
                if(sectionM.containsKey(section_id)){
                    return sectionM.get(section_id);
                }else{
                    SectionModel model = Inc.db.tables.section.select.select(section_id);
                    if(model == null)return null;
                    sectionM.put(model.getSection_id(), model);
                    return model;
                }
            }
            public ArrayList<SectionModel> getAllByClassses_id(int Classses_id){
                if(arr_sectionM_via_class_id.containsKey(Classses_id)){
                    return arr_sectionM_via_class_id.get(Classses_id);
                }else{
                    return new ArrayList<>();
                }
            }

            public ArrayList<SectionModel> getAll() {
                return new ArrayList<SectionModel>(sectionM.values());
            }

            public ArrayList<SectionModel> getAllByClassses_idNotDeleted(int classes_id) {
                ArrayList<SectionModel> arr = new ArrayList<>();
                for (SectionModel i :arr_sectionM_via_class_id.get(classes_id)) {
                    if(!i.isSection_isDelete()){
                        arr.add(i);
                    }
                }
                return arr;
            }
        }

        public class Edit{
            public boolean editSection_nameAndPassword(SectionModel model,String section_name,String section_password){
                if(sectionM.containsKey(model.getSection_id())){
                    sectionM.get(model.getSection_id()).setSection_name(section_name);
                    sectionM.get(model.getSection_id()).setSection_password(section_password);
                    return true;
                }else{
                    return false;
                }
            }
            public boolean editSection_isDelete(SectionModel model,boolean section_isDelete){
                if(sectionM.containsKey(model.getClass_id())){
                    sectionM.get(model.getSection_id()).setSection_isDelete(section_isDelete);
                    return true;
                }else{
                    return false;
                }
            }
        }

        public class Delete{
            public void forceDelete(int section_id){
                if(sectionM.containsKey(section_id)){
                    sectionM.remove(section_id);
                }
            }
            public void forceDelete(SectionModel sectionModel){
                if(sectionModel == null)return;
                forceDelete(sectionModel.getSection_id());
            }

            public void deleteViaClasses_id(int classes_id) {
                if(arr_sectionM_via_class_id.get(classes_id)!=null){
                    ArrayList<SectionModel> arrDelete = new ArrayList<>(arr_sectionM_via_class_id.get(classes_id));
                    if(arrDelete != null){
                        for (SectionModel s:arrDelete) {
                            forceDelete(s.getSection_id());
                        }
                    }
                }
            }
        }
    }

    public class StudentsSection{

        public Add add = null;
        public Get get = null;
        public Edit edit = null;
        public Delete delete = null;

        public StudentsSection(){
            this.add = new Add();
            this.get = new Get();
            this.edit = new Edit();
            this.delete = new Delete();
            studentsSectionM.addListener(new MapChangeListener<Integer, StudentsSectionModel>() {
                @Override
                public void onChanged(Change<? extends Integer, ? extends StudentsSectionModel> m) {
                    if(m.wasAdded()){
                        StudentsSectionModel model = m.getValueAdded();

                        if(!arr_studentsSectionM_via_section_id.containsKey(model.getSection_id())){
                            arr_studentsSectionM_via_section_id.put(model.getSection_id(),new ArrayList<>());
                        }
                        arr_studentsSectionM_via_section_id.get(model.getSection_id()).add(model);

                        if(!arr_studentsSectionM_via_students_id.containsKey(model.getStudents_id())){
                            arr_studentsSectionM_via_students_id.put(model.getStudents_id(),new ArrayList<>());
                        }
                        arr_studentsSectionM_via_students_id.get(model.getStudents_id()).add(model);

                        Inc.action.lisner.studentsSection.onAddModel(model);
                    }
                    if(m.wasRemoved()){
                        StudentsSectionModel model = m.getValueRemoved();

                        if(arr_studentsSectionM_via_section_id.containsKey(model.getSection_id())){
                            StudentsSectionModel oldModel = null;
                            for (StudentsSectionModel i:arr_studentsSectionM_via_section_id.get(model.getSection_id())) {
                                if(i.getStudents_section_id() == model.getStudents_section_id()){
                                    oldModel = i;
                                    break;
                                }
                            }
                            if(oldModel != null){
                                arr_studentsSectionM_via_section_id.get(model.getSection_id()).remove(oldModel);
                            }
                        }

                        if(arr_studentsSectionM_via_students_id.containsKey(model.getStudents_id())){
                            StudentsSectionModel oldModel = null;
                            for (StudentsSectionModel i:arr_studentsSectionM_via_students_id.get(model.getStudents_id())) {
                                if(i.getStudents_section_id() == model.getStudents_section_id()){
                                    oldModel = i;
                                    break;
                                }
                            }
                            if(oldModel != null){
                                arr_studentsSectionM_via_students_id.get(model.getStudents_id()).remove(oldModel);
                            }
                        }

                        Inc.action.lisner.studentsSection.onRemoveModel(model);
                    }
                }
            });
        }

        public class Add{
            public void add(StudentsSectionModel model){
                if(model == null)return;
                if(!studentsSectionM.containsKey(model.getStudents_section_id())){
                    studentsSectionM.put(model.getStudents_section_id(),model);
                }
            }
            public void add(ArrayList<StudentsSectionModel> arr){
                if(arr == null)return;
                if(arr.size() <= 0)return;
                for (StudentsSectionModel studentsSectionModel : arr) {
                    add(studentsSectionModel);
                }
            }
        }

        public class Get{
            public ObservableMap<Integer, StudentsSectionModel> getObservableMap(){
                return studentsSectionM;
            }
            public StudentsSectionModel get(int students_section_id){
                if(studentsSectionM.containsKey(students_section_id)){
                    return studentsSectionM.get(students_section_id);
                }else{
                    StudentsSectionModel model = Inc.db.tables.students_section.select.select(students_section_id);
                    if(model == null)return null;
                    studentsSectionM.put(model.getStudents_section_id(), model);
                    return model;
                }
            }
            public ArrayList<StudentsSectionModel> getAllBySection_id(int section_id){
                if(arr_studentsSectionM_via_section_id.containsKey(section_id)){
                    return arr_studentsSectionM_via_section_id.get(section_id);
                }else{
                    return new ArrayList<>();
                }
            }
            public ArrayList<StudentsSectionModel> getAllByStudents_id(int students_id){
                if(arr_studentsSectionM_via_students_id.containsKey(students_id)){
                    return arr_studentsSectionM_via_students_id.get(students_id);
                }else{
                    return new ArrayList<>();
                }
            }

            public ArrayList<StudentsSectionModel> getAllBySection_idNotDeleted(int section_id) {
                ArrayList<StudentsSectionModel> arr = new ArrayList<>();
                for (StudentsSectionModel i :arr_studentsSectionM_via_section_id.get(section_id)) {
                    if(!i.isStudents_section_isDelete()){
                        arr.add(i);
                    }
                }
                return arr;
            }

            public ArrayList<StudentsSectionModel> getAllByStudents_idAndClasses_id(int students_id,int classes_id) {
                ArrayList<StudentsSectionModel> arr = new ArrayList<>();
                ArrayList<StudentsSectionModel> studentsArr = getAllByStudents_id(students_id);
                for (StudentsSectionModel i:studentsArr) {
                    if(i.getSectionModel().getClass_id() == classes_id){
                        arr.add(i);
                    }
                }
                return arr;
            }

            public ArrayList<StudentsSectionModel> getAllByStudents_idAndSection_id(int students_id, int section_id) {
                ArrayList<StudentsSectionModel> arr = new ArrayList<>();
                ArrayList<StudentsSectionModel> studentsArr = getAllByStudents_id(students_id);
                for (StudentsSectionModel i:studentsArr) {
                    if(i.getSectionModel().getSection_id() == section_id){
                        arr.add(i);
                    }
                }
                return arr;
            }
        }

        public class Edit{
            public boolean editStudents_Section_isDelete(StudentsSectionModel model,boolean students_section_isDelete){
                if(studentsSectionM.containsKey(model.getStudents_section_id())){
                    studentsSectionM.get(model.getStudents_section_id()).setStudents_section_isDelete(students_section_isDelete);
                    return true;
                }else{
                    return false;
                }
            }
        }

        public class Delete{
            public void forceDelete(int students_section_id){
                if(studentsSectionM.containsKey(students_section_id)){
                    studentsSectionM.remove(students_section_id);
                }
            }
            public void forceDelete(StudentsSectionModel studentsSectionModel){
                if(studentsSectionModel == null)return;
                forceDelete(studentsSectionModel.getStudents_section_id());
            }

            public void deleteViaStudents_id(int students_id) {
                ArrayList<StudentsSectionModel> arr = new ArrayList<>(arr_studentsSectionM_via_students_id.get(students_id));
                arr.forEach(studentsSectionModel -> {
                    forceDelete(studentsSectionModel);
                });
            }

            public void deleteViaClasses_id(int classes_id) {
                ArrayList<StudentsSectionModel> arr = new ArrayList<>(studentsSectionM.values());
                arr.forEach(studentsSectionModel -> {
                    if(studentsSectionModel.getSectionModel().getClass_id() == classes_id){
                        forceDelete(studentsSectionModel);
                    }
                });
            }

            public void deleteViaSection_id(int section_id) {
                ArrayList<StudentsSectionModel> arr = new ArrayList<>(arr_studentsSectionM_via_section_id.get(section_id));
                arr.forEach(studentsSectionModel -> {
                    if(studentsSectionModel.getSection_id() == section_id){
                        forceDelete(studentsSectionModel);
                    }
                });
            }
        }
    }

    public class Setting{

        public Add add = null;
        public Get get = null;
        public Edit edit = null;

        public Setting(){
            this.add = new Add();
            this.get = new Get();
            this.edit = new Edit();

        }

        public class Add{
            public void add(SettingModel model){
                if(model == null)return;
                settingM.set(model);
            }
        }

        public class Get{
            //ObjectProperty<SettingModel> settingM
            public ObjectProperty<SettingModel> getSettingModelProperty(){
                return settingM;
            }
            public SettingModel get(){
                return settingM.get();
            }
        }

        public class Edit{
            public boolean edit_usernameAndPassword(String username,String password){
                settingM.get().setSetting_username(username);
                settingM.get().setSetting_password(password);
                return true;
            }

            public void edit_serverPasswordPort(String serverPassword, int serverPort) {
                settingM.get().setSetting_serverPassword(serverPassword);
                settingM.get().setSetting_serverPort(serverPort);
            }
        }
    }

    public class Server{
        private BooleanProperty isRunning;
        private IntegerProperty runningType;
        public Get get;
        public Set set;
        public Server(){
            isRunning = new SimpleBooleanProperty(false);
            runningType = new SimpleIntegerProperty(sa.osama_alharbi.sas.ws.Server.Type.SERVER_CLOSED);
            runningType.addListener((observable, oldValue, newValue) -> {
                switch (newValue.intValue()){
                    case sa.osama_alharbi.sas.ws.Server.Type.SERVER_CLOSED:
                    case sa.osama_alharbi.sas.ws.Server.Type.SERVER_TRY_START:
                    case sa.osama_alharbi.sas.ws.Server.Type.SERVER_TRY_CLOSE:
                        isRunning.set(false);
                        break;
                    case sa.osama_alharbi.sas.ws.Server.Type.SERVER_STARTED:
                        isRunning.set(true);
                        break;
                }
            });
            get = new Get();
            set = new Set();
        }
        public class Get{
            public boolean getIsRunning(){
                return isRunning.get();
            }
            public BooleanProperty getIsRunningProperty(){
                return isRunning;
            }
            public int getRunningType(){
                return runningType.get();
            }
            public IntegerProperty getRunningTypeProperty(){
                return runningType;
            }
        }
        public class Set{
            public void setRunningType(int type){
                Platform.runLater(()->{
                    runningType.set(type);
                });
            }
        }
    }
}
