package sa.osama_alharbi.sas.db;

public class DBTable {
	public db_Setting setting = null;
	public db_Attendance attendance = null;
	public db_Students_Section students_section = null;
	public db_Section section = null;
	public db_Class class_ = null;
	public db_Students students = null;
	
	private DB db = null;
	public DBTable(DB db) {
		this.db = db;
		this.setting = new db_Setting(this.db);
		this.attendance = new db_Attendance(this.db);
		this.students_section = new db_Students_Section(this.db);
		this.section = new db_Section(this.db);
		this.class_ = new db_Class(this.db);
		this.students = new db_Students(this.db);
	}
}
