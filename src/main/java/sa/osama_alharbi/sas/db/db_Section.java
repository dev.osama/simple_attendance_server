package sa.osama_alharbi.sas.db;

import sa.osama_alharbi.sas.model.SectionModel;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class db_Section {

	public static final Var var = new Var();
	public static class Var{
		public static final String TABLE_NAME = "section";

		public static final String ITEM_id 			= TABLE_NAME+"_id";
		public static final String ITEM_name		= TABLE_NAME+"_name";
		public static final String ITEM_password	= TABLE_NAME+"_password";
		public static final String ITEM_isDelete	= TABLE_NAME+"_isDelete";
		public static final String Class_id 		= db_Class.Var.ITEM_id;
	}

	private DB db = null;

	public Create create;
	public Select select;
	public Insert insert;
	public Update update;
	public Delete delete;

	public db_Section(DB db) {
		this.db = db;
		create = new Create();
		select = new Select();
		insert = new Insert();
		update = new Update();
		delete = new Delete();
	}

	public static SectionModel toModel(ResultSet rs){
		try {
			return new SectionModel(rs.getInt(Var.ITEM_id), rs.getString(Var.ITEM_name), rs.getString(Var.ITEM_password),
					rs.getInt(Var.ITEM_isDelete)==1 ,rs.getInt(Var.Class_id));
		} catch (SQLException throwables) {
			return null;
		}
	}

	public class Create{
		public boolean createTable() {
			String sql = "CREATE TABLE IF NOT EXISTS "+ Var.TABLE_NAME+" " +
					"( " +
					"  "+ Var.ITEM_id+" INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT, " +
					"  "+ Var.ITEM_name+" TEXT NOT NULL, " +
					"  "+ Var.ITEM_password+" TEXT NOT NULL, " +
					"  "+ Var.ITEM_isDelete+" TINYINT NOT NULL, " +
					"  "+ Var.Class_id+" INTEGER NOT NULL, " +
					"  FOREIGN KEY ("+ Var.Class_id+") REFERENCES "+db_Class.Var.TABLE_NAME+"("+ Var.Class_id+") " +
					");";
			try {
				PreparedStatement stam = db.getStatement(sql);

				if(stam.executeUpdate() == 0) {

					return true;
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

			return false;
		}
	}

	public class Select{
		//TODO select 1
		public SectionModel select(int class_id) {
			String sql = "SELECT * FROM "+ Var.TABLE_NAME+" WHERE "+ Var.ITEM_id+" = ? AND "+Var.ITEM_isDelete+" = 0 ";
			try {
				PreparedStatement stam = db.getStatement(sql);
				stam.setInt(1,class_id);

				ResultSet rs = stam.executeQuery();

				if (rs.next()) {
					return toModel(rs);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

			return null;
		}

		//TODO select *
		public ArrayList<SectionModel> selectAll() {
			ArrayList<SectionModel> arr = new ArrayList<SectionModel>();
			String sql = "SELECT * FROM "+ Var.TABLE_NAME+" WHERE "+Var.ITEM_isDelete+" = 0 ";
			try {
				PreparedStatement stam = db.getStatement(sql);


				ResultSet rs = stam.executeQuery();

				while (rs.next()) {
					arr.add(toModel(rs));
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

			return arr;
		}

//		public ArrayList<SectionModel> selectAllNotDeleted() {
//			ArrayList<SectionModel> arr = new ArrayList<SectionModel>();
//			String sql = "SELECT * FROM "+ Var.TABLE_NAME+" WHERE "+Var.ITEM_isDelete+" = 0 ";
//			try {
//				PreparedStatement stam = db.getStatement(sql);
//
//
//				ResultSet rs = stam.executeQuery();
//
//				while (rs.next()) {
//					arr.add(toModel(rs));
//				}
//			} catch (SQLException e) {
//				e.printStackTrace();
//			}
//
//			return arr;
//		}
	}

	public class Insert{
		//TODO insert
		public SectionModel insert(String section_name, String section_password,int class_id) {
			String sql = "INSERT INTO "+ Var.TABLE_NAME+" "
					+ " ( "+ Var.ITEM_id+" , "+ Var.ITEM_name+" , "+ Var.ITEM_password+" , "+ Var.ITEM_isDelete+" , "+Var.Class_id+" ) "
					+ " VALUES "
					+ " ( NULL , ? , ? , 0 , ? )";
			try {
				PreparedStatement stam = db.getStatementWithId(sql);

				stam.setString(1,section_name);
				stam.setString(2,section_password);
				stam.setInt(3,class_id);


				int affectedRows = stam.executeUpdate();

				if (affectedRows != 0) {
					ResultSet rs = stam.getGeneratedKeys();
					if (rs.next()) {

						return select.select(rs.getInt(1));
					}
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

			return null;
		}
	}

	public class Update{
		//TODO update name
		public boolean updateSection_name_andPassword(int section_id,String section_name,String section_password) {
			String sql = "UPDATE "+ Var.TABLE_NAME+" SET "
					+ " "+ Var.ITEM_name+" = ? , "
					+ " "+ Var.ITEM_password+" = ?  "
					+ " WHERE "
					+ " "+ Var.ITEM_id+" = ? ";
			try {
				PreparedStatement stam = db.getStatement(sql);

				stam.setString(1, section_name);
				stam.setString(2, section_password);
				stam.setInt(3, section_id);

				int affectedRows = stam.executeUpdate();

				if (affectedRows > 0) {
					return true;
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

			return false;
		}

		//TODO update delete
		public boolean updateSection_isDelete(int section_id,boolean section_isDelete) {
			String sql = "UPDATE "+ Var.TABLE_NAME+" SET "
					+ " "+ Var.ITEM_isDelete+" = ?  "
					+ " WHERE "
					+ " "+ Var.ITEM_id+" = ? ";
			try {
				PreparedStatement stam = db.getStatement(sql);

				stam.setInt(1, section_isDelete?1:0);
				stam.setInt(2, section_id);

				int affectedRows = stam.executeUpdate();

				if (affectedRows > 0) {
					return true;
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return false;
		}

		//TODO update delete
		public boolean updateSection_isDeleteViaClasses_id(int classes_id,boolean section_isDelete) {
			String sql = "UPDATE "+ Var.TABLE_NAME+" SET "
					+ " "+ Var.ITEM_isDelete+" = ?  "
					+ " WHERE "
					+ " "+ Var.Class_id+" = ? ";
			try {
				PreparedStatement stam = db.getStatement(sql);

				stam.setInt(1, section_isDelete?1:0);
				stam.setInt(2, classes_id);

				int affectedRows = stam.executeUpdate();

				if (affectedRows > 0) {
					return true;
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return false;
		}
	}

	public class Delete{
		//TODO delete -->> link to update delete
		public boolean deleteSection(int section_id) {
			return update.updateSection_isDelete(section_id,true);
		}
		public boolean deleteSectionViaClasses_id(int classes_id) {
			return update.updateSection_isDeleteViaClasses_id(classes_id,true);
		}
	}
}
