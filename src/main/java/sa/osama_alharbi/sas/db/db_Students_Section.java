package sa.osama_alharbi.sas.db;

import sa.osama_alharbi.sas.model.SectionModel;
import sa.osama_alharbi.sas.model.StudentsModel;
import sa.osama_alharbi.sas.model.StudentsSectionModel;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class db_Students_Section {

	public static final Var var = new Var();
	public static class Var{
		public static final String TABLE_NAME = "students_section";

		public static final String ITEM_id 			= TABLE_NAME+"_id";
		public static final String ITEM_isDelete	= TABLE_NAME+"_isDelete";
		public static final String Students_id 		= db_Students.Var.ITEM_id;
		public static final String Section_id 		= db_Section.Var.ITEM_id;
	}

	private DB db = null;

	public Create create;
	public Select select;
	public Insert insert;
	public Update update;
	public Delete delete;

	public db_Students_Section(DB db) {
		this.db = db;
		create = new Create();
		select = new Select();
		insert = new Insert();
		update = new Update();
		delete = new Delete();
	}

	public static StudentsSectionModel toModel(ResultSet rs){
		try {
			return new StudentsSectionModel(rs.getInt(Var.ITEM_id),rs.getInt(Var.ITEM_isDelete)==1 ,rs.getInt(Var.Students_id),rs.getInt(Var.Section_id));
		} catch (SQLException throwables) {
			return null;
		}
	}

	public class Create{
		public boolean createTable() {
			String sql = "CREATE TABLE IF NOT EXISTS "+ Var.TABLE_NAME+" " +
					"( " +
					"  "+ Var.ITEM_id+" INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT, " +
					"  "+ Var.ITEM_isDelete+" TINYINT NOT NULL, " +
					"  "+ Var.Students_id+" INTEGER NOT NULL, " +
					"  "+ Var.Section_id+" INTEGER NOT NULL, " +
					"  FOREIGN KEY ("+ Var.Students_id+") REFERENCES "+db_Students.Var.TABLE_NAME+"("+ Var.Students_id+"), " +
					"  FOREIGN KEY ("+ Var.Section_id+") REFERENCES "+db_Section.Var.TABLE_NAME+"("+ Var.Section_id+") " +
					");";
			try {
				PreparedStatement stam = db.getStatement(sql);

				if(stam.executeUpdate() == 0) {

					return true;
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

			return false;
		}
	}

	public class Select{
		//TODO select 1
		public StudentsSectionModel select(int students_section_id) {
			String sql = "SELECT * FROM "+ Var.TABLE_NAME+" WHERE "+ Var.ITEM_id+" = ? AND "+Var.ITEM_isDelete+" = 0 ";
			try {
				PreparedStatement stam = db.getStatement(sql);
				stam.setInt(1,students_section_id);

				ResultSet rs = stam.executeQuery();

				if (rs.next()) {
					return toModel(rs);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

			return null;
		}

		//TODO select *
		public ArrayList<StudentsSectionModel> selectAll() {
			ArrayList<StudentsSectionModel> arr = new ArrayList<StudentsSectionModel>();
			String sql = "SELECT * FROM "+ Var.TABLE_NAME+" WHERE "+Var.ITEM_isDelete+" = 0 ";
			try {
				PreparedStatement stam = db.getStatement(sql);


				ResultSet rs = stam.executeQuery();

				while (rs.next()) {
					arr.add(toModel(rs));
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

			return arr;
		}

//		public ArrayList<StudentsSectionModel> selectAllNotDeleted() {
//			ArrayList<StudentsSectionModel> arr = new ArrayList<StudentsSectionModel>();
//			String sql = "SELECT * FROM "+ Var.TABLE_NAME+" WHERE "+Var.ITEM_isDelete+" = 0 ";
//			try {
//				PreparedStatement stam = db.getStatement(sql);
//
//
//				ResultSet rs = stam.executeQuery();
//
//				while (rs.next()) {
//					arr.add(toModel(rs));
//				}
//			} catch (SQLException e) {
//				e.printStackTrace();
//			}
//
//			return arr;
//		}

		/*
		SELECT section.class_id
		FROM students_section,section,class
		where students_section.students_id = 1
		AND students_section.students_section_isDelete = 0
		AND students_section.section_id = section.section_id
		AND section.section_isDelete = 0
		AND class.class_isDelete = 0
		GROUP BY 1;
		 */

		public ArrayList<SectionModel> selectAllSectionModels_via_studentsId(int students_id) {
			ArrayList<SectionModel> models = new ArrayList<>();
			String sql = "SELECT "+db_Section.Var.TABLE_NAME+".* " +
					"FROM "+ Var.TABLE_NAME+","+db_Section.Var.TABLE_NAME+","+db_Class.Var.TABLE_NAME+" " +
					"where "+Var.TABLE_NAME+"."+Var.Students_id+" = ? " +
					"AND "+Var.TABLE_NAME+"."+Var.ITEM_isDelete+" = 0 " +
					"AND "+Var.TABLE_NAME+"."+Var.Section_id+" = "+db_Section.Var.TABLE_NAME+"."+db_Section.Var.ITEM_id+ " " +
					"AND "+db_Section.Var.TABLE_NAME+"."+db_Section.Var.Class_id+ " = "+db_Class.Var.TABLE_NAME+"."+db_Class.Var.ITEM_id+" " +
					"AND "+db_Section.Var.TABLE_NAME+"."+db_Section.Var.ITEM_isDelete+" = 0 " +
					"AND "+db_Class.Var.TABLE_NAME+"."+db_Class.Var.ITEM_isDelete+" = 0 ;";
			try {
				PreparedStatement stam = db.getStatement(sql);
				stam.setInt(1,students_id);

				ResultSet rs = stam.executeQuery();

				while (rs.next()) {
					models.add(db_Section.toModel(rs));
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

			return models;
		}

		public ArrayList<StudentsSectionModel> selectAllStudentsSectionModel_via_studentsId(int students_id) {
			ArrayList<StudentsSectionModel> models = new ArrayList<>();
			String sql = "SELECT "+Var.TABLE_NAME+".* " +
					"FROM "+ Var.TABLE_NAME+","+db_Section.Var.TABLE_NAME+","+db_Class.Var.TABLE_NAME+" " +
					"where "+Var.TABLE_NAME+"."+Var.Students_id+" = ? " +
					"AND "+Var.TABLE_NAME+"."+Var.ITEM_isDelete+" = 0 " +
					"AND "+Var.TABLE_NAME+"."+Var.Section_id+" = "+db_Section.Var.TABLE_NAME+"."+db_Section.Var.ITEM_id+ " " +
					"AND "+db_Section.Var.TABLE_NAME+"."+db_Section.Var.Class_id+ " = "+db_Class.Var.TABLE_NAME+"."+db_Class.Var.ITEM_id+" " +
					"AND "+db_Section.Var.TABLE_NAME+"."+db_Section.Var.ITEM_isDelete+" = 0 " +
					"AND "+db_Class.Var.TABLE_NAME+"."+db_Class.Var.ITEM_isDelete+" = 0 ;";
			try {
				PreparedStatement stam = db.getStatement(sql);
				stam.setInt(1,students_id);

				ResultSet rs = stam.executeQuery();

				while (rs.next()) {
					models.add(toModel(rs));
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

			return models;
		}
	}

	public class Insert{
		//TODO insert
		public StudentsSectionModel insert(int section_id,int students_id) {
			String sql = "INSERT INTO "+ Var.TABLE_NAME+" "
					+ " ( "+ Var.ITEM_id+" , "+ Var.ITEM_isDelete+" , "+ Var.Section_id+" , "+ Var.Students_id +"  ) "
					+ " VALUES "
					+ " ( NULL , 0 , ? , ? )";
			try {
				PreparedStatement stam = db.getStatementWithId(sql);

				stam.setInt(1,section_id);
				stam.setInt(2,students_id);


				int affectedRows = stam.executeUpdate();

				if (affectedRows != 0) {
					ResultSet rs = stam.getGeneratedKeys();
					if (rs.next()) {
						return select.select(rs.getInt(1));
					}
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

			return null;
		}
	}

	public class Update{

		//TODO update delete
		public boolean updateStudents_Section_isDelete(int students_section_id,boolean students_section_isDelete) {
			String sql = "UPDATE "+ Var.TABLE_NAME+" SET "
					+ " "+ Var.ITEM_isDelete+" = ?  "
					+ " WHERE "
					+ " "+ Var.ITEM_id+" = ? ";
			try {
				PreparedStatement stam = db.getStatement(sql);

				stam.setInt(1, students_section_isDelete?1:0);
				stam.setInt(2, students_section_id);

				int affectedRows = stam.executeUpdate();

				if (affectedRows > 0) {
					return true;
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return false;
		}

		public boolean updateStudents_Section_isDelete_viaStudents_id(int students_id,boolean students_section_isDelete) {
			String sql = "UPDATE "+ Var.TABLE_NAME+" SET "
					+ " "+ Var.ITEM_isDelete+" = ?  "
					+ " WHERE "
					+ " "+ Var.Students_id+" = ? ";
			try {
				PreparedStatement stam = db.getStatement(sql);

				stam.setInt(1, students_section_isDelete?1:0);
				stam.setInt(2, students_id);

				int affectedRows = stam.executeUpdate();

				if (affectedRows > 0) {
					return true;
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return false;
		}

		public boolean updateStudents_Section_isDelete_viaClasses_id(int classes_id,boolean students_section_isDelete) {
			String sql = 	"UPDATE "+Var.TABLE_NAME+","+db_Section.Var.TABLE_NAME+" " +
					" SET" +
					" "+Var.TABLE_NAME+"."+Var.ITEM_isDelete+" = ? " +
					" WHERE "+db_Section.Var.TABLE_NAME+"."+db_Section.Var.Class_id+" = ? " +
					" AND "+db_Section.Var.TABLE_NAME+"."+db_Section.Var.ITEM_id+" = "+Var.TABLE_NAME+"."+Var.Section_id+" ";
			try {
				PreparedStatement stam = db.getStatement(sql);

				stam.setInt(1, students_section_isDelete?1:0);
				stam.setInt(2, classes_id);

				int affectedRows = stam.executeUpdate();

				if (affectedRows > 0) {
					return true;
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return false;
		}

		public boolean updateStudents_Section_isDelete_viaSections_id(int sections_id,boolean students_section_isDelete) {
			String sql = 	"UPDATE "+Var.TABLE_NAME+" " +
					" SET" +
					" "+Var.TABLE_NAME+"."+Var.ITEM_isDelete+" = ? " +
					" WHERE "+Var.Section_id+" = ? " ;
			try {
				PreparedStatement stam = db.getStatement(sql);

				stam.setInt(1, students_section_isDelete?1:0);
				stam.setInt(2, sections_id);

				int affectedRows = stam.executeUpdate();

				if (affectedRows > 0) {
					return true;
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return false;
		}
	}

	public class Delete{
		//TODO delete -->> link to update delete
		public boolean deleteStudents(int students_section_id) {
			return update.updateStudents_Section_isDelete(students_section_id,true);
		}

		public boolean deleteStudents_viaStudents_id(int students_id) {
			return update.updateStudents_Section_isDelete_viaStudents_id(students_id,true);
		}

		public boolean deleteStudents_viaClasses_id(int classes_id) {
			return update.updateStudents_Section_isDelete_viaClasses_id(classes_id,true);
		}

		public boolean deleteStudents_viaSection_id(int section_id) {
			return update.updateStudents_Section_isDelete_viaSections_id(section_id,true);
		}
	}
}
