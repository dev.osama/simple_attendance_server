package sa.osama_alharbi.sas.db;

import sa.osama_alharbi.sas.model.ClassesModel;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class db_Class {

	public static class Var{
		public static final String TABLE_NAME = "class";

		public static final String ITEM_id 			= TABLE_NAME+"_id";
		public static final String ITEM_name 		= TABLE_NAME+"_name";
		public static final String ITEM_isDelete	= TABLE_NAME+"_isDelete";
	}

	private DB db = null;

	public Create create;
	public Select select;
	public Insert insert;
	public Update update;
	public Delete delete;

	public static ClassesModel toModel(ResultSet rs){
		try {
			return new ClassesModel(rs.getInt(Var.ITEM_id), rs.getString(Var.ITEM_name),rs.getInt(Var.ITEM_isDelete)==1);
		} catch (SQLException throwables) {
			return null;
		}
	}

	public db_Class(DB db) {
		this.db = db;
		create = new Create();
		select = new Select();
		insert = new Insert();
		update = new Update();
		delete = new Delete();
	}

	public class Create{
		public boolean createTable() {
			String sql = "CREATE TABLE IF NOT EXISTS "+ Var.TABLE_NAME+" " +
					"( " +
					"  "+ Var.ITEM_id+" INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT, " +
					"  "+ Var.ITEM_name+" TEXT NOT NULL, " +
					"  "+ Var.ITEM_isDelete+" TINYINT NOT NULL " +
					");";
			try {
				PreparedStatement stam = db.getStatement(sql);

				if(stam.executeUpdate() == 0) {

					return true;
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

			return false;
		}
	}

	public class Select{
		//TODO select 1
		public ClassesModel select(int class_id) {
			String sql = "SELECT * FROM "+ Var.TABLE_NAME+" WHERE "+ Var.ITEM_id+" = ? AND "+Var.ITEM_isDelete+" = 0 ";
			try {
				PreparedStatement stam = db.getStatement(sql);
				stam.setInt(1,class_id);

				ResultSet rs = stam.executeQuery();

				if (rs.next()) {
					return toModel(rs);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

			return null;
		}

		//TODO select *
		public ArrayList<ClassesModel> selectAll() {
			ArrayList<ClassesModel> arr = new ArrayList<ClassesModel>();
			String sql = "SELECT * FROM "+ Var.TABLE_NAME+" WHERE "+Var.ITEM_isDelete+" = 0 ";
			try {
				PreparedStatement stam = db.getStatement(sql);


				ResultSet rs = stam.executeQuery();

				while (rs.next()) {
					arr.add(toModel(rs));
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

			return arr;
		}

//		public ArrayList<ClassesModel> selectAllNotDeleted() {
//			ArrayList<ClassesModel> arr = new ArrayList<ClassesModel>();
//			String sql = "SELECT * FROM "+ Var.TABLE_NAME+" WHERE "+Var.ITEM_isDelete+" = 0 ";
//			try {
//				PreparedStatement stam = db.getStatement(sql);
//
//
//				ResultSet rs = stam.executeQuery();
//
//				while (rs.next()) {
//					arr.add(toModel(rs));
//				}
//			} catch (SQLException e) {
//				e.printStackTrace();
//			}
//
//			return arr;
//		}
	}

	public class Insert{
		//TODO insert
		public ClassesModel insert(String class_name) {
			String sql = "INSERT INTO "+ Var.TABLE_NAME+" "
					+ " ( "+ Var.ITEM_id+" , "+ Var.ITEM_name+" , "+ Var.ITEM_isDelete+"  ) "
					+ " VALUES "
					+ " ( NULL , ? , 0 )";
			try {
				PreparedStatement stam = db.getStatementWithId(sql);

				stam.setString(1,class_name);


				int affectedRows = stam.executeUpdate();

				if (affectedRows != 0) {
					ResultSet rs = stam.getGeneratedKeys();
					if (rs.next()) {

						return select.select(rs.getInt(1));
					}
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

			return null;
		}
	}

	public class Update{
		//TODO update name
		public boolean updateClass_name(int class_id,String class_name) {
			String sql = "UPDATE "+ Var.TABLE_NAME+" SET "
					+ " "+ Var.ITEM_name+" = ?  "
					+ " WHERE "
					+ " "+ Var.ITEM_id+" = ? ";
			try {
				PreparedStatement stam = db.getStatement(sql);

				stam.setString(1, class_name);
				stam.setInt(2, class_id);

				int affectedRows = stam.executeUpdate();

				if (affectedRows > 0) {
					return true;
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

			return false;
		}

		//TODO update delete
		public boolean updateClass_isDelete(int class_id,boolean class_isDelete) {
			String sql = "UPDATE "+ Var.TABLE_NAME+" SET "
					+ " "+ Var.ITEM_isDelete+" = ?  "
					+ " WHERE "
					+ " "+ Var.ITEM_id+" = ? ";
			try {
				PreparedStatement stam = db.getStatement(sql);

				stam.setInt(1, class_isDelete?1:0);
				stam.setInt(2, class_id);

				int affectedRows = stam.executeUpdate();

				if (affectedRows > 0) {
					return true;
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return false;
		}
	}

	public class Delete{
		//TODO delete -->> link to update delete
		public boolean deleteClass(int students_id) {
			return update.updateClass_isDelete(students_id,true);
		}
	}
}
