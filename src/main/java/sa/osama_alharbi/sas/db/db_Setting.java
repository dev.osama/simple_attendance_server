package sa.osama_alharbi.sas.db;

import sa.osama_alharbi.sas.model.ClassesModel;
import sa.osama_alharbi.sas.model.SectionModel;
import sa.osama_alharbi.sas.model.SettingModel;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class db_Setting {

	public static final Var var = new Var();
	public static class Var{
		public static final String TABLE_NAME = "setting";

		public static final String ITEM_id 				= TABLE_NAME+"_id";
		public static final String ITEM_username 		= TABLE_NAME+"_username";
		public static final String ITEM_password		= TABLE_NAME+"_password";
		public static final String ITEM_serverPassword	= TABLE_NAME+"_serverPassword";
		public static final String ITEM_serverPort		= TABLE_NAME+"_serverPort";
	}
	
	private DB db = null;

	public Create create;
	public Select select;
	public Insert insert;
	public Update update;
	public Delete delete;
	
	public db_Setting(DB db) {
		this.db = db;
		create = new Create();
		select = new Select();
		insert = new Insert();
		update = new Update();
		delete = new Delete();
	}

	public static SettingModel toModel(ResultSet rs){
		try {
			return new SettingModel(rs.getInt(Var.ITEM_id), rs.getString(Var.ITEM_username),rs.getString(Var.ITEM_password),rs.getString(Var.ITEM_serverPassword),rs.getInt(Var.ITEM_serverPort));
		} catch (SQLException throwables) {
			return null;
		}
	}

	public class Create{
		public boolean createTable() {
			String sql = "CREATE TABLE IF NOT EXISTS "+ Var.TABLE_NAME+" " +
					"( " +
					"  "+ Var.ITEM_id+" INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT, " +
					"  "+ Var.ITEM_username+" TEXT NOT NULL, " +
					"  "+ Var.ITEM_password+" TEXT NOT NULL, " +
					"  "+ Var.ITEM_serverPassword+" TEXT NOT NULL, " +
					"  "+ Var.ITEM_serverPort+" INTEGER NOT NULL " +
					");";
			try {
				PreparedStatement stam = db.getStatement(sql);

				if(stam.executeUpdate() == 0) {

					return true;
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

			return false;
		}
	}

	public class Select{
		public SettingModel select() {
			String sql = "SELECT * FROM "+ Var.TABLE_NAME+" WHERE "+ Var.ITEM_id+" = 1 ";
			try {
				PreparedStatement stam = db.getStatement(sql);

				ResultSet rs = stam.executeQuery();

				if (rs.next()) {
					return toModel(rs);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return null;
		}
	}

	public class Insert{
		//INSERT INTO `simpleattendance`.`setting` (`setting_username`, `setting_password`, `setting_serverPassword`, `setting_serverPort`) VALUES ('g', 'g', 'g', 'g');
		//TODO insert after create table
		public SettingModel insert() {
			String sql = "INSERT INTO "+ Var.TABLE_NAME+" "
					+ " ( "+ Var.ITEM_id+" , "+ Var.ITEM_username+" , "+ Var.ITEM_password+" , "+Var.ITEM_serverPassword+" , "+Var.ITEM_serverPort+"  ) "
					+ " VALUES "
					+ " ( NULL , 'admin' , 'admin' , '123123' , 8988 )";
			try {
				PreparedStatement stam = db.getStatementWithId(sql);

				int affectedRows = stam.executeUpdate();

				if (affectedRows != 0) {
					ResultSet rs = stam.getGeneratedKeys();
					if (rs.next()) {
						return select.select();
					}
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

			return null;
		}
	}

	public class Update{
		//UPDATE `simpleattendance`.`setting` SET `setting_username` = 'ah', `setting_password` = 'aa' WHERE (`setting_id` = '1');
		//TODO update username and password
		public boolean update_usernameAndPassword(String username,String password) {
			String sql = "UPDATE "+ Var.TABLE_NAME+" SET "
					+ " "+ Var.ITEM_username+" = ? , "
					+ " "+ Var.ITEM_password+" = ?  "
					+ " WHERE "
					+ " "+ Var.ITEM_id+" = 1 ";
			try {
				PreparedStatement stam = db.getStatement(sql);

				stam.setString(1, username);
				stam.setString(2, password);

				int affectedRows = stam.executeUpdate();

				if (affectedRows > 0) {
					return true;
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

			return false;
		}

		public boolean update_serverPasswordPort(String serverPassword,int serverPort) {
			String sql = "UPDATE "+ Var.TABLE_NAME+" SET "
					+ " "+ Var.ITEM_serverPort+" = ? ,  "
					+ " "+ Var.ITEM_serverPassword+" = ?  "
					+ " WHERE "
					+ " "+ Var.ITEM_id+" = 1 ";
			try {
				PreparedStatement stam = db.getStatement(sql);

				stam.setInt(1, serverPort);
				stam.setString(1, serverPassword);

				int affectedRows = stam.executeUpdate();

				if (affectedRows > 0) {
					return true;
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

			return false;
		}
	}

	public class Delete{

	}
}
