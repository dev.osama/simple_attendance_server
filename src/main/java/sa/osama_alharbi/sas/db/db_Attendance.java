package sa.osama_alharbi.sas.db;

import sa.osama_alharbi.sas.model.AttendanceModel;
import sa.osama_alharbi.sas.model.ClassesModel;

import java.sql.Array;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class db_Attendance {
    public static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
    public static DateTimeFormatter formatterFromDB = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    public static final Var var = new Var();

    public static class Var {
        public static final String TABLE_NAME = "attendance";

        public static final String ITEM_id = TABLE_NAME + "_id";
        public static final String ITEM_dateTime = TABLE_NAME + "_dateTime";
        public static final String ITEM_isAbsent = TABLE_NAME + "_isAbsent";
        public static final String ITEM_isLate = TABLE_NAME + "_isLate";
        public static final String Students_Section_id = db_Students_Section.Var.ITEM_id;
    }

    private DB db = null;

    public Create create;
    public Select select;
    public Insert insert;
    public Update update;
    public Delete delete;

    public static AttendanceModel toModel(ResultSet rs) {
        try {
            String datatime = rs.getString(Var.ITEM_dateTime);
            datatime = datatime.substring(0, datatime.length() - 2);
            return new AttendanceModel(rs.getInt(var.ITEM_id), LocalDateTime.parse(datatime, formatterFromDB), rs.getInt(var.ITEM_isAbsent) == 1, rs.getInt(var.ITEM_isLate) == 1,
                    rs.getInt(var.Students_Section_id));
        } catch (SQLException throwables) {
            return null;
        }
    }

    public db_Attendance(DB db) {
        this.db = db;
        create = new Create();
        select = new Select();
        insert = new Insert();
        update = new Update();
        delete = new Delete();
    }

    public class Create {
        public boolean createTable() {
            String sql = "CREATE TABLE IF NOT EXISTS " + Var.TABLE_NAME + " " +
                    "( " +
                    "  " + Var.ITEM_id + " INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT, " +
                    "  " + Var.ITEM_dateTime + " datetime NOT NULL, " +
                    "  " + Var.ITEM_isAbsent + " TINYINT NOT NULL, " +
                    "  " + Var.ITEM_isLate + " TINYINT NOT NULL, " +
                    "  " + Var.Students_Section_id + " INTEGER NOT NULL, " +
                    "  FOREIGN KEY (" + Var.Students_Section_id + ") REFERENCES " + db_Students_Section.Var.TABLE_NAME + "(" + Var.Students_Section_id + ") " +
                    ");";
            try {
                PreparedStatement stam = db.getStatement(sql);

                if (stam.executeUpdate() == 0) {

                    return true;
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

            return false;
        }
    }

    public class Select {

        public int selectCountIsPresentViaStudents_section_ids(ArrayList<Integer> arr) {
            if(arr.size() == 0) { return 0; }
            ArrayList<String> stringArr = new ArrayList<>();
            for (int i:arr) {
                stringArr.add(i+"");
            }
            String ids = String.join(" , ", stringArr);
            String sql = "SELECT COUNT("+Var.ITEM_id+") AS a " +
                    " FROM "+ Var.TABLE_NAME+" " +
                    " WHERE "+ Var.ITEM_isLate+" = 0 " +
                    " AND "+ Var.ITEM_isAbsent+" = 0 " +
                    " AND "+Var.Students_Section_id+" IN ( "+ids+" ) ";
            try {

                PreparedStatement stam = db.getStatement(sql);

                ResultSet rs = stam.executeQuery();

                if (rs.next()) {
                    return rs.getInt("a");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

            return 0;
        }

        public int selectCountIsLateViaStudents_section_ids(ArrayList<Integer> arr) {
            if(arr.size() == 0) { return 0; }
            ArrayList<String> stringArr = new ArrayList<>();
            for (int i:arr) {
                stringArr.add(i+"");
            }
            String ids = String.join(" , ", stringArr);
            String sql = "SELECT COUNT("+Var.ITEM_id+") AS a " +
                    " FROM "+ Var.TABLE_NAME+" " +
                    " WHERE "+ Var.ITEM_isLate+" = 1 " +
                    " AND "+ Var.ITEM_isAbsent+" = 0 " +
                    " AND "+Var.Students_Section_id+" IN ( "+ids+" ) ";
            try {

                PreparedStatement stam = db.getStatement(sql);

                ResultSet rs = stam.executeQuery();

                if (rs.next()) {
                    return rs.getInt("a");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

            return 0;
        }

        public int selectCountIsAbsentViaStudents_section_ids(ArrayList<Integer> arr) {
            if(arr.size() == 0) { return 0; }
            ArrayList<String> stringArr = new ArrayList<>();
            for (int i:arr) {
                stringArr.add(i+"");
            }
            String ids = String.join(" , ", stringArr);
            String sql = "SELECT COUNT("+Var.ITEM_id+") AS a " +
                    " FROM "+ Var.TABLE_NAME+" " +
                    " WHERE "+ Var.ITEM_isLate+" = 0 " +
                    " AND "+ Var.ITEM_isAbsent+" = 1 " +
                    " AND "+Var.Students_Section_id+" IN ( "+ids+" ) ";
            try {

                PreparedStatement stam = db.getStatement(sql);

                ResultSet rs = stam.executeQuery();

                if (rs.next()) {
                    return rs.getInt("a");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

            return 0;
        }


        /*
        SELECT attendance.*
        FROM attendance,students_section,section
        WHERE attendance.students_section_id = students_section.students_section_id
        AND students_section.section_id = section.section_id
        AND students_section.students_id = 1
        AND students_section.section_id = 1
        AND section.class_id = 1
         */
        public ArrayList<AttendanceModel> selectAllViaStudents_idAndClasses_idAndSections_id(int students_id, int class_id, int section_id) {
            ArrayList<AttendanceModel> arr = new ArrayList<AttendanceModel>();
            String sql = "SELECT "+Var.TABLE_NAME+".* " +
                    " FROM "+Var.TABLE_NAME+","+db_Students_Section.Var.TABLE_NAME+","+db_Section.Var.TABLE_NAME+" " +
                    " WHERE "+Var.TABLE_NAME+"."+Var.Students_Section_id+" = "+db_Students_Section.Var.TABLE_NAME+"."+db_Students_Section.Var.ITEM_id+" " +
                    " AND "+db_Students_Section.Var.TABLE_NAME+"."+db_Students_Section.Var.Section_id+" = "+db_Section.Var.TABLE_NAME+"."+db_Section.Var.ITEM_id+" " +
                    " AND "+db_Students_Section.Var.TABLE_NAME+"."+db_Students_Section.Var.Students_id+" = ? " +
                    " AND "+db_Students_Section.Var.TABLE_NAME+"."+db_Students_Section.Var.Section_id+" = ? " +
                    " AND "+db_Section.Var.TABLE_NAME+"."+db_Section.Var.Class_id+" = ? ;";
            try {
                PreparedStatement stam = db.getStatement(sql);
                stam.setInt(1,students_id);
                stam.setInt(2,section_id);
                stam.setInt(3,class_id);


                ResultSet rs = stam.executeQuery();

                while (rs.next()) {
                    arr.add(toModel(rs));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

            return arr;
        }
    }

    public class Insert {

        public boolean insert(ArrayList<AttendanceModel> arr) {
            String sql = "INSERT INTO " + Var.TABLE_NAME + " "
                    + " ( " + Var.ITEM_id + " , " + Var.ITEM_dateTime + " , " + Var.ITEM_isAbsent + " , " + Var.ITEM_isLate + " , "+Var.Students_Section_id+"  ) "
                    + " VALUES "
                    + " ( NULL , ? , ? , ? , ? )";
            try {
                PreparedStatement stam = db.getStatementWithId(sql);
                String dateTime = LocalDateTime.now().format(formatter);

                for (AttendanceModel i:arr) {
                    stam.setString(1, dateTime);
                    stam.setInt(2, i.isAttendance_isAbsent()?1:0);
                    stam.setInt(3, i.isAttendance_isLate()?1:0);
                    stam.setInt(4, i.getStudents_section_id());

                    stam.addBatch();
                }

                int[] inserted = stam.executeBatch();

                System.out.println("inserted.length = "+inserted.length);
                System.out.println("arr.size() = "+arr.size());

                return true;
            } catch (SQLException e) {
                e.printStackTrace();
            }

            return false;
        }
    }

    public class Update {

    }

    public class Delete {

    }
}
