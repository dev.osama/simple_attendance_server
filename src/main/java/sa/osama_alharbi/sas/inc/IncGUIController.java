package sa.osama_alharbi.sas.inc;

import sa.osama_alharbi.sas.controller.*;

public class IncGUIController {
	public Root root;
	public Students students;
	public Login login;
	public Classes classes;
	public HotBar hotBar;
	public Section section;
	public Server server;
    public Students_AddEdit students_AddEdit;
	public Students_Attendance students_Attendance;
	public Students_Section students_Section;
	public Students_Classes students_Classes;
    public Section_AddEdit section_AddEdit;
	public Section_Attendance section_Attendance;
	public Section_Students section_Students;
    public Classes_AddEdit classes_AddEdit;
	public Classes_Section classes_Section;
	public Classes_Students classes_Students;
	public Classes_Attendance classes_Attendance;
	public Setting setting;
}
