package sa.osama_alharbi.sas.inc;

import javafx.application.Platform;
import sa.osama_alharbi.sas.db.DB;
import sa.osama_alharbi.sas.inc.action.Action;
import sa.osama_alharbi.sas.model.Model;
import sa.osama_alharbi.sas.ws.Server;

public class Inc {
	public static IncGUI gui = new IncGUI();
	public static IncGUIController gui_controller = new IncGUIController();
	public static DB db = new DB();
	public static Action action;
//	public static GUIModel guiModel;
	public static Model model;
	public static Server server;
//	public static IncServer server;
//	public static Report report;
//	public static WS ws;
//	public static ModelWs modelWs;
//	public static WS_Server ws_Server;
//	public static v1_Model ws_ServerModel;
	
	
	public static void start() {
		db.start();
		action = new Action();
		model = new Model();
		server = new Server();
//		ws_ServerModel = new v1_Model();
//		modelWs = new ModelWs();
//		guiModel = new GUIModel();
//		server = new IncServer();
//		report = new Report();
//		ws = new WS();
//		ws_Server = new WS_Server();
		Platform.runLater(()->{
			gui.start();
			Platform.runLater(()->{
				action.start();
			});
//				action.start();
		});
	}
}
