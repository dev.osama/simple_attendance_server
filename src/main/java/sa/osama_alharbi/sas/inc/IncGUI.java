package sa.osama_alharbi.sas.inc;

import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import sa.osama_alharbi.sas.controller.*;
import sa.osama_alharbi.sas.view.Index;

import java.io.IOException;

public class IncGUI {

	public Stage stage;//, stageReport;
	public Scene scene;//, sceneReport;

//	public BorderPane gui_Home;
	public BorderPane gui_Root,gui_Students,gui_Classes,gui_Section,gui_Server,gui_Setting;
	public AnchorPane gui_LodingApp,gui_Login,gui_Students_AddEdit, gui_Students_Classes, gui_Students_Section, gui_Students_Attendance,
			gui_Section_AddEdit,gui_Section_Attendance,gui_Section_Students,
			gui_Classes_AddEdit,gui_Classes_Section,gui_Classes_Students,gui_Classes_Attendance;
	public HBox gui_HotBar;
//	public VBox gui_Home_Dash, gui_studentsSetting_insertStudentsViaExcel;
//	public AnchorPane gui_Login, gui_infoCom, gui_infoCom_Info, gui_infoCom_Password, gui_infoCom_Connect,
//			gui_infoCom_Username, gui_infoCom_Ticket, gui_server, gui_map, gui_students, gui_students_edit,
//			gui_students_trips,gui_Students_Trip_viewer, gui_students_bills, gui_students_schedule, gui_students_query, gui_studentsSetting,gui_Students_Attendance_viewer,
//			gui_students_attendance, gui_buses, gui_buses_edit, gui_buses_trip,gui_buses_trip_viewer, gui_buses_students,gui_buses_students_viewer, gui_buses_employees,gui_buses_employees_viewer,
//			gui_employees, gui_employees_edit, gui_employees_password, gui_employees_trips,gui_employees_trips_viewer, gui_employees_studetns,gui_employees_studetns_viewer,
//			gui_employees_buses,gui_employees_buses_viewer, gui_trip, gui_trip_edit, gui_trip_studetns,gui_trip_studetns_viewer,
//			gui_trip_attendance,gui_trip_mapHistory,gui_LodingApp,popup_StudentsAttendanceViewer,popup_Youtube;
//	public JRViewer jRViewer = null;
//	public JasperPrint jasperPrint = null;

	public IncGUI() {

		FXMLLoader loaderLodingApp = getLoader("LodingApp");
		gui_LodingApp = (AnchorPane) getPain(loaderLodingApp);

		stage = new Stage();
		scene = new Scene(gui_LodingApp);
		stage.setScene(scene);
		stage.show();
	}

	public void start() {
		generateGUI();
	}

	public FXMLLoader getLoader(String path) {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Index.class.getResource(path + ".fxml"));
		return loader;
	}

	public Pane getPain(FXMLLoader loader) {
		try {
			return loader.load();
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	private void generateGUI() {

		FXMLLoader loaderRoot = getLoader("Root");
		gui_Root = (BorderPane) getPain(loaderRoot);
		Inc.gui_controller.root = loaderRoot.<Root>getController();

		FXMLLoader loaderLogin = getLoader("Login");
		gui_Login = (AnchorPane) getPain(loaderLogin);
		Inc.gui_controller.login = loaderLogin.<Login>getController();

		FXMLLoader loaderStudents_AddEdit = getLoader("Students_AddEdit");
		gui_Students_AddEdit = (AnchorPane) getPain(loaderStudents_AddEdit);
		Inc.gui_controller.students_AddEdit = loaderStudents_AddEdit.<Students_AddEdit>getController();

		FXMLLoader loaderStudents_Attendance = getLoader("Students_Attendance");
		gui_Students_Attendance = (AnchorPane) getPain(loaderStudents_Attendance);
		Inc.gui_controller.students_Attendance = loaderStudents_Attendance.<Students_Attendance>getController();

		FXMLLoader loaderStudents_Section = getLoader("Students_Section");
		gui_Students_Section = (AnchorPane) getPain(loaderStudents_Section);
		Inc.gui_controller.students_Section = loaderStudents_Section.<Students_Section>getController();

		FXMLLoader loaderStudents_Classes = getLoader("Students_Classes");
		gui_Students_Classes = (AnchorPane) getPain(loaderStudents_Classes);
		Inc.gui_controller.students_Classes = loaderStudents_Classes.<Students_Classes>getController();

		FXMLLoader loaderStudents = getLoader("Students");
		gui_Students = (BorderPane) getPain(loaderStudents);
		Inc.gui_controller.students = loaderStudents.<Students>getController();

		FXMLLoader loaderClasses_AddEdit = getLoader("Classes_AddEdit");
		gui_Classes_AddEdit = (AnchorPane) getPain(loaderClasses_AddEdit);
		Inc.gui_controller.classes_AddEdit = loaderClasses_AddEdit.<Classes_AddEdit>getController();

		FXMLLoader loaderClasses_Section = getLoader("Classes_Section");
		gui_Classes_Section = (AnchorPane) getPain(loaderClasses_Section);
		Inc.gui_controller.classes_Section = loaderClasses_Section.<Classes_Section>getController();

		FXMLLoader loaderClasses_Students = getLoader("Classes_Students");
		gui_Classes_Students = (AnchorPane) getPain(loaderClasses_Students);
		Inc.gui_controller.classes_Students = loaderClasses_Students.<Classes_Students>getController();

		FXMLLoader loaderClasses_Attendance = getLoader("Classes_Attendance");
		gui_Classes_Attendance = (AnchorPane) getPain(loaderClasses_Attendance);
		Inc.gui_controller.classes_Attendance = loaderClasses_Attendance.<Classes_Attendance>getController();

		FXMLLoader loaderClasses = getLoader("Classes");
		gui_Classes = (BorderPane) getPain(loaderClasses);
		Inc.gui_controller.classes = loaderClasses.<Classes>getController();

		FXMLLoader loaderHotBar = getLoader("HotBar");
		gui_HotBar = (HBox) getPain(loaderHotBar);
		Inc.gui_controller.hotBar = loaderHotBar.<HotBar>getController();

		FXMLLoader loaderSection_AddEdit = getLoader("Section_AddEdit");
		gui_Section_AddEdit = (AnchorPane) getPain(loaderSection_AddEdit);
		Inc.gui_controller.section_AddEdit = loaderSection_AddEdit.<Section_AddEdit>getController();

		FXMLLoader loaderSection_Attendance = getLoader("Section_Attendance");
		gui_Section_Attendance = (AnchorPane) getPain(loaderSection_Attendance);
		Inc.gui_controller.section_Attendance = loaderSection_Attendance.<Section_Attendance>getController();

		FXMLLoader loaderSection_Students = getLoader("Section_Students");
		gui_Section_Students = (AnchorPane) getPain(loaderSection_Students);
		Inc.gui_controller.section_Students = loaderSection_Students.<Section_Students>getController();

		FXMLLoader loaderSection = getLoader("Section");
		gui_Section = (BorderPane) getPain(loaderSection);
		Inc.gui_controller.section = loaderSection.<Section>getController();

		FXMLLoader loaderServer = getLoader("Server");
		gui_Server = (BorderPane) getPain(loaderServer);
		Inc.gui_controller.server = loaderServer.<Server>getController();

		FXMLLoader loaderSetting = getLoader("Setting");
		gui_Setting = (BorderPane) getPain(loaderSetting);
		Inc.gui_controller.setting = loaderSetting.<Setting>getController();
//
////		System.out.println("!!!!!!!!!!!!!1122 1");
//		FXMLLoader loaderStudents_Trip_viewer = getLoader("Students_Trips_viewer");
////		System.out.println("!!!!!!!!!!!!!1122 2");
//		gui_Students_Trip_viewer = (AnchorPane) getPain(loaderStudents_Trip_viewer);
////		System.out.println("!!!!!!!!!!!!!1122 3");
//		Inc.gui_controller.students_trips_viewer = loaderStudents_Trip_viewer.<Students_Trip_viewer>getController();
////		System.out.println("!!!!!!!!!!!!!1122 4");
//
//		FXMLLoader loaderStudents_Trips = getLoader("Students_Trips");
//		gui_students_trips = (AnchorPane) getPain(loaderStudents_Trips);
//		Inc.gui_controller.students_trips = loaderStudents_Trips.<Students_Trips>getController();
//
//		FXMLLoader loaderStudents_Bills = getLoader("Students_Bills");
//		gui_students_bills = (AnchorPane) getPain(loaderStudents_Bills);
//		Inc.gui_controller.students_bills = loaderStudents_Bills.<Students_Bills>getController();
//
//		FXMLLoader loaderStudents_Schedule = getLoader("Students_Schedule");
//		gui_students_schedule = (AnchorPane) getPain(loaderStudents_Schedule);
//		Inc.gui_controller.students_schedule = loaderStudents_Schedule.<Students_Schedule>getController();
//
//		FXMLLoader loaderStudents_Attendance_viewer = getLoader("Students_Attendance_viewer");
//		gui_Students_Attendance_viewer = (AnchorPane) getPain(loaderStudents_Attendance_viewer);
//		Inc.gui_controller.students_attendance_viewer = loaderStudents_Attendance_viewer.<Students_Attendance_viewer>getController();
//
//		FXMLLoader loaderStudents_Attendance = getLoader("Students_Attendance");
//		gui_students_attendance = (AnchorPane) getPain(loaderStudents_Attendance);
//		Inc.gui_controller.students_attendance = loaderStudents_Attendance.<Students_Attendance>getController();
//
//		FXMLLoader loaderStudents_Query = getLoader("Students_Query");
//		gui_students_query = (AnchorPane) getPain(loaderStudents_Query);
//		Inc.gui_controller.students_query = loaderStudents_Query.<Students_Query>getController();
//
//		FXMLLoader loaderStudentsSetting_insertStudentsViaExcel = getLoader("StudentsSetting_insertStudentsViaExcel");
//		gui_studentsSetting_insertStudentsViaExcel = (VBox) getPain(loaderStudentsSetting_insertStudentsViaExcel);
//		Inc.gui_controller.studentsSetting_insertStudentsViaExcel = loaderStudentsSetting_insertStudentsViaExcel
//				.<StudentsSetting_insertStudentsViaExcel>getController();
//
//		FXMLLoader loaderStudentsSetting = getLoader("StudentsSetting");
//		gui_studentsSetting = (AnchorPane) getPain(loaderStudentsSetting);
//		Inc.gui_controller.studentsSetting = loaderStudentsSetting.<StudentsSetting>getController();
//
//		FXMLLoader loaderStudents = getLoader("Students");
//		gui_students = (AnchorPane) getPain(loaderStudents);
//		Inc.gui_controller.students = loaderStudents.<Students>getController();
//
//		FXMLLoader loaderBuses_Edit = getLoader("Buses_Edit");
//		gui_buses_edit = (AnchorPane) getPain(loaderBuses_Edit);
//		Inc.gui_controller.buses_edit = loaderBuses_Edit.<Buses_Edit>getController();
//
//		FXMLLoader loaderBuses_Trips_viewer = getLoader("Buses_Trips_viewer");
//		gui_buses_trip_viewer = (AnchorPane) getPain(loaderBuses_Trips_viewer);
//		Inc.gui_controller.buses_trips_viewer = loaderBuses_Trips_viewer.<Buses_Trip_viewer>getController();
//
//		FXMLLoader loaderBuses_Trips = getLoader("Buses_Trips");
//		gui_buses_trip = (AnchorPane) getPain(loaderBuses_Trips);
//		Inc.gui_controller.buses_trips = loaderBuses_Trips.<Buses_Trips>getController();
//
//		FXMLLoader loaderBuses_Students_viewer = getLoader("Buses_Students_viewer");
//		gui_buses_students_viewer = (AnchorPane) getPain(loaderBuses_Students_viewer);
//		Inc.gui_controller.buses_students_viewer = loaderBuses_Students_viewer.<Buses_Students_viewer>getController();
//
//		FXMLLoader loaderBuses_Students = getLoader("Buses_Students");
//		gui_buses_students = (AnchorPane) getPain(loaderBuses_Students);
//		Inc.gui_controller.buses_students = loaderBuses_Students.<Buses_Students>getController();
//
//		FXMLLoader loaderBuses_Employees_viewer = getLoader("Buses_Employees_viewer");
//		gui_buses_employees_viewer = (AnchorPane) getPain(loaderBuses_Employees_viewer);
//		Inc.gui_controller.buses_employees_viewer = loaderBuses_Employees_viewer.<Buses_Employees_viewer>getController();
//
//		FXMLLoader loaderBuses_Employees = getLoader("Buses_Employees");
//		gui_buses_employees = (AnchorPane) getPain(loaderBuses_Employees);
//		Inc.gui_controller.buses_employees = loaderBuses_Employees.<Buses_Employees>getController();
//
//		FXMLLoader loaderBuses = getLoader("Buses");
//		gui_buses = (AnchorPane) getPain(loaderBuses);
//		Inc.gui_controller.buses = loaderBuses.<Buses>getController();
//
//		FXMLLoader loaderEmployees_Edit = getLoader("Employees_Edit");
//		gui_employees_edit = (AnchorPane) getPain(loaderEmployees_Edit);
//		Inc.gui_controller.employees_edit = loaderEmployees_Edit.<Employees_Edit>getController();
//
//		FXMLLoader loaderEmployees_Password = getLoader("Employees_Password");
//		gui_employees_password = (AnchorPane) getPain(loaderEmployees_Password);
//		Inc.gui_controller.employees_password = loaderEmployees_Password.<Employees_Password>getController();
//
//		FXMLLoader loaderEmployees_Trips_viewer = getLoader("Employees_Trips_viewer");
//		gui_employees_trips_viewer = (AnchorPane) getPain(loaderEmployees_Trips_viewer);
//		Inc.gui_controller.employees_trips_viewer = loaderEmployees_Trips_viewer.<Employees_Trips_viewer>getController();
//
//		FXMLLoader loaderEmployees_Trips = getLoader("Employees_Trips");
//		gui_employees_trips = (AnchorPane) getPain(loaderEmployees_Trips);
//		Inc.gui_controller.employees_trips = loaderEmployees_Trips.<Employees_Trips>getController();
//
//		FXMLLoader loaderEmployees_Students_viewer = getLoader("Employees_Students_viewer");
//		gui_employees_studetns_viewer = (AnchorPane) getPain(loaderEmployees_Students_viewer);
//		Inc.gui_controller.employees_students_viewer = loaderEmployees_Students_viewer.<Employees_Students_viewer>getController();
//
//		FXMLLoader loaderEmployees_Students = getLoader("Employees_Students");
//		gui_employees_studetns = (AnchorPane) getPain(loaderEmployees_Students);
//		Inc.gui_controller.employees_students = loaderEmployees_Students.<Employees_Students>getController();
//
//		FXMLLoader loaderEmployees_Buses_viewer = getLoader("Employees_Buses_viewer");
//		gui_employees_buses_viewer = (AnchorPane) getPain(loaderEmployees_Buses_viewer);
//		Inc.gui_controller.employees_buses_viewer = loaderEmployees_Buses_viewer.<Employees_Buses_viewer>getController();
//
//		FXMLLoader loaderEmployees_Buses = getLoader("Employees_Buses");
//		gui_employees_buses = (AnchorPane) getPain(loaderEmployees_Buses);
//		Inc.gui_controller.employees_buses = loaderEmployees_Buses.<Employees_Buses>getController();
//
//		FXMLLoader loaderEmployees = getLoader("Employees");
//		gui_employees = (AnchorPane) getPain(loaderEmployees);
//		Inc.gui_controller.employees = loaderEmployees.<Employees>getController();
//
//		FXMLLoader loaderTrip_Edit = getLoader("Trip_Edit");
//		gui_trip_edit = (AnchorPane) getPain(loaderTrip_Edit);
//		Inc.gui_controller.trip_edit = loaderTrip_Edit.<Trip_Edit>getController();
//
//		FXMLLoader loaderTrip_Students_viewer = getLoader("Trip_Students_viewer");
//		gui_trip_studetns_viewer = (AnchorPane) getPain(loaderTrip_Students_viewer);
//		Inc.gui_controller.trip_students_viewer = loaderTrip_Students_viewer.<Trip_Students_viewer>getController();
//
//		FXMLLoader loaderTrip_Students = getLoader("Trip_Students");
//		gui_trip_studetns = (AnchorPane) getPain(loaderTrip_Students);
//		Inc.gui_controller.trip_students = loaderTrip_Students.<Trip_Students>getController();
//
//		FXMLLoader loaderTrip_Attendance = getLoader("Trip_Attendance");
//		gui_trip_attendance = (AnchorPane) getPain(loaderTrip_Attendance);
//		Inc.gui_controller.trip_attendance = loaderTrip_Attendance.<Trip_Attendance>getController();
//
//		FXMLLoader loaderTrip_MapHistory = getLoader("Trip_MapHistory");
//		gui_trip_mapHistory = (AnchorPane) getPain(loaderTrip_MapHistory);
//		Inc.gui_controller.trip_mapHistory = loaderTrip_MapHistory.<Trip_MapHistory>getController();
//
//		FXMLLoader loaderTrip = getLoader("Trip");
//		gui_trip = (AnchorPane) getPain(loaderTrip);
//		Inc.gui_controller.trip = loaderTrip.<Trip>getController();
//
//		FXMLLoader loaderInfoCom_Info = getLoader("InfoCom_Info");
//		gui_infoCom_Info = (AnchorPane) getPain(loaderInfoCom_Info);
//		Inc.gui_controller.infoCom_Info = loaderInfoCom_Info.<InfoCom_Info>getController();
//
//		FXMLLoader loaderInfoCom_Password = getLoader("InfoCom_Password");
//		gui_infoCom_Password = (AnchorPane) getPain(loaderInfoCom_Password);
//		Inc.gui_controller.infoCom_Password = loaderInfoCom_Password.<InfoCom_Password>getController();
//
//		FXMLLoader loaderInfoCom_Connect = getLoader("InfoCom_Connect");
//		gui_infoCom_Connect = (AnchorPane) getPain(loaderInfoCom_Connect);
//		Inc.gui_controller.infoCom_Connect = loaderInfoCom_Connect.<InfoCom_Connect>getController();
//
//		FXMLLoader loaderInfoCom_Username = getLoader("InfoCom_Username");
//		gui_infoCom_Username = (AnchorPane) getPain(loaderInfoCom_Username);
//		Inc.gui_controller.infoCom_Username = loaderInfoCom_Username.<InfoCom_Username>getController();
//
//		FXMLLoader loaderInfoCom_Ticket = getLoader("InfoCom_Ticket");
//		gui_infoCom_Ticket = (AnchorPane) getPain(loaderInfoCom_Ticket);
//		Inc.gui_controller.infoCom_Ticket = loaderInfoCom_Ticket.<InfoCom_Ticket>getController();
//
//		FXMLLoader loaderInfoCom = getLoader("InfoCom");
//		gui_infoCom = (AnchorPane) getPain(loaderInfoCom);
//		Inc.gui_controller.infoCom = loaderInfoCom.<InfoCom>getController();
//
//		FXMLLoader loaderServer = getLoader("Server");
//		gui_server = (AnchorPane) getPain(loaderServer);
//		Inc.gui_controller.server = loaderServer.<Server>getController();
//
//		FXMLLoader loaderPopup_StudentsAttendanceViewer = getLoader("Popup_StudentsAttendanceViewer");
//		popup_StudentsAttendanceViewer = (AnchorPane) getPain(loaderPopup_StudentsAttendanceViewer);
//		Inc.gui_controller.popup_StudentsAttendanceViewer = loaderPopup_StudentsAttendanceViewer.<Popup_StudentsAttendanceViewer>getController();
//
//		FXMLLoader loaderPopup_YoutubeViewer = getLoader("Youtube");
//		popup_Youtube = (AnchorPane) getPain(loaderPopup_YoutubeViewer);
//		Inc.gui_controller.popup_Youtube = loaderPopup_YoutubeViewer.<Youtube>getController();
//
//		FXMLLoader loaderMap = getLoader("Map4");
//		gui_map = (AnchorPane) getPain(loaderMap);
//		Inc.gui_controller.map = loaderMap.<Map4>getController();
//
//		scene.setFill(Color.TRANSPARENT);// controlle
//		// varCss.css
//		// mainTap
//		// mainButton
//		// students
//		scene.getStylesheets().add(Index.class.getResource("varCss.css").toExternalForm());
//		scene.getStylesheets().add(Index.class.getResource("buttons.css").toExternalForm());
//		scene.getStylesheets().add(Index.class.getResource("mainTap.css").toExternalForm());
//		scene.getStylesheets().add(Index.class.getResource("students.css").toExternalForm());
//		scene.getStylesheets().add(Index.class.getResource("trip.css").toExternalForm());
//		scene.getStylesheets().add(Index.class.getResource("buses.css").toExternalForm());
//		scene.getStylesheets().add(Index.class.getResource("employees.css").toExternalForm());
//		scene.getStylesheets().add(Index.class.getResource("infoCom.css").toExternalForm());
//		scene.getStylesheets().add(Index.class.getResource("server.css").toExternalForm());
//		scene.getStylesheets().add(Index.class.getResource("main.css").toExternalForm());
//		scene.getStylesheets().add(Index.class.getResource("list.css").toExternalForm());
//
////		scene.getStylesheets().add(Index.class.getResource("controlle.css").toExternalForm());
//		scene.getStylesheets().add(Index.class.getResource("home.css").toExternalForm());
//		scene.getStylesheets().add(Index.class.getResource("css2.css").toExternalForm());
//		rootCss.addListener((obs, o, n) -> {
//			scene.getStylesheets().add(n);
//		});
////		scene.getStylesheets().addAll(Index.class.getResource("css.css").toExternalForm(),Index.class.getResource("css2.css").toExternalForm());
////		stage.setScene(scene);
//		stage.getIcons().add(new Image(sa.osama_alharbi.tmp.gImg.Index.class.getResourceAsStream("appIcon.png")));
		show();
		stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			@Override
			public void handle(WindowEvent event) {
				System.exit(0);
			}
		});

//		stageReport = new Stage();
//		AnchorPane anchorPane = new AnchorPane();
////		swingNode = new SwingNode();
//		jRViewer = new JRViewer(null);
//		sceneReport = new Scene(anchorPane);
//		stageReport.setScene(sceneReport);
//
//		Inc.gui.stageReport.setOnCloseRequest(new EventHandler<WindowEvent>() {
//			@Override
//			public void handle(WindowEvent event) {
//				Inc.gui.jRViewer.setVisible(true);
//				Inc.gui.jRViewer = null;
//				System.gc();
//			}
//		});
		Platform.setImplicitExit(true);
	}

	public StringProperty rootCss = new SimpleStringProperty();

	private void show() {
		scene.setRoot(gui_Root);
		gui_Root.setCenter(gui_Login);
		stage.setMaximized(true);
		if (!stage.isShowing()) {
			stage.show();
		}
	}

	public static class tools {
		private static double x = 0, y = 0;

		public static void nodePressed(MouseEvent event) {
			x = event.getSceneX();
			y = event.getSceneY();
		}

		public static void nodeDragged(MouseEvent event) {
			if (Inc.gui.stage.isMaximized()) {
				return;
			}

			Node node = (Node) event.getSource();
			Stage stage1 = (Stage) node.getScene().getWindow();
			stage1.setX(event.getScreenX() - x);
			stage1.setY(event.getScreenY() - y);
		}
	}

	public static class guiShow {
//		public static void home() {
//			Inc.gui.gui_Home.setCenter(Inc.gui.gui_Home_Dash);
//		}
//
//		public static void logout() {
//			Inc.gui.scene.setRoot(Inc.gui.gui_Login);
//		}

//		public static void login() {
//			Inc.gui.scene.setRoot(Inc.gui.gui_Root);
////			home();
//		}

//		public static void addStudents() {
//			Inc.gui.scene.setRoot(Inc.gui.gui_addStudents);
//		}
//		public static void editStudents() {
//			Inc.gui.scene.setRoot(Inc.gui.gui_studentsMnagementEdit);
//		}
//		public static void addEmployees() {
//			Inc.gui.scene.setRoot(Inc.gui.gui_addEmployees);
//		}
//		public static void students() {
//			Inc.gui.gui_Home.setCenter(Inc.gui.gui_students);
////			Inc.gui.scene.setRoot(Inc.gui.gui_students);
//		}
//
//		public static void employees() {
//			Inc.gui.gui_Home.setCenter(Inc.gui.gui_employees);
////			Inc.gui.scene.setRoot(Inc.gui.gui_employees);
//		}
//
//		public static void trip() {
//			Inc.gui.gui_Home.setCenter(Inc.gui.gui_trip);
////			Inc.gui.scene.setRoot(Inc.gui.gui_trip);
//		}
//
//		public static void buses() {
//			Inc.gui.gui_Home.setCenter(Inc.gui.gui_buses);
////			Inc.gui.scene.setRoot(Inc.gui.gui_buses);
//		}
//
//		public static void infoCom() {
//			Inc.gui.gui_Home.setCenter(Inc.gui.gui_infoCom);
////			Inc.gui.scene.setRoot(Inc.gui.gui_infoCom);
////			Inc.gui_controller.infoCom.reset();
//		}
//
//		public static void server() {
//			Inc.gui.gui_Home.setCenter(Inc.gui.gui_server);
////			Inc.gui.scene.setRoot(Inc.gui.gui_server);
//		}
//
//		public static void map() {
//			Inc.gui.gui_Home.setCenter(Inc.gui.gui_map);
////			Inc.gui.scene.setRoot(Inc.gui.gui_map);
//		}

//		public static void busesaMnagement() {
//			Inc.gui.scene.setRoot(Inc.gui.gui_busesMnagement);
//		}
//		public static void employeesMnagement() {
//			Inc.gui.scene.setRoot(Inc.gui.gui_employeesMnagement);
//		}
//		public static void studentsMnagement() {
//			Inc.gui.scene.setRoot(Inc.gui.gui_studentsMnagement);
//		}
		public static void exit() {
			System.exit(0);
		}

		public static void maxmize() {
//			if(Inc.gui.stage.isMaximized()) {
//				
//			    Inc.gui.stage.setMaximized(!Inc.gui.stage.isMaximized());
//			    return;
//			}
//			Screen screen = Screen.getPrimary();
//		    Rectangle2D bounds = screen.getVisualBounds();
//		    System.out.println(bounds.getWidth()+" - "+bounds.getHeight());
//		    Inc.gui.stage.setWidth(bounds.getWidth());
//		    Inc.gui.stage.setHeight(bounds.getHeight());

			Screen screen = Screen.getPrimary();
			Rectangle2D bounds = screen.getVisualBounds();

			if (Inc.gui.stage.getX() == bounds.getMinX() && Inc.gui.stage.getY() == bounds.getMinY()
					&& Inc.gui.stage.getWidth() == bounds.getWidth()
					&& Inc.gui.stage.getHeight() == bounds.getWidth()) {

				Inc.gui.stage.setX(bounds.getMinX() + 100);
				Inc.gui.stage.setY(bounds.getMinY() + 50);
				Inc.gui.stage.setWidth(bounds.getWidth() - 500);
				Inc.gui.stage.setHeight(bounds.getHeight() - 500);
			} else {
				Inc.gui.stage.setX(bounds.getMinX());
				Inc.gui.stage.setY(bounds.getMinY());
				Inc.gui.stage.setWidth(bounds.getWidth());
				Inc.gui.stage.setHeight(bounds.getHeight());
			}

		}

		public static void minimize() {
			Inc.gui.stage.setIconified(true);
		}

//		public static void back() {
//			home();
//		}
	}
}
