package sa.osama_alharbi.sas.inc.action;

import sa.osama_alharbi.sas.inc.Inc;
import sa.osama_alharbi.sas.model.StudentsModel;

import java.util.ArrayList;

public class Setting {
    public Edit edit;
    public Setting(){
        this.edit = new Edit();
    }

    public void start() {
        Inc.model.setting.add.add(Inc.db.tables.setting.select.select());
        Inc.gui_controller.server.reset();
    }

    public class Edit{
        public boolean edit_usernameAndPassword(String username,String password){
            if(Inc.db.tables.setting.update.update_usernameAndPassword(username,password)){
                Inc.model.setting.edit.edit_usernameAndPassword(username,password);
                return true;
            }else{
                return false;
            }
        }
        public boolean edit_serverPasswordPort(String serverPassword,int serverPort){
            if(Inc.db.tables.setting.update.update_serverPasswordPort(serverPassword,serverPort)){
                Inc.model.setting.edit.edit_serverPasswordPort(serverPassword,serverPort);
                return true;
            }else{
                return false;
            }
        }
    }
}
