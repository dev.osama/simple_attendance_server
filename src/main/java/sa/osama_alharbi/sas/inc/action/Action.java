package sa.osama_alharbi.sas.inc.action;

public class Action {
    public LogInOut logInOut;
    public HotBar hotBar;
    public Lisner lisner;
    public Students students;
    public Classes classes;
    public Section section;
    public StudentsSection studentsSection;
    public Setting setting;
    public Attendance attendance;
    public Action(){
        logInOut = new LogInOut();
        hotBar = new HotBar();
        lisner = new Lisner();
        students = new Students();
        classes = new Classes();
        section = new Section();
        studentsSection = new StudentsSection();
        setting = new Setting();
        attendance = new Attendance();
    }

    public void start(){
        students.start();
        classes.start();
        section.start();
        studentsSection.start();
        setting.start();
    }
}
