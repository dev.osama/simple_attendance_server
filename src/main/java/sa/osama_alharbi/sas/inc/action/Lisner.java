package sa.osama_alharbi.sas.inc.action;

import sa.osama_alharbi.sas.inc.Inc;
import sa.osama_alharbi.sas.model.ClassesModel;
import sa.osama_alharbi.sas.model.SectionModel;
import sa.osama_alharbi.sas.model.StudentsModel;
import sa.osama_alharbi.sas.model.StudentsSectionModel;

public class Lisner {
    public Students students;
    public Classes classes;
    public Section section;
    public StudentsSection studentsSection;
    public Lisner() {
        this.students = new Students();
        this.classes = new Classes();
        this.section = new Section();
        this.studentsSection = new StudentsSection();
    }

    public class Students {
        public void onAddModel(StudentsModel model) {
            Inc.gui_controller.students.data.addStudentsModel(model);
        }
        public void onRemoveModel(StudentsModel model) {
            Inc.gui_controller.students.data.deleteStudentsModel(model);
        }
    }
    public class Classes {
        public void onAddModel(ClassesModel model) {
            Inc.gui_controller.classes.data.addClassesModel(model);
            Inc.gui_controller.students_Classes.data.addClassesModel(model);
            Inc.gui_controller.students_Section.data.addClassesModel(model);
            Inc.gui_controller.students_Attendance.data.addClassesModel(model);
            Inc.gui_controller.section_AddEdit.data.addClassesModel(model);
        }
        public void onRemoveModel(ClassesModel model) {
            Inc.gui_controller.classes.data.deleteClassesModel(model);
            Inc.gui_controller.students_Classes.data.deleteClassesModel(model);
            Inc.gui_controller.students_Section.data.deleteClassesModel(model);
            Inc.gui_controller.students_Attendance.data.deleteClassesModel(model);
            Inc.gui_controller.section_AddEdit.data.deleteClassesModel(model);
        }
    }

    public class Section{
        public void onAddModel(SectionModel model) {
            Inc.gui_controller.students_Attendance.data.addSectionModel(model);
            Inc.gui_controller.students_Section.data.addSectionModel(model);
            Inc.gui_controller.section.data.addSectionModel(model);
            Inc.gui_controller.classes_Section.data.addSectionModel(model);
            Inc.gui_controller.classes_Students.data.addSectionModel(model);
            Inc.gui_controller.classes_Attendance.data.addSectionModel(model);
        }
        public void onRemoveModel(SectionModel model) {
            Inc.gui_controller.students_Attendance.data.deleteSectionModel(model);
            Inc.gui_controller.students_Section.data.deleteSectionModel(model);
            Inc.gui_controller.section.data.deleteSectionModel(model);
            Inc.gui_controller.classes_Section.data.deleteSectionModel(model);
            Inc.gui_controller.classes_Students.data.deleteSectionModel(model);
            Inc.gui_controller.classes_Attendance.data.deleteSectionModel(model);
        }
    }

    public class StudentsSection {
        public void onAddModel(StudentsSectionModel model) {
//            Inc.gui_controller.classes_Attendance.data.addSectionModel(model);
            Inc.gui_controller.classes_Students.data.addStudentsSectionModel(model);
            Inc.gui_controller.section_Students.data.addStudentsSectionModel(model);
        }
        public void onRemoveModel(StudentsSectionModel model) {
//            Inc.gui_controller.students.data.deleteStudentsModel(model);
            Inc.gui_controller.classes_Students.data.deleteStudentsSectionModel(model);
            Inc.gui_controller.section_Students.data.deleteStudentsSectionModel(model);
        }
    }
}
