package sa.osama_alharbi.sas.inc.action;

import sa.osama_alharbi.sas.inc.Inc;
import sa.osama_alharbi.sas.model.SectionModel;
import sa.osama_alharbi.sas.model.StudentsModel;
import sa.osama_alharbi.sas.model.StudentsSectionModel;

import java.util.ArrayList;

public class StudentsSection {
    public Add add;
    public Edit edit;
    public Delete delete;
    public StudentsSection(){
        this.add = new Add();
        this.edit = new Edit();
        this.delete = new Delete();
    }

    public void start() {
        ArrayList<StudentsSectionModel> arr = Inc.db.tables.students_section.select.selectAll();
        Inc.model.studentsSection.get.getObservableMap().clear();
        Inc.model.studentsSection.add.add(arr);
    }

    public class Add{
        public boolean addNewSection(String section_name,String section_password,int class_id){
//            SectionModel model = Inc.db.tables.section.insert.insert(section_name,section_password,class_id);
//            if(model != null){
//                Inc.model.section.add.add(model);
//                return true;
//            }else{
//                return false;
//            }
            return true;
        }

        public StudentsSectionModel addNewStudentsSection(StudentsModel studentsModel, SectionModel sectionModel) {
            StudentsSectionModel model = Inc.db.tables.students_section.insert.insert(sectionModel.getSection_id(),studentsModel.getStudents_id());
            if(model != null){
                Inc.model.studentsSection.add.add(model);
            }
            return model;
        }
    }

    public class Edit{
//        public boolean editSection(SectionModel model,String section_name,String section_password){
//            if(Inc.db.tables.section.update.updateSection_name_andPassword(model.getClass_id(),section_name,section_password)){
//                Inc.model.section.edit.editSection_nameAndPassword(model,section_name,section_password);
//                return true;
//            }else{
//                return false;
//            }
//        }
    }

    public class Delete{
        public boolean deleteStudentsSection(StudentsSectionModel studentsSectionModel) {
            if(Inc.db.tables.students_section.update.updateStudents_Section_isDelete(studentsSectionModel.getStudents_section_id(),true)){
                Inc.model.studentsSection.edit.editStudents_Section_isDelete(studentsSectionModel,true);
                return true;
            }
            return false;
        }
    }
}
