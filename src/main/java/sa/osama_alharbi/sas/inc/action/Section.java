package sa.osama_alharbi.sas.inc.action;

import sa.osama_alharbi.sas.inc.Inc;
import sa.osama_alharbi.sas.model.SectionModel;

import java.util.ArrayList;

public class Section {
    public Add add;
    public Edit edit;
    public Delete delete;
    public Section(){
        this.add = new Add();
        this.edit = new Edit();
        this.delete = new Delete();
    }

    public void start() {
        ArrayList<SectionModel> arr = Inc.db.tables.section.select.selectAll();
        Inc.model.section.get.getObservableMap().clear();
        Inc.model.section.add.add(arr);
    }

    public class Add{
        public boolean addNewSection(String section_name,String section_password,int class_id){
            SectionModel model = Inc.db.tables.section.insert.insert(section_name,section_password,class_id);
            if(model != null){
                Inc.model.section.add.add(model);
                return true;
            }else{
                return false;
            }
        }
    }

    public class Edit{
        public boolean editSection(SectionModel model,String section_name,String section_password){
            if(Inc.db.tables.section.update.updateSection_name_andPassword(model.getClass_id(),section_name,section_password)){
                Inc.model.section.edit.editSection_nameAndPassword(model,section_name,section_password);
                return true;
            }else{
                return false;
            }
        }
    }

    public class Delete{
        public boolean deleteSection(SectionModel model){
            //delete students_section
            //delete section
            if(Inc.db.tables.students_section.delete.deleteStudents_viaSection_id(model.getSection_id())){
                Inc.model.studentsSection.delete.deleteViaSection_id(model.getSection_id());
                if(Inc.db.tables.section.delete.deleteSection(model.getSection_id())){
                    Inc.model.section.delete.forceDelete(model.getSection_id());
                    return true;
                }else {
                    return false;
                }
            }else{
                return false;
            }
        }
    }
}
