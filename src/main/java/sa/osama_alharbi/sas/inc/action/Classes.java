package sa.osama_alharbi.sas.inc.action;

import sa.osama_alharbi.sas.inc.Inc;
import sa.osama_alharbi.sas.model.ClassesModel;
import sa.osama_alharbi.sas.model.StudentsModel;

import java.util.ArrayList;

public class Classes {
    public Add add;
    public Edit edit;
    public Delete delete;
    public Classes(){
        this.add = new Add();
        this.edit = new Edit();
        this.delete = new Delete();
    }

    public void start() {
        ArrayList<ClassesModel> arr = Inc.db.tables.class_.select.selectAll();
        Inc.model.classes.get.getObservableMap().clear();
        Inc.model.classes.add.add(arr);
    }

    public class Add{
        public boolean addNewClasses(String classes_name){
            ClassesModel model = Inc.db.tables.class_.insert.insert(classes_name);
            if(model != null){
                Inc.model.classes.add.add(model);
                return true;
            }else{
                return false;
            }
        }
    }

    public class Edit{
        public boolean editClasses(ClassesModel model,String classes_name){
            if(Inc.db.tables.class_.update.updateClass_name(model.getClass_id(),classes_name)){
                Inc.model.classes.edit.editClasses_name(model,classes_name);
                return true;
            }else{
                return false;
            }
        }
    }

    public class Delete{
        public boolean deleteClasses(ClassesModel model){
            if(Inc.db.tables.students_section.delete.deleteStudents_viaClasses_id(model.getClass_id())){
                Inc.model.studentsSection.delete.deleteViaClasses_id(model.getClass_id());
                if(Inc.db.tables.section.delete.deleteSectionViaClasses_id(model.getClass_id())){
                    Inc.model.section.delete.deleteViaClasses_id(model.getClass_id());
                    if(Inc.db.tables.class_.delete.deleteClass(model.getClass_id())){
                        Inc.model.classes.delete.forceDelete(model);
                        return true;
                    }else{
                        return false;
                    }
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }
    }
}
