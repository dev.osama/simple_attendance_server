package sa.osama_alharbi.sas.inc.action;

import sa.osama_alharbi.sas.inc.Inc;
import sa.osama_alharbi.sas.model.AttendanceModel;
import sa.osama_alharbi.sas.model.SectionModel;

import java.util.ArrayList;

public class Attendance {
    public Add add;
    public Attendance(){
        this.add = new Add();
    }

    public class Add{
        public boolean add(ArrayList<AttendanceModel> arr){
            return Inc.db.tables.attendance.insert.insert(arr);
        }
    }
}
