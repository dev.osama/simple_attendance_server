package sa.osama_alharbi.sas.inc.action;

import sa.osama_alharbi.sas.inc.Inc;
import sa.osama_alharbi.sas.model.StudentsModel;

import java.util.ArrayList;

public class Students {
    public Add add;
    public Edit edit;
    public Delete delete;
    public Students(){
        this.add = new Add();
        this.edit = new Edit();
        this.delete = new Delete();
    }

    public void start() {
        ArrayList<StudentsModel> arr = Inc.db.tables.students.select.selectAll();
        Inc.model.students.get.getObservableMap().clear();
        Inc.model.students.add.add(arr);
    }

    public class Add{
        public boolean addNewStudents(String students_name){
            StudentsModel model = Inc.db.tables.students.insert.insert(students_name);
            if(model != null){
                Inc.model.students.add.add(model);
                return true;
            }else{
                return false;
            }
        }
    }

    public class Edit{
        public boolean editStudents(StudentsModel model,String students_name){
            if(Inc.db.tables.students.update.updateStudents_name(model.getStudents_id(),students_name)){
                Inc.model.students.edit.editStudents_name(model,students_name);
                return true;
            }else{
                return false;
            }
        }
    }

    public class Delete{
        public boolean deleteStudents(StudentsModel model){
            if(Inc.db.tables.students.delete.deleteStudents(model.getStudents_id())){
                Inc.model.students.delete.delete(model);
                if (Inc.db.tables.students_section.delete.deleteStudents_viaStudents_id(model.getStudents_id())){
                    Inc.model.studentsSection.delete.deleteViaStudents_id(model.getStudents_id());
                }
                return true;
            }else{
                return false;
            }
        }
    }
}
