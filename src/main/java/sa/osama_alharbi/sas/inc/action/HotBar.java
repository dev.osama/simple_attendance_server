package sa.osama_alharbi.sas.inc.action;

import sa.osama_alharbi.sas.inc.Inc;

public class HotBar {
    public void showStudents(){
        Inc.gui.gui_Root.setCenter(Inc.gui.gui_Students);
    }
    public void showClasses(){
        Inc.gui.gui_Root.setCenter(Inc.gui.gui_Classes);
    }
    public void showStudentsClasses(){
        Inc.gui.gui_Root.setCenter(Inc.gui.gui_Section);
    }
    public void showServer(){
        Inc.gui.gui_Root.setCenter(Inc.gui.gui_Server);
    }

    public void showSetting() {
        Inc.gui.gui_Root.setCenter(Inc.gui.gui_Setting);
    }
}
