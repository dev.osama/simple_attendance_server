package sa.osama_alharbi.sas.controller;

import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import sa.osama_alharbi.sas.img.Index;
import sa.osama_alharbi.sas.model.ClassesModel;
import sa.osama_alharbi.sas.model.SectionModel;

import java.net.URL;
import java.util.ResourceBundle;

public class Students_Classes_listItem implements Initializable {

    @FXML public Label lblTitle;
    @FXML public HBox root;
    @FXML public ImageView imgYesNo;
    private Image yes,no;

//    public ObservableList<SectionModel> arrSectionModel;
    private ClassesModel classesModel = null;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
//        arrSectionModel = FXCollections.observableArrayList();
        yes = new Image(Index.class.getResourceAsStream("checkbox_yes.png"));
        no = new Image(Index.class.getResourceAsStream("checkbox_no.png"));
        imgYesNo.setImage(no);
//        lblYesNo.setText("X");

//        arrSectionModel.addListener(new ListChangeListener<SectionModel>() {
//            @Override
//            public void onChanged(Change<? extends SectionModel> c) {
//                if(c.getList().size() > 0){
//                    lblYesNo.setText("GOOD");
//                }else{
//                    lblYesNo.setText("X");
//                }
//            }
//        });
    }


    public void show(boolean isHaveSectionModel){
        if(isHaveSectionModel){
//            lblYesNo.setText("GOOD");
            imgYesNo.setImage(yes);
        }else{
//            lblYesNo.setText("X");
            imgYesNo.setImage(no);
        }
    }


//    public void addSectionModel(SectionModel sectionModel){
//        if(sectionModel != null){
//            if(sectionModel.getClassesModel().getClass_id() == classesModel.getClass_id()){
//                boolean isSameSectionModel = false;
//                for (SectionModel i:arrSectionModel) {
//                    if(sectionModel.getSection_id() == i.getSection_id()){
//                        isSameSectionModel = true;
//                    }
//                }
//                if(!isSameSectionModel){
//                    arrSectionModel.add(sectionModel);
//                }
//            }
//        }
//    }
//    public void clearSectionModel(){
//        arrSectionModel.clear();
//    }

    public void setClassesModel(ClassesModel classesModel){
        this.classesModel = classesModel;
        if(lblTitle.textProperty().isBound()){
            lblTitle.textProperty().unbind();
        }
        lblTitle.textProperty().bind(this.classesModel.class_nameProperty());
    }

    public ClassesModel getClassesModel() {
        return this.classesModel;
    }
}
