package sa.osama_alharbi.sas.controller;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import sa.osama_alharbi.sas.inc.Inc;
import sa.osama_alharbi.sas.model.ClassesModel;

import java.net.URL;
import java.util.ResourceBundle;

public class Classes_AddEdit implements Initializable {
    @FXML public TextField txtClassName;
    @FXML public Button btnAddEdit,btnReset;
    @FXML public HBox boxAddEditReset;

    private BooleanProperty isAdd;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        isAdd = new SimpleBooleanProperty(false);
        btnReset.setText("اعادة تعين");
        btnAddEdit.setText("تعديل");
        isAdd.addListener((observable, oldValue, isAdd) -> {
            boxAddEditReset.getChildren().clear();
            if(isAdd){
                btnAddEdit.setText("إضافة");
                boxAddEditReset.getChildren().add(btnAddEdit);
            }else{
                btnAddEdit.setText("تعديل");
                boxAddEditReset.getChildren().add(btnAddEdit);
                boxAddEditReset.getChildren().add(btnReset);
            }
        });
    }

    private ClassesModel model;
    public void setModel(ClassesModel model){
        if(model != null){
            txtClassName.setText(model.getClass_name());
        }else{
            txtClassName.setText("");
        }
        this.model = model;
    }

    public void parintGuiRady(){
        isAdd.setValue(Inc.gui_controller.classes.isAdd.getValue());
        isAdd.bind(Inc.gui_controller.classes.isAdd);
        isAdd.addListener((observable, oldValue, isAdd) -> {
            if(isAdd){
                txtClassName.setText("");
            }
        });
        Inc.gui_controller.classes.isAdd.setValue(true);
    }

    @FXML public void onClickAddEdit(ActionEvent event) {
        if(isAdd.get()){
            //add
            if(txtClassName.getText().isEmpty()){
                //show alert is empty
            }else{
                //send to action
                if(Inc.action.classes.add.addNewClasses(txtClassName.getText())){
                    Inc.gui_controller.classes.lst.getSelectionModel().selectLast();
                }
            }
        }else{
            //edit
            if(txtClassName.getText().equals(model)){
                //show alert nothing to do
            }else{
                //send to action
                if(Inc.action.classes.edit.editClasses(model,txtClassName.getText())){
                    if(model != null){
                        txtClassName.setText(model.getClass_name());
                    }
                }
            }
        }
    }

    @FXML public void onClickReset(){
        if(model != null){
            txtClassName.setText(model.getClass_name());
        }else{
            txtClassName.setText("");
        }
    }
}
