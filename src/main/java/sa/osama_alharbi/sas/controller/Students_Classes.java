package sa.osama_alharbi.sas.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.layout.HBox;
import sa.osama_alharbi.sas.inc.Inc;
import sa.osama_alharbi.sas.model.ClassesModel;
import sa.osama_alharbi.sas.model.SectionModel;
import sa.osama_alharbi.sas.model.StudentsModel;
import sa.osama_alharbi.sas.model.StudentsSectionModel;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ResourceBundle;

public class Students_Classes implements Initializable {

    @FXML public ListView<Students_Classes_listItem> list;
    public ObservableList<Students_Classes_listItem> obsList;
    public Data data = null;

    @FXML public Label lblClassName,lblNumPresint,lblNumAbsent,lblNumTotalPresintAbsent,lblNumStudentsClasses,lblNumLate;

    public Students_Classes(){
        obsList = FXCollections.observableArrayList();
        data = new Data(obsList);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        list.setItems(obsList);

        list.setCellFactory(param -> {
            return new ListCell<>(){
                @Override
                protected void updateItem(Students_Classes_listItem item, boolean empty) {
                    super.updateItem(item, empty);
                    if(item != null && !empty){
                        setGraphic(item.root);
                    }else{
                        setGraphic(null);
                    }
                }
            };
        });

        list.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            setClassesModel(newValue);
        });

        lblClassName.setText("");
        lblNumPresint.setText("");
        lblNumAbsent.setText("");
        lblNumTotalPresintAbsent.setText("");
        lblNumStudentsClasses.setText("");

    }

    private Students_Classes_listItem item;
    private void setClassesModel(Students_Classes_listItem item){
        this.item = item;
        show();
    }

    private void show(){
        //unBind
        if(lblClassName.textProperty().isBound()){lblClassName.textProperty().unbind();}
        if(lblNumPresint.textProperty().isBound()){lblClassName.textProperty().unbind();}
        if(lblNumAbsent.textProperty().isBound()){lblClassName.textProperty().unbind();}
        if(lblNumTotalPresintAbsent.textProperty().isBound()){lblClassName.textProperty().unbind();}
        if(lblNumStudentsClasses.textProperty().isBound()){lblClassName.textProperty().unbind();}

        if(item != null){
            lblClassName.textProperty().bind(item.getClassesModel().class_nameProperty());

            if(studentsModel != null){
                ArrayList<StudentsSectionModel> studentsSectionModelArrayList = Inc.model.studentsSection.get.getAllByStudents_idAndClasses_id(studentsModel.getStudents_id(),item.getClassesModel().getClass_id());
                ArrayList<Integer> studentsSection_idsArr = new ArrayList<>();
                for (StudentsSectionModel i:studentsSectionModelArrayList) {
                    studentsSection_idsArr.add(i.getStudents_section_id());
                }

                int present = Inc.db.tables.attendance.select.selectCountIsPresentViaStudents_section_ids(studentsSection_idsArr);
                int late = Inc.db.tables.attendance.select.selectCountIsLateViaStudents_section_ids(studentsSection_idsArr);
                int absent = Inc.db.tables.attendance.select.selectCountIsAbsentViaStudents_section_ids(studentsSection_idsArr);
                int total = present + late + absent;

                lblNumPresint.setText(present+"");
                lblNumLate.setText(late+"");
                lblNumAbsent.setText(absent+"");
                lblNumTotalPresintAbsent.setText(total+"");
                lblNumStudentsClasses.setText(studentsSectionModelArrayList.size()+"");
            }else{
                lblNumPresint.setText("");
                lblNumLate.setText("");
                lblNumAbsent.setText("");
                lblNumTotalPresintAbsent.setText("");
                lblNumStudentsClasses.setText("0");
            }
        }else{
            lblClassName.setText("");
            lblNumPresint.setText("");
            lblNumLate.setText("");
            lblNumAbsent.setText("");
            lblNumTotalPresintAbsent.setText("");
            lblNumStudentsClasses.setText("");
        }
    }

    private void clearSelect(){

    }

    private StudentsModel studentsModel = null;
    public void setStudentsModel(StudentsModel studentsModel) {
        this.studentsModel = studentsModel;
        for (Students_Classes_listItem i:obsList) {
            i.show(false);
        }
        if(studentsModel != null){

            ArrayList<ClassesModel> arr = Inc.model.classes.get.getAllClassesByStudents_id(studentsModel.getStudents_id());

            for (Students_Classes_listItem item:obsList) {
                item.show(arr.contains(item.getClassesModel()));
            }
        }
        show();
    }


    public class Data{
        private ObservableList<Students_Classes_listItem> obs = null;
        public Data(ObservableList<Students_Classes_listItem> obs){
            this.obs = obs;
        }
        public void addClassesModel(ClassesModel model){
            boolean isThereClassesModel = false;
            for (Students_Classes_listItem itm: obs) {
                if(itm.getClassesModel().getClass_id() == model.getClass_id()){
                    isThereClassesModel = true;
                }
            }

            if(!isThereClassesModel){
                FXMLLoader looder = Inc.gui.getLoader("Students_Classes_listItem");
                HBox pan = (HBox) Inc.gui.getPain(looder);
                Students_Classes_listItem cont = looder.<Students_Classes_listItem>getController();
                cont.setClassesModel(model);

                this.obs.add(cont);
            }

        }
        public void deleteClassesModel(ClassesModel model){
            Students_Classes_listItem itm = null;
            for (Students_Classes_listItem i: obs) {
                if(i.getClassesModel().getClass_id() == model.getClass_id()){
                    itm = i;
                }
            }

            if(itm != null){
                this.obs.remove(itm);
            }

        }
    }

}
