package sa.osama_alharbi.sas.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import sa.osama_alharbi.sas.img.Index;
import sa.osama_alharbi.sas.model.SectionModel;
import sa.osama_alharbi.sas.model.StudentsSectionModel;

import java.net.URL;
import java.util.ResourceBundle;

public class Students_Section_listItem  implements Initializable {

    @FXML
    public Label  lblClassTitle, lblSectionTitle;
    @FXML public ImageView imgYesNo;
    private Image yes,no;
    @FXML
    public HBox root;

    private StudentsSectionModel studentsSectionModel = null;
    private SectionModel sectionModel = null;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        yes = new Image(Index.class.getResourceAsStream("checkbox_yes.png"));
        no = new Image(Index.class.getResourceAsStream("checkbox_no.png"));
        imgYesNo.setImage(no);
//        lblYesNo.setText("X");
    }

    public StudentsSectionModel getStudentsSectionModel() {
        return studentsSectionModel;
    }

    public void setStudentsSectionModel(StudentsSectionModel studentsSectionModel) {
        this.studentsSectionModel = studentsSectionModel;
        if (this.studentsSectionModel != null) {
            if (this.studentsSectionModel.getSectionModel().getSection_id() == sectionModel.getSection_id()) {
//                lblYesNo.setText("GOOD");
                imgYesNo.setImage(yes);
            } else {
//                lblYesNo.setText("X");
                imgYesNo.setImage(no);
            }
        } else {
//            lblYesNo.setText("X");
            imgYesNo.setImage(no);
        }
    }

    public boolean isMustDeleteDisabled(){
        return this.studentsSectionModel == null;
    }

    public boolean isInSection(){
        if(studentsSectionModel == null){
            return false;
        }
        return this.studentsSectionModel.getSectionModel().getSection_id() == sectionModel.getSection_id();
    }

    public void setSectionModel(SectionModel sectionModel) {
        this.sectionModel = sectionModel;
        if (lblClassTitle.textProperty().isBound()) {
            lblClassTitle.textProperty().unbind();
        }
        if (lblSectionTitle.textProperty().isBound()) {
            lblSectionTitle.textProperty().unbind();
        }
        lblClassTitle.textProperty().bind(this.sectionModel.getClassesModel().class_nameProperty());
        lblSectionTitle.textProperty().bind(this.sectionModel.section_nameProperty());
    }

    public SectionModel getSectionModel() {
        return this.sectionModel;
    }
}