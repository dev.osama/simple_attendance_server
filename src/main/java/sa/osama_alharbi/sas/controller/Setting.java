package sa.osama_alharbi.sas.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import sa.osama_alharbi.sas.inc.Inc;

public class Setting {

    @FXML public TextField txtUsername;

    @FXML public PasswordField txtOldPassword,txtNewPassword,txtReNewPassword;

    @FXML public Button btnSave;

    @FXML public void onClickSave(ActionEvent event) {
        if(!txtUsername.getText().isEmpty() && !txtOldPassword.getText().isEmpty() &&
                !txtNewPassword.getText().isEmpty() && !txtReNewPassword.getText().isEmpty()){
            if(txtOldPassword.getText().equals(Inc.model.setting.get.get().getSetting_password())){
                if(txtNewPassword.getText().equals(txtReNewPassword.getText())){
                    if(Inc.action.setting.edit.edit_usernameAndPassword(txtUsername.getText(),txtNewPassword.getText())){
                        txtUsername.setText("");
                        txtOldPassword.setText("");
                        txtNewPassword.setText("");
                        txtReNewPassword.setText("");
                    }else{
                        //db is error
                    }
                }else{
                    //txtNewPassword is not equals txtReNewPassword
                }
            }else{
                //txtOldPassword is not equals Setting_password
            }
        }else {
            //one of tham are empty
        }
    }

    @FXML public void onClickClearAttendanc(ActionEvent event) {

    }

    @FXML public void onClickClearClasses(ActionEvent event) {

    }

    @FXML public void onClickClearSections(ActionEvent event) {

    }

    @FXML public void onClickClearStudents(ActionEvent event) {

    }

}
