package sa.osama_alharbi.sas.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import sa.osama_alharbi.sas.inc.Inc;

public class Login {
    @FXML
    public TextField txtUsername;

    @FXML
    public PasswordField txtPassword;

    @FXML
    public void onClickLogin(ActionEvent event) {
        txtUsername.setText("a");
        txtPassword.setText("a");
        Inc.action.logInOut.login(txtUsername.getText(),txtPassword.getText());
    }
}
