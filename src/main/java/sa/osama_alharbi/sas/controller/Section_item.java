package sa.osama_alharbi.sas.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import sa.osama_alharbi.sas.img.Index;
import sa.osama_alharbi.sas.inc.Inc;
import sa.osama_alharbi.sas.model.SectionModel;

import java.net.URL;
import java.util.ResourceBundle;

public class Section_item implements Initializable {
    @FXML public Label lblName,lblClasses_name;
    private SectionModel model;

    @FXML public ImageView imgDelete;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        imgDelete.setImage(new Image(Index.class.getResourceAsStream("delete.png")));
    }

    @FXML public void onClickDelete(){
        System.out.println("onClickDelete");
        Inc.action.section.delete.deleteSection(model);
    }

    public SectionModel getModel() {
        return model;
    }

    public void setModel(SectionModel model) {
        this.model = model;
        if(lblName.textProperty().isBound()){
            lblName.textProperty().unbind();
        }
        if(lblClasses_name.textProperty().isBound()){
            lblClasses_name.textProperty().unbind();
        }
        if(model != null){
            lblName.textProperty().bind(this.model.section_nameProperty());
            lblClasses_name.textProperty().bind(this.model.getClassesModel().class_nameProperty());
        }
    }
}
