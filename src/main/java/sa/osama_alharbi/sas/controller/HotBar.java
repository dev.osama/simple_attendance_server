package sa.osama_alharbi.sas.controller;

import javafx.event.ActionEvent;
import sa.osama_alharbi.sas.inc.Inc;

public class HotBar {
    public void onClickStudents(ActionEvent actionEvent) {
        Inc.action.hotBar.showStudents();
    }

    public void onClickClasses(ActionEvent actionEvent) {
        Inc.action.hotBar.showClasses();
    }

    public void onClickStudentsClasses(ActionEvent actionEvent) {
        Inc.action.hotBar.showStudentsClasses();
    }

    public void onClickServer(ActionEvent actionEvent) {
        Inc.action.hotBar.showServer();
    }

    public void onClickSetting(ActionEvent actionEvent) {
        Inc.action.hotBar.showSetting();
    }

    public void onClickLogOut(ActionEvent actionEvent) {
        Inc.action.logInOut.logout();
    }
}
