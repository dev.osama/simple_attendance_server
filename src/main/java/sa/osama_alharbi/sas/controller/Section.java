package sa.osama_alharbi.sas.controller;

import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import sa.osama_alharbi.sas.inc.Inc;
import sa.osama_alharbi.sas.model.ClassesModel;
import sa.osama_alharbi.sas.model.SectionModel;

import java.net.URL;
import java.util.ResourceBundle;

public class Section implements Initializable {

    @FXML public Tab tab_AddEdit;
    @FXML public Tab tab_Students;
    @FXML public Tab tab_Attendance;
    @FXML public TabPane tab;

    @FXML public ListView<SectionModel> lst;
    public ObservableList<SectionModel> obsLst;
    @FXML public Button btnAdd;

    public BooleanProperty isAdd;
    public Data data = null;

    public Section(){
        obsLst = FXCollections.observableArrayList();
        data = new Data(obsLst);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        isAdd = new SimpleBooleanProperty(false);
        tab_AddEdit.setContent(Inc.gui.gui_Section_AddEdit);
        tab_Students.setContent(Inc.gui.gui_Section_Students);
        tab_Attendance.setContent(Inc.gui.gui_Section_Attendance);

        lst.setItems(obsLst);

        lst.setCellFactory(param -> {
            return new ListCell<>(){
                @Override
                protected void updateItem(SectionModel item, boolean empty) {
                    super.updateItem(item, empty);
                    if(item != null && !empty){
                        FXMLLoader looder = Inc.gui.getLoader("Section_item");
                        HBox pan = (HBox) Inc.gui.getPain(looder);
                        looder.<Section_item>getController().setModel(item);

                        setGraphic(pan);
                    }else{
                        setText(null);
                        setGraphic(null);
                    }
                }
            };
        });

        lst.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if(isAdd.get()){
                isAdd.setValue(false);
            }
            Inc.gui_controller.section_AddEdit.setModel(newValue);
        });
//
//        obsLst.addAll("Osama","AAAAAA","bbbbbb");



        Platform.runLater(() -> {
            Inc.gui_controller.section_AddEdit.parintGuiRady();
            Inc.gui_controller.section_Students.init_after_classes_GUI_created();
        });
    }

    @FXML public void onClickAdd(ActionEvent event) {
        lst.getSelectionModel().clearSelection();
        isAdd.setValue(true);
        tab.getSelectionModel().select(tab_AddEdit);
    }

    public class Data{
        private ObservableList<SectionModel> obs = null;
        public Data(ObservableList<SectionModel> obs){
            this.obs = obs;
        }
        public void addSectionModel(SectionModel model){
            this.obs.add(model);
        }
        public void deleteSectionModel(SectionModel model){
            this.obs.remove(model);
        }
    }
}
