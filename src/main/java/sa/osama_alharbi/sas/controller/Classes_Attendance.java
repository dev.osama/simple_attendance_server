package sa.osama_alharbi.sas.controller;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.StringConverter;
import sa.osama_alharbi.sas.inc.Inc;
import sa.osama_alharbi.sas.model.AttendanceModel;
import sa.osama_alharbi.sas.model.ClassesModel;
import sa.osama_alharbi.sas.model.SectionModel;

import java.net.URL;
import java.util.ResourceBundle;

public class Classes_Attendance implements Initializable {

    @FXML public ComboBox<SectionBoxItemModel> boxSelectSection;
    public ObservableList<SectionBoxItemModel> obsBoxSelectSection;

    @FXML public ComboBox<DateBoxItemModel> boxSelectDate;
    public ObservableList<DateBoxItemModel> obsBoxSelectDate;

    @FXML public ComboBox<TimeBoxItemModel> boxSelectTime;
    public ObservableList<TimeBoxItemModel> obsBoxSelectTime;

    @FXML public ListView<AttendanceModel> list;
    public ObservableList<AttendanceModel> obsList;

    @FXML public Label lblStudentsClassName,lblDate,lblTime,lblIsAbsent;

    public Data data = null;
    public Filter filter = null;

    public Classes_Attendance(){
        obsList = FXCollections.observableArrayList();
        obsBoxSelectSection = FXCollections.observableArrayList();
        obsBoxSelectDate = FXCollections.observableArrayList();
        obsBoxSelectTime = FXCollections.observableArrayList();

        data = new Data(obsBoxSelectSection,obsList,obsBoxSelectDate,obsBoxSelectTime);
        filter = new Filter();

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        list.setItems(obsList);
        list.setCellFactory(param -> {
            return new ListCell<>(){
                @Override
                protected void updateItem(AttendanceModel item, boolean empty) {
                    super.updateItem(item, empty);
                    if(item != null && !empty){
                        if(textProperty().isBound()){
                            textProperty().unbind();
                        }
                        textProperty().bind(item.getStudentsModel().students_nameProperty());
                    }else{
                        if(textProperty().isBound()){
                            textProperty().unbind();
                        }
                        setText(null);
                        setGraphic(null);
                    }
                }
            };
        });
//        obsList.addAll("Osama","AAAAAA","bbbbbb");


        boxSelectSection.setItems(obsBoxSelectSection);
        boxSelectSection.setCellFactory(param -> {
            return new ListCell<>(){
                @Override
                protected void updateItem(SectionBoxItemModel item, boolean empty) {
                    super.updateItem(item, empty);
                    if(item != null && !empty){
                        if(textProperty().isBound()){
                            textProperty().unbind();
                        }
                        textProperty().bind(item.title);
                    }else{
                        if(textProperty().isBound()){
                            textProperty().unbind();
                        }
                        setText(null);
                        setGraphic(null);
                    }
                }
            };
        });
        boxSelectSection.setConverter(new StringConverter<SectionBoxItemModel>() {
            @Override
            public String toString(SectionBoxItemModel itm) {
                if(itm != null){
                    return itm.getTitle();
                }
                return null;
            }

            @Override
            public SectionBoxItemModel fromString(String string) {
                return null;
            }
        });
        boxSelectSection.getSelectionModel().select(data.allBoxSectionModel);
//        obsBoxSelectSection.addAll("Osama","AAAAAA","bbbbbb");


        boxSelectDate.setItems(obsBoxSelectDate);
        boxSelectDate.setCellFactory(param -> {
            return new ListCell<>(){
                @Override
                protected void updateItem(DateBoxItemModel item, boolean empty) {
                    super.updateItem(item, empty);
                    if(item != null && !empty){
                        if(textProperty().isBound()){
                            textProperty().unbind();
                        }
                        textProperty().bind(item.title);
                    }else{
                        if(textProperty().isBound()){
                            textProperty().unbind();
                        }
                        setText(null);
                        setGraphic(null);
                    }
                }
            };
        });
        boxSelectDate.setConverter(new StringConverter<DateBoxItemModel>() {
            @Override
            public String toString(DateBoxItemModel itm) {
                if(itm != null){
                    return itm.title.get();
                }
                return null;
            }

            @Override
            public DateBoxItemModel fromString(String string) {
                return null;
            }
        });
        boxSelectDate.getSelectionModel().select(data.allBoxDate);
//        obsBoxSelectDate.addAll("Osama","AAAAAA","bbbbbb");


        boxSelectTime.setItems(obsBoxSelectTime);
        boxSelectTime.setCellFactory(param -> {
            return new ListCell<>(){
                @Override
                protected void updateItem(TimeBoxItemModel item, boolean empty) {
                    super.updateItem(item, empty);
                    if(item != null && !empty){
                        if(textProperty().isBound()){
                            textProperty().unbind();
                        }
                        textProperty().bind(item.title);
                    }else{
                        if(textProperty().isBound()){
                            textProperty().unbind();
                        }
                        setText(null);
                        setGraphic(null);
                    }
                }
            };
        });
        boxSelectTime.setConverter(new StringConverter<TimeBoxItemModel>() {
            @Override
            public String toString(TimeBoxItemModel itm) {
                if(itm != null){
                    return itm.title.get();
                }
                return null;
            }

            @Override
            public TimeBoxItemModel fromString(String string) {
                return null;
            }
        });
        boxSelectTime.getSelectionModel().select(data.allBoxTime);
//        obsBoxSelectTime.addAll("Osama","AAAAAA","bbbbbb");

    }

    public void init_after_classes_GUI_created(){
        Inc.gui_controller.classes.lst.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            filter.boxSelectSection_autofilter(newValue);
            if(newValue != null){
                boxSelectSection.getSelectionModel().select(data.allBoxSectionModel);
            }
        });
        filter.boxSelectSection_autofilter(null);
    }

    public class Filter{
        public void boxSelectSection_autofilter(ClassesModel classesModel){
            FilteredList<SectionBoxItemModel> filteredData = new FilteredList<SectionBoxItemModel>(obsBoxSelectSection, s -> true);
            boxSelectSection.setItems(filteredData);
            filteredData.setPredicate(data -> {
                if(classesModel != null){
                    if(data.isAll){
                        return true;
                    }
                    return data.getSectionModel().getClass_id() == classesModel.getClass_id();
                }else{
                    return false;
                }
            });
        }
    }

    public class Data{

        public SectionBoxItemModel allBoxSectionModel = new SectionBoxItemModel(null,true);
        private ObservableList<SectionBoxItemModel> obsBoxSelectSection = null;

        public DateBoxItemModel allBoxDate = new DateBoxItemModel(null,true);
        private ObservableList<DateBoxItemModel> obsBoxDate = null;

        public TimeBoxItemModel allBoxTime = new TimeBoxItemModel(null,true);
        private ObservableList<TimeBoxItemModel> obsBoxTime = null;

        private ObservableList<AttendanceModel> obsList = null;

        public Data(ObservableList<SectionBoxItemModel> obsBoxSelectSection, ObservableList<AttendanceModel> obsList,ObservableList<DateBoxItemModel> obsBoxDate,ObservableList<TimeBoxItemModel> obsBoxTime){
            this.obsBoxSelectSection = obsBoxSelectSection;
            this.obsBoxSelectSection.add(allBoxSectionModel);

            this.obsList = obsList;

            this.obsBoxDate = obsBoxDate;
            this.obsBoxDate.add(allBoxDate);

            this.obsBoxTime = obsBoxTime;
            this.obsBoxTime.add(allBoxTime);
        }
        public void addAttendanceModel(AttendanceModel model){
            this.obsList.add(model);
        }
        public void deleteAttendanceModel(AttendanceModel model){
            this.obsList.remove(model);

        }
        public void addSectionModel(SectionModel model){
            this.obsBoxSelectSection.add(new SectionBoxItemModel(model,false));
        }
        public void deleteSectionModel(SectionModel model){
            SectionBoxItemModel delete = null;
            for (SectionBoxItemModel itm:obsBoxSelectSection) {
                if(!itm.isAll){
                    if(itm.sectionModel.getSection_id() == model.getSection_id()){
                        delete = itm;
                        break;
                    }
                }
            }
            if(delete != null){
                this.obsBoxSelectSection.remove(delete);
            }
        }
    }
    protected class SectionBoxItemModel{
        private SectionModel sectionModel;
        private boolean isAll;
        private StringProperty title;
        public SectionBoxItemModel(SectionModel sectionModel, boolean isAll) {
            this.sectionModel = sectionModel;
            this.isAll = isAll;
            this.title = new SimpleStringProperty();
            if(isAll){
                this.title.setValue("الكل");
            }else{
                this.title.bind(sectionModel.section_nameProperty());
            }
        }
        public SectionModel getSectionModel() {
            return sectionModel;
        }
        public void setSectionModel(SectionModel sectionModel) {
            this.sectionModel = sectionModel;
        }
        public boolean isAll() {
            return isAll;
        }
        public void setAll(boolean all) {
            isAll = all;
        }
        public String getTitle() {
            return title.get();
        }
        public StringProperty titleProperty() {
            return title;
        }
        public void setTitle(String title) {
            this.title.set(title);
        }
    }

    protected class DateBoxItemModel {
        private AttendanceModel attendanceModel;
        private boolean isAll;
        private StringProperty title;

        public DateBoxItemModel(AttendanceModel attendanceModel, boolean isAll) {
            this.attendanceModel = attendanceModel;
            this.isAll = isAll;
            this.title = new SimpleStringProperty();
            if (isAll) {
                this.title.setValue("الكل");
            } else {
                //TODO convert dateTime to String date
//                this.title.bind(Bindings.convert(attendanceModel.attendance_dateTimeProperty().));
            }
        }
    }

    protected class TimeBoxItemModel {
        private AttendanceModel attendanceModel;
        private boolean isAll;
        private StringProperty title;

        public TimeBoxItemModel(AttendanceModel attendanceModel, boolean isAll) {
            this.attendanceModel = attendanceModel;
            this.isAll = isAll;
            this.title = new SimpleStringProperty();
            if (isAll) {
                this.title.setValue("الكل");
            } else {
                //TODO convert dateTime to String time
//                this.title.bind(Bindings.convert(attendanceModel.attendance_dateTimeProperty().));
            }
        }
    }
}
