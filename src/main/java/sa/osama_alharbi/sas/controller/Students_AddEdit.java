package sa.osama_alharbi.sas.controller;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import sa.osama_alharbi.sas.inc.Inc;
import sa.osama_alharbi.sas.model.StudentsModel;

import java.net.URL;
import java.util.ResourceBundle;

public class Students_AddEdit implements Initializable {

    @FXML public TextField txtStudentsName;
    @FXML public Button btnAddEdit,btnReset;
    @FXML public HBox boxAddEditReset;

    private BooleanProperty isAdd;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        isAdd = new SimpleBooleanProperty(false);
        btnReset.setText("اعادة تعين");
        btnAddEdit.setText("تعديل");
        isAdd.addListener((observable, oldValue, isAdd) -> {
            boxAddEditReset.getChildren().clear();
            if(isAdd){
                btnAddEdit.setText("إضافة");
                boxAddEditReset.getChildren().add(btnAddEdit);
            }else{
                btnAddEdit.setText("تعديل");
                boxAddEditReset.getChildren().add(btnAddEdit);
                boxAddEditReset.getChildren().add(btnReset);
            }
        });
    }

    private StudentsModel model;
    public void setModel(StudentsModel model){
        if(model != null){
            txtStudentsName.setText(model.getStudents_name());
        }else{
            txtStudentsName.setText("");
        }
        this.model = model;
    }

    public void parintGuiRady(){
        isAdd.setValue(Inc.gui_controller.students.isAdd.getValue());
        isAdd.bind(Inc.gui_controller.students.isAdd);
        isAdd.addListener((observable, oldValue, isAdd) -> {
            if(isAdd){
                txtStudentsName.setText("");
            }
        });
        Inc.gui_controller.students.isAdd.setValue(true);
    }

    @FXML public void onClickAddEdit(){
        if(isAdd.get()){
            //add
            if(txtStudentsName.getText().isEmpty()){
                //show alert is empty
            }else{
                //send to action
                if(Inc.action.students.add.addNewStudents(txtStudentsName.getText())){
                    Inc.gui_controller.students.list.getSelectionModel().selectLast();
                }
            }
        }else{
            //edit
            if(txtStudentsName.getText().equals(model)){
                //show alert nothing to do
            }else{
                //send to action
                if(Inc.action.students.edit.editStudents(model,txtStudentsName.getText())){
                    if(model != null){
                        txtStudentsName.setText(model.getStudents_name());
                    }
                }
            }
        }
    }

    @FXML public void onClickReset(){
        if(model != null){
            txtStudentsName.setText(model.getStudents_name());
        }else{
            txtStudentsName.setText("");
        }
    }

}
