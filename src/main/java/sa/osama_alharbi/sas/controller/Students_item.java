package sa.osama_alharbi.sas.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import sa.osama_alharbi.sas.img.Index;
import sa.osama_alharbi.sas.inc.Inc;
import sa.osama_alharbi.sas.model.StudentsModel;

import java.net.URL;
import java.util.ResourceBundle;

public class Students_item implements Initializable {

    @FXML public Label lblTitle;
    @FXML public ImageView imgDelete;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        imgDelete.setImage(new Image(Index.class.getResourceAsStream("delete.png")));
    }

    @FXML public void onClickImgDeleted(){
        Inc.action.students.delete.deleteStudents(model);
    }

    private StudentsModel model ;
    public void setModel(StudentsModel model) {
        this.model = model;
        if(lblTitle.textProperty().isBound()){
            lblTitle.textProperty().unbind();
        }
        lblTitle.textProperty().bind(model.students_nameProperty());
    }
}
