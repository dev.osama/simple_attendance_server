package sa.osama_alharbi.sas.controller;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import sa.osama_alharbi.sas.inc.Inc;

import java.net.URL;
import java.util.ResourceBundle;

public class Server implements Initializable {

    @FXML
    public TextField txtPortNumber, txtPass;

    @FXML
    public Button btnStartClose, btnSave, btnReset;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        btnStartClose.setText("تشغيل");
        Inc.model.server.get.getRunningTypeProperty().addListener((observable, oldValue, newValue) -> {
            switch (newValue.intValue()) {
                case sa.osama_alharbi.sas.ws.Server.Type.SERVER_TRY_START:
                    btnStartClose.setText("جاري التشغيل");
                    break;
                case sa.osama_alharbi.sas.ws.Server.Type.SERVER_STARTED:
                    btnStartClose.setText("إغلاق");
                    break;
                case sa.osama_alharbi.sas.ws.Server.Type.SERVER_TRY_CLOSE:
                    btnStartClose.setText("جاري الاغلاق");
                    break;
                case sa.osama_alharbi.sas.ws.Server.Type.SERVER_CLOSED:
                    btnStartClose.setText("تشغيل");
                    break;
            }
        });
        txtPortNumber.disableProperty().bind(Inc.model.server.get.getIsRunningProperty());
        txtPass.disableProperty().bind(Inc.model.server.get.getIsRunningProperty());
    }

    public void reset() {
        txtPortNumber.setText(Inc.model.setting.get.get().getSetting_serverPort() + "");
        txtPass.setText(Inc.model.setting.get.get().getSetting_serverPassword());
    }

    @FXML
    public void onClickSave() {
        if (!txtPass.getText().isEmpty() && !txtPortNumber.getText().isEmpty()) {
            Inc.action.setting.edit.edit_serverPasswordPort(txtPass.getText(), Integer.parseInt(txtPortNumber.getText()));
        }
    }

    @FXML
    public void onClickReset() {
        reset();
    }

    @FXML
    public void onClickStartClose(ActionEvent event) {
        Platform.runLater(() -> {
            new Thread(() -> {
                switch (Inc.model.server.get.getRunningType()) {
                    case sa.osama_alharbi.sas.ws.Server.Type.SERVER_STARTED:
                        Inc.server.stop();
                        break;
                    case sa.osama_alharbi.sas.ws.Server.Type.SERVER_CLOSED:
                        try {
                            Inc.server.start(Inc.model.setting.get.get().getSetting_serverPort());
                        } catch (Exception e) {

                        }
                        break;
                    case sa.osama_alharbi.sas.ws.Server.Type.SERVER_TRY_CLOSE:
                    case sa.osama_alharbi.sas.ws.Server.Type.SERVER_TRY_START:
                        break;
                }
            }).start();
        });

    }
}
