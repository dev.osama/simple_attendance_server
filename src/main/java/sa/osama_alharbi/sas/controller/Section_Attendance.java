package sa.osama_alharbi.sas.controller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.StringConverter;
import sa.osama_alharbi.sas.model.AttendanceModel;

import java.net.URL;
import java.util.ResourceBundle;

public class Section_Attendance implements Initializable {

    @FXML public ComboBox<DateBoxItemModel> boxSelectDate;
    public ObservableList<DateBoxItemModel> obsBoxSelectDate;

    @FXML public ComboBox<TimeBoxItemModel> boxSelectTime;
    public ObservableList<TimeBoxItemModel> obsBoxSelectTime;

    @FXML public ListView<AttendanceModel> list;
    public ObservableList<AttendanceModel> obsList;

    @FXML public Label lblStudentsClassName,lblDate,lblTime,lblIsAbsent;
    public Data data = null;

    public Section_Attendance(){
        obsList = FXCollections.observableArrayList();
        obsBoxSelectDate = FXCollections.observableArrayList();
        obsBoxSelectTime = FXCollections.observableArrayList();
        data = new Data(obsList,obsBoxSelectDate,obsBoxSelectTime);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        list.setItems(obsList);
        list.setCellFactory(param -> {
            return new ListCell<>(){
                @Override
                protected void updateItem(AttendanceModel item, boolean empty) {
                    super.updateItem(item, empty);
                    if(item != null && !empty){
                        if(textProperty().isBound()){
                            textProperty().unbind();
                        }
                        textProperty().bind(item.getStudentsModel().students_nameProperty());
                    }else{
                        if(textProperty().isBound()){
                            textProperty().unbind();
                        }
                        setText(null);
                        setGraphic(null);
                    }
                }
            };
        });
//        obsList.addAll("Osama","AAAAAA","bbbbbb");


        boxSelectDate.setItems(obsBoxSelectDate);
        boxSelectDate.setCellFactory(param -> {
            return new ListCell<>(){
                @Override
                protected void updateItem(DateBoxItemModel item, boolean empty) {
                    super.updateItem(item, empty);
                    if(item != null && !empty){
                        if(textProperty().isBound()){
                            textProperty().unbind();
                        }
                        textProperty().bind(item.title);
                    }else{
                        if(textProperty().isBound()){
                            textProperty().unbind();
                        }
                        setText(null);
                        setGraphic(null);
                    }
                }
            };
        });
        boxSelectDate.setConverter(new StringConverter<DateBoxItemModel>() {
            @Override
            public String toString(DateBoxItemModel itm) {
                if(itm != null){
                    return itm.title.get();
                }
                return null;
            }

            @Override
            public DateBoxItemModel fromString(String string) {
                return null;
            }
        });
        boxSelectDate.getSelectionModel().select(data.allBoxDate);
//        obsBoxSelectDate.addAll("Osama","AAAAAA","bbbbbb");


        boxSelectTime.setItems(obsBoxSelectTime);
        boxSelectTime.setCellFactory(param -> {
            return new ListCell<>(){
                @Override
                protected void updateItem(TimeBoxItemModel item, boolean empty) {
                    super.updateItem(item, empty);
                    if(item != null && !empty){
                        if(textProperty().isBound()){
                            textProperty().unbind();
                        }
                        textProperty().bind(item.title);
                    }else{
                        if(textProperty().isBound()){
                            textProperty().unbind();
                        }
                        setText(null);
                        setGraphic(null);
                    }
                }
            };
        });
        boxSelectTime.setConverter(new StringConverter<TimeBoxItemModel>() {
            @Override
            public String toString(TimeBoxItemModel itm) {
                if(itm != null){
                    return itm.title.get();
                }
                return null;
            }

            @Override
            public TimeBoxItemModel fromString(String string) {
                return null;
            }
        });
        boxSelectTime.getSelectionModel().select(data.allBoxTime);
//        obsBoxSelectTime.addAll("Osama","AAAAAA","bbbbbb");

    }

    public class Data{
        public DateBoxItemModel allBoxDate = new DateBoxItemModel(null,true);
        private ObservableList<DateBoxItemModel> obsBoxDate = null;

        public TimeBoxItemModel allBoxTime = new TimeBoxItemModel(null,true);
        private ObservableList<TimeBoxItemModel> obsBoxTime = null;

        private ObservableList<AttendanceModel> obsList = null;

        public Data(ObservableList<AttendanceModel> obsList, ObservableList<DateBoxItemModel> obsBoxDate, ObservableList<TimeBoxItemModel> obsBoxTime){
            this.obsList = obsList;

            this.obsBoxDate = obsBoxDate;
            this.obsBoxDate.add(allBoxDate);

            this.obsBoxTime = obsBoxTime;
            this.obsBoxTime.add(allBoxTime);
        }
        public void addAttendanceModel(AttendanceModel model){
            this.obsList.add(model);
        }
        public void deleteAttendanceModel(AttendanceModel model){
            this.obsList.remove(model);

        }
    }

    protected class DateBoxItemModel {
        private AttendanceModel attendanceModel;
        private boolean isAll;
        private StringProperty title;

        public DateBoxItemModel(AttendanceModel attendanceModel, boolean isAll) {
            this.attendanceModel = attendanceModel;
            this.isAll = isAll;
            this.title = new SimpleStringProperty();
            if (isAll) {
                this.title.setValue("الكل");
            } else {
                //TODO convert dateTime to String date
//                this.title.bind(Bindings.convert(attendanceModel.attendance_dateTimeProperty().));
            }
        }
    }

    protected class TimeBoxItemModel {
        private AttendanceModel attendanceModel;
        private boolean isAll;
        private StringProperty title;

        public TimeBoxItemModel(AttendanceModel attendanceModel, boolean isAll) {
            this.attendanceModel = attendanceModel;
            this.isAll = isAll;
            this.title = new SimpleStringProperty();
            if (isAll) {
                this.title.setValue("الكل");
            } else {
                //TODO convert dateTime to String time
//                this.title.bind(Bindings.convert(attendanceModel.attendance_dateTimeProperty().));
            }
        }
    }
}
