package sa.osama_alharbi.sas.controller;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.util.StringConverter;
import sa.osama_alharbi.sas.inc.Inc;
import sa.osama_alharbi.sas.model.ClassesModel;
import sa.osama_alharbi.sas.model.SectionModel;

import java.net.URL;
import java.util.ResourceBundle;

public class Section_AddEdit implements Initializable {

    @FXML
    public TextField txtStudentsClassName;
    @FXML public PasswordField txtPassword;
    @FXML public ComboBox<ClassesModel> boxClasses;
    public ObservableList<ClassesModel> obsBoxClasses;
    @FXML public Button btnAddEdit,btnReset;
    @FXML public HBox boxAddEditReset;
    public Data data = null;
    private BooleanProperty isAdd;

    public Section_AddEdit(){
        obsBoxClasses = FXCollections.observableArrayList();
        data = new Data(obsBoxClasses);
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        isAdd = new SimpleBooleanProperty(false);
        btnReset.setText("اعادة تعين");
        btnAddEdit.setText("تعديل");
        isAdd.addListener((observable, oldValue, isAdd) -> {
            boxAddEditReset.getChildren().clear();
            if(isAdd){
                btnAddEdit.setText("إضافة");
                boxAddEditReset.getChildren().add(btnAddEdit);
            }else{
                btnAddEdit.setText("تعديل");
                boxAddEditReset.getChildren().add(btnAddEdit);
                boxAddEditReset.getChildren().add(btnReset);
            }
        });

        boxClasses.setItems(obsBoxClasses);
        boxClasses.setCellFactory(param -> {
            return new ListCell<>(){
                @Override
                protected void updateItem(ClassesModel item, boolean empty) {
                    super.updateItem(item, empty);
                    if(item != null && !empty){
                        if(textProperty().isBound()){
                            textProperty().unbind();
                        }
                        textProperty().bind(item.class_nameProperty());
                    }else{
                        if(textProperty().isBound()){
                            textProperty().unbind();
                        }
                        setText(null);
                        setGraphic(null);
                    }
                }
            };
        });
        boxClasses.setConverter(new StringConverter<ClassesModel>() {
            @Override
            public String toString(ClassesModel itm) {
                if(itm == null){
                    return null;
                }
                return itm.getClass_name();
            }

            @Override
            public ClassesModel fromString(String string) {
                return null;
            }
        });

        boxClasses.getSelectionModel().selectFirst();

    }

    private SectionModel model;
    public void setModel(SectionModel model){
        if(model != null){
            txtStudentsClassName.setText(model.getSection_name());
            boxClasses.getSelectionModel().select(model.getClassesModel());
            boxClasses.setDisable(true);
        }else{
            txtStudentsClassName.setText("");
            boxClasses.getSelectionModel().clearSelection();
            boxClasses.setDisable(false);
        }
        txtPassword.setText("");
        this.model = model;
    }

    public void parintGuiRady(){
        isAdd.setValue(Inc.gui_controller.section.isAdd.getValue());
        isAdd.bind(Inc.gui_controller.section.isAdd);
        isAdd.addListener((observable, oldValue, isAdd) -> {
            if(isAdd){
                txtStudentsClassName.setText("");
                txtPassword.setText("");
            }
        });
        Inc.gui_controller.section.isAdd.setValue(true);
    }

    @FXML public void onClickAddEdit(ActionEvent event) {
        if(isAdd.get()){
            //add
            ClassesModel classesModel = boxClasses.getSelectionModel().getSelectedItem();
            if(txtStudentsClassName.getText().isEmpty() || txtPassword.getText().isEmpty() || classesModel == null){
                //show alert is empty
            }else{
                //send to action
                if(Inc.action.section.add.addNewSection(txtStudentsClassName.getText(),txtPassword.getText(),classesModel.getClass_id())){
                    Inc.gui_controller.section.lst.getSelectionModel().selectLast();
                }
            }
        }else{
            //edit
            boolean flag_isNameChanged = false;
            boolean flag_isNameNOTEmpty = false;
            boolean flag_isPasswordAddNew = false;
            if(!txtStudentsClassName.getText().equals(model.getSection_name())){
                flag_isNameChanged = true;
            }
            if(!txtStudentsClassName.getText().isEmpty()){
                flag_isNameNOTEmpty = true;
            }
            if(!txtPassword.getText().isEmpty()){
                flag_isPasswordAddNew = true;
            }
            if(flag_isPasswordAddNew){
                if(flag_isNameNOTEmpty){
                    if(flag_isNameChanged){
                        //update name and password
                        if(Inc.action.section.edit.editSection(model,txtStudentsClassName.getText(),txtPassword.getText())){
                            if(model != null){
                                txtStudentsClassName.setText(model.getSection_name());
                                txtPassword.setText("");
                            }
                        }
                    }else{
                        //update password
                        if(Inc.action.section.edit.editSection(model,model.getSection_name(),txtPassword.getText())){
                            if(model != null){
                                txtStudentsClassName.setText(model.getSection_name());
                                txtPassword.setText("");
                            }
                        }
                    }
                }else{
                    //alert the name is Empty
                }
            }else{
                if(flag_isNameNOTEmpty){
                    if(flag_isNameChanged){
                        //update name
                        if(Inc.action.section.edit.editSection(model,txtStudentsClassName.getText(),model.getSection_password())){
                            if(model != null){
                                txtStudentsClassName.setText(model.getSection_name());
                                txtPassword.setText("");
                            }
                        }
                    }else{
                        //alert nothing to change
                    }
                }else{
                    //alert nothing to change
                }
            }
        }
    }
    @FXML public void onClickReset(ActionEvent event){
        if(model != null){
            txtStudentsClassName.setText(model.getSection_name());
        }else{
            txtStudentsClassName.setText("");
        }
        txtPassword.setText("");
    }
    public class Data{
        private ObservableList<ClassesModel> obsBoxSelectClass = null;


        public Data(ObservableList<ClassesModel> obsBoxSelectClass){
            this.obsBoxSelectClass = obsBoxSelectClass;
        }
        public void addClassesModel(ClassesModel model){
            this.obsBoxSelectClass.add(model);
        }
        public void deleteClassesModel(ClassesModel model){
            this.obsBoxSelectClass.remove(model);
        }
    }


}
