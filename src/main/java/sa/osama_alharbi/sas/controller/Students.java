package sa.osama_alharbi.sas.controller;

import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.VBox;
import sa.osama_alharbi.sas.inc.Inc;
import sa.osama_alharbi.sas.model.StudentsModel;

import java.net.URL;
import java.util.ResourceBundle;

public class Students implements Initializable {
    @FXML public ListView<StudentsModel> list;
    public ObservableList<StudentsModel> obsLst;
    public Data data = null;

    public BooleanProperty isAdd;
    @FXML public Tab tab_AddEdit;
    @FXML public Tab tab_Classes;
    @FXML public Tab tab_StudentsClasses;
    @FXML public Tab tab_Attendance;
    @FXML public TabPane tab;

    public Students(){
        obsLst = FXCollections.observableArrayList();
        data = new Data(obsLst);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        isAdd = new SimpleBooleanProperty(false);
        tab_AddEdit.setContent(Inc.gui.gui_Students_AddEdit);
        tab_Classes.setContent(Inc.gui.gui_Students_Classes);
        tab_StudentsClasses.setContent(Inc.gui.gui_Students_Section);
        tab_Attendance.setContent(Inc.gui.gui_Students_Attendance);

        list.setItems(obsLst);

        list.setCellFactory(param -> {
            return new ListCell<>(){
                @Override
                protected void updateItem(StudentsModel item, boolean empty) {
                    super.updateItem(item, empty);
                    if(item != null && !empty){
                        FXMLLoader looder = Inc.gui.getLoader("Students_item");
                        VBox pan = (VBox) Inc.gui.getPain(looder);
                        looder.<Students_item>getController().setModel(item);

                        setGraphic(pan);
                    }else{
                        setText(null);
                        setGraphic(null);
                    }
                }
            };
        });

        list.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if(isAdd.get()){
                isAdd.setValue(false);
            }
            Inc.gui_controller.students_AddEdit.setModel(newValue);
            Inc.gui_controller.students_Classes.setStudentsModel(newValue);
            Inc.gui_controller.students_Section.setStudentsModel(newValue);
        });




        Platform.runLater(() -> {
            Inc.gui_controller.students_AddEdit.parintGuiRady();
            Inc.gui_controller.students_Attendance.init_after_classes_GUI_created();
        });
    }

    @FXML public void onClickAdd(){
        list.getSelectionModel().clearSelection();
        isAdd.setValue(true);
        tab.getSelectionModel().select(tab_AddEdit);
    }

    public class Data{
        private ObservableList<StudentsModel> obs = null;
        public Data(ObservableList<StudentsModel> obs){
            this.obs = obs;
        }
        public void addStudentsModel(StudentsModel model){
            this.obs.add(model);
        }
        public void deleteStudentsModel(StudentsModel model){
            this.obs.remove(model);
        }
    }
}
