package sa.osama_alharbi.sas.controller;

import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import sa.osama_alharbi.sas.inc.Inc;
import sa.osama_alharbi.sas.model.ClassesModel;

import java.net.URL;
import java.util.ResourceBundle;

public class Classes implements Initializable {
    @FXML public ListView<ClassesModel> lst;
    public ObservableList<ClassesModel> obsLst;
    public Data data = null;

    public BooleanProperty isAdd;
    @FXML public TextField txtSearch;
    @FXML public Tab tab_AddEdit;
    @FXML public Tab tab_StudentsClasses;
    @FXML public Tab tab_Students;
    @FXML public Tab tab_Attendance;
    @FXML public TabPane tab;

    @FXML public Button btnAdd;

    public Classes(){
        obsLst = FXCollections.observableArrayList();
        data = new Data(obsLst);
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        isAdd = new SimpleBooleanProperty(false);

        tab_AddEdit.setContent(Inc.gui.gui_Classes_AddEdit);
        tab_StudentsClasses.setContent(Inc.gui.gui_Classes_Section);
        tab_Students.setContent(Inc.gui.gui_Classes_Students);
        tab_Attendance.setContent(Inc.gui.gui_Classes_Attendance);

        lst.setItems(obsLst);

        lst.setCellFactory(param -> {
            return new ListCell<>(){
                @Override
                protected void updateItem(ClassesModel item, boolean empty) {
                    super.updateItem(item, empty);
                    if(item != null && !empty){
                        FXMLLoader looder = Inc.gui.getLoader("Classes_item");
                        HBox pan = (HBox) Inc.gui.getPain(looder);
                        looder.<Classes_item>getController().setTitle(item);

                        setGraphic(pan);
                    }else{
                        setText(null);
                        setGraphic(null);
                    }
                }
            };
        });

        lst.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if(isAdd.get()){
                isAdd.setValue(false);
            }
            Inc.gui_controller.classes_AddEdit.setModel(newValue);
        });

        Platform.runLater(() -> {
            Inc.gui_controller.classes_AddEdit.parintGuiRady();
            Inc.gui_controller.classes_Section.init_after_classes_GUI_created();
            Inc.gui_controller.classes_Students.init_after_classes_GUI_created();
            Inc.gui_controller.classes_Attendance.init_after_classes_GUI_created();
        });


    }

    @FXML public void onClickAdd(ActionEvent event) {
        lst.getSelectionModel().clearSelection();
        isAdd.setValue(true);
        tab.getSelectionModel().select(tab_AddEdit);
    }

    public class Data{
        private ObservableList<ClassesModel> obs = null;
        public Data(ObservableList<ClassesModel> obs){
            this.obs = obs;
        }
        public void addClassesModel(ClassesModel model){
            this.obs.add(model);
        }
        public void deleteClassesModel(ClassesModel model){
            this.obs.remove(model);
        }
    }
}








/*
الطلاب المواد
-عرض المعلومات

الطلاب الشعبة
-عرض المعلومات
------------------------------------------
المواد

المواد الشعب
-عرض المعلومات

المواد الطلاب
-عرض المعلومات
-حذف زر حذف الطالب من الشعبة
------------------------------------------
الشعب

الشعب الطلاب
-عرض المعلومات
-زر حذف الطالب من الشعبة
------------------------------------------
السيرفر
اضافة الرقم السري
------------------------------------------
الاعدادات
-اضافة واجهة جديدة
-اضافة اسم المستخدم
-اضافة الرقم السري
 */