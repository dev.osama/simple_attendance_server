package sa.osama_alharbi.sas.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import sa.osama_alharbi.sas.model.SectionModel;

public class Classes_Section_listItem {
    @FXML
    public Label lblTitle;
    @FXML
    public HBox root;

    private SectionModel sectionModel = null;

    public void setSectionModel(SectionModel sectionModel) {
        this.sectionModel = sectionModel;
        if (lblTitle.textProperty().isBound()) {
            lblTitle.textProperty().unbind();
        }
        lblTitle.textProperty().bind(this.sectionModel.section_nameProperty());
    }

    public SectionModel getSectionModel() {
        return this.sectionModel;
    }
}
