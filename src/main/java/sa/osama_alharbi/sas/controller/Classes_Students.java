package sa.osama_alharbi.sas.controller;

import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.util.StringConverter;
import sa.osama_alharbi.sas.inc.Inc;
import sa.osama_alharbi.sas.model.ClassesModel;
import sa.osama_alharbi.sas.model.SectionModel;
import sa.osama_alharbi.sas.model.StudentsModel;
import sa.osama_alharbi.sas.model.StudentsSectionModel;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class Classes_Students implements Initializable {
    @FXML public ComboBox<SectionBoxItemModel> boxSelectStudentsClass;
    public ObservableList<SectionBoxItemModel> obsBoxSelectStudentsClass;
    public Data data = null;
    public Filter filter = null;

    @FXML public ListView<StudentsSectionModel> list;
    public ObservableList<StudentsSectionModel> obsList;

    @FXML public Label lblStudentsClassName,lblNumPresint,lblNumAbsent,lblNumTotalPresintAbsent,lblNumLate;

    @FXML public Button btnDeleteStudentsClass;

    public Classes_Students(){
        obsList = FXCollections.observableArrayList();
        obsBoxSelectStudentsClass = FXCollections.observableArrayList();
        this.data = new Data(obsBoxSelectStudentsClass,obsList);
        filter = new Filter();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        list.setItems(this.data.obsList);
        list.setCellFactory(param -> {
            return new ListCell<>(){
                @Override
                protected void updateItem(StudentsSectionModel item, boolean empty) {
                    super.updateItem(item, empty);
                    if(item != null && !empty){
                        if(textProperty().isBound()){
                            textProperty().unbind();
                        }
                        textProperty().bind(item.getStudentsModel().students_nameProperty());
                    }else{
                        if(textProperty().isBound()){
                            textProperty().unbind();
                        }
                        setText(null);
                        setGraphic(null);
                    }
                }
            };
        });
        list.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            show();
        });
//        obsList.addAll("Osama","AAAAAA","bbbbbb");


        boxSelectStudentsClass.setItems(obsBoxSelectStudentsClass);
        boxSelectStudentsClass.setCellFactory(param -> {
            return new ListCell<>(){
                @Override
                protected void updateItem(SectionBoxItemModel item, boolean empty) {
                    super.updateItem(item, empty);
                    if(item != null && !empty){
                        if(textProperty().isBound()){
                            textProperty().unbind();
                        }
                        textProperty().bind(item.title);
                    }else{
                        if(textProperty().isBound()){
                            textProperty().unbind();
                        }
                        setText(null);
                        setGraphic(null);
                    }
                }
            };
        });
        boxSelectStudentsClass.setConverter(new StringConverter<SectionBoxItemModel>() {
            @Override
            public String toString(SectionBoxItemModel itm) {
                if(itm == null){
                    return null;
                }
                return itm.getTitle();
            }

            @Override
            public SectionBoxItemModel fromString(String string) {
                return null;
            }
        });
        boxSelectStudentsClass.getSelectionModel().select(data.allBoxSectionModel);
        boxSelectStudentsClass.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            filter.listSelectStudentsSections_autofilter(newValue);
            show();
        });

    }

    private void show(){
        SectionBoxItemModel item = boxSelectStudentsClass.getSelectionModel().getSelectedItem();
        StudentsSectionModel studentsSectionModel = list.getSelectionModel().getSelectedItem();

        if(lblStudentsClassName.textProperty().isBound()){lblStudentsClassName.textProperty().unbind(); }

        if(item != null){
            if(item.isAll){
                if(studentsSectionModel != null){
                    lblStudentsClassName.setText("كل الشعب");

                    ArrayList<StudentsSectionModel> studentsSectionModelArrayList = Inc.model.studentsSection.get.getAllByStudents_idAndClasses_id(studentsSectionModel.getStudents_id(),studentsSectionModel.getSectionModel().getClass_id());
                    ArrayList<Integer> studentsSection_idsArr = new ArrayList<>();
                    for (StudentsSectionModel i:studentsSectionModelArrayList) {
                        studentsSection_idsArr.add(i.getStudents_section_id());
                    }

                    int present = Inc.db.tables.attendance.select.selectCountIsPresentViaStudents_section_ids(studentsSection_idsArr);
                    int late = Inc.db.tables.attendance.select.selectCountIsLateViaStudents_section_ids(studentsSection_idsArr);
                    int absent = Inc.db.tables.attendance.select.selectCountIsAbsentViaStudents_section_ids(studentsSection_idsArr);
                    int total = present + late + absent;


                    lblNumPresint.setText(present+"");
                    lblNumLate.setText(late+"");
                    lblNumAbsent.setText(absent+"");
                    lblNumTotalPresintAbsent.setText(total+"");
                    return;
                }
            }else{
                if(studentsSectionModel != null){
                    lblStudentsClassName.textProperty().bind(item.getSectionModel().section_nameProperty());

                    ArrayList<StudentsSectionModel> studentsSectionModelArrayList = Inc.model.studentsSection.get.getAllByStudents_idAndSection_id(studentsSectionModel.getStudents_id(),studentsSectionModel.getSection_id());
                    ArrayList<Integer> studentsSection_idsArr = new ArrayList<>();
                    for (StudentsSectionModel i:studentsSectionModelArrayList) {
                        studentsSection_idsArr.add(i.getStudents_section_id());
                    }

                    int present = Inc.db.tables.attendance.select.selectCountIsPresentViaStudents_section_ids(studentsSection_idsArr);
                    int late = Inc.db.tables.attendance.select.selectCountIsLateViaStudents_section_ids(studentsSection_idsArr);
                    int absent = Inc.db.tables.attendance.select.selectCountIsAbsentViaStudents_section_ids(studentsSection_idsArr);
                    int total = present + late + absent;


                    lblNumPresint.setText(present+"");
                    lblNumLate.setText(late+"");
                    lblNumAbsent.setText(absent+"");
                    lblNumTotalPresintAbsent.setText(total+"");
                    return;
                }
            }

            lblStudentsClassName.setText("");
            lblNumPresint.setText("");
            lblNumLate.setText("");
            lblNumAbsent.setText("");
            lblNumTotalPresintAbsent.setText("");
            btnDeleteStudentsClass.setText("");
            btnDeleteStudentsClass.setDisable(true);

        }else{

            lblStudentsClassName.setText("");
            lblNumPresint.setText("");
            lblNumLate.setText("");
            lblNumAbsent.setText("");
            lblNumTotalPresintAbsent.setText("");
            btnDeleteStudentsClass.setText("");
            btnDeleteStudentsClass.setDisable(true);
        }
    }


    private StudentsModel model = null;
    public void init_after_classes_GUI_created(){
        Inc.gui_controller.classes.lst.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            filter.boxSelectStudentsClass_autofilter(newValue);
            if(newValue != null){
                boxSelectStudentsClass.getSelectionModel().select(data.allBoxSectionModel);
            }
        });
        filter.boxSelectStudentsClass_autofilter(null);
    }

    public class Filter{
        public void boxSelectStudentsClass_autofilter(ClassesModel classesModel){
            FilteredList<SectionBoxItemModel> filteredData = new FilteredList<SectionBoxItemModel>(obsBoxSelectStudentsClass, s -> true);
            boxSelectStudentsClass.setItems(filteredData);
            filteredData.setPredicate(data -> {
                if(classesModel != null){
                    if(data.isAll){
                        return true;
                    }
                    return data.getSectionModel().getClass_id() == classesModel.getClass_id();
                }else{
                    return false;
                }
            });
        }
        public void listSelectStudentsSections_autofilter(SectionBoxItemModel sectionBoxItemModel){
            FilteredList<StudentsSectionModel> filteredData = new FilteredList<StudentsSectionModel>(data.obsList, s -> true);
            list.setItems(filteredData);
            filteredData.setPredicate(data -> {
                if(sectionBoxItemModel != null){
                    if(sectionBoxItemModel.isAll){
                        for (StudentsSectionModel i:filteredData) {
                            if(i.getStudents_id() == data.getStudents_id()){
                                return false;
                            }
                        }
                        if(Inc.gui_controller.classes.lst.getSelectionModel().selectedItemProperty().get() == null){
                            return false;
                        }
                        return Inc.gui_controller.classes.lst.getSelectionModel().selectedItemProperty().get().getClass_id() == data.getSectionModel().getClass_id();
                    }
                    for (StudentsSectionModel i:filteredData) {
                        if(i.getStudents_id() == data.getStudents_id()){
                            return false;
                        }
                    }
                    return data.getSectionModel().getSection_id() == sectionBoxItemModel.getSectionModel().getSection_id();
                }else{
                    return false;
                }
            });
        }
    }

    @FXML public void onClickDelete(ActionEvent event) {

    }

    public class Data{

        public SectionBoxItemModel allBoxSectionModel = new SectionBoxItemModel(null,true);
        private ObservableList<SectionBoxItemModel> obsBoxSelectStudentsClass = null;
        private ObservableList<StudentsSectionModel> obsList = null;

        public Data(ObservableList<SectionBoxItemModel> obsBoxSelectStudentsClass,ObservableList<StudentsSectionModel> obsList){
            this.obsBoxSelectStudentsClass = obsBoxSelectStudentsClass;
            this.obsBoxSelectStudentsClass.add(allBoxSectionModel);
            this.obsList = obsList;
        }
        public void addStudentsSectionModel(StudentsSectionModel model){
            this.obsList.add(model);
        }
        public void deleteStudentsSectionModel(StudentsSectionModel model){
            if(this.obsList.contains(model)){
                Platform.runLater(() -> {
                    this.obsList.remove(model);
                });
            }

        }
        public void addSectionModel(SectionModel model){
            this.obsBoxSelectStudentsClass.add(new SectionBoxItemModel(model,false));
        }
        public void deleteSectionModel(SectionModel model){
            SectionBoxItemModel delete = null;
            for (SectionBoxItemModel itm:obsBoxSelectStudentsClass) {
                if(!itm.isAll){
                    if(itm.sectionModel.getSection_id() == model.getSection_id()){
                        delete = itm;
                        break;
                    }
                }
            }
            if(delete != null){
                if(this.obsBoxSelectStudentsClass.contains(delete)){
                    Platform.runLater(() -> {
                        this.obsBoxSelectStudentsClass.remove(model);
                    });
                }
            }
        }
    }
    protected class SectionBoxItemModel{
        private SectionModel sectionModel;
        private boolean isAll;
        private StringProperty title;
        public SectionBoxItemModel(SectionModel sectionModel, boolean isAll) {
            this.sectionModel = sectionModel;
            this.isAll = isAll;
            this.title = new SimpleStringProperty();
            if(isAll){
                this.title.setValue("الكل");
            }else{
                //TODO Do somthing
                this.title.bind(sectionModel.section_nameProperty());
            }
        }
        public SectionModel getSectionModel() {
            return sectionModel;
        }
        public void setSectionModel(SectionModel sectionModel) {
            this.sectionModel = sectionModel;
        }
        public boolean isAll() {
            return isAll;
        }
        public void setAll(boolean all) {
            isAll = all;
        }
        public String getTitle() {
            return title.get();
        }
        public StringProperty titleProperty() {
            return title;
        }
        public void setTitle(String title) {
            this.title.set(title);
        }
    }
}
