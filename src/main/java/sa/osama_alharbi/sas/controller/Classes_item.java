package sa.osama_alharbi.sas.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import sa.osama_alharbi.sas.img.Index;
import sa.osama_alharbi.sas.inc.Inc;
import sa.osama_alharbi.sas.model.ClassesModel;

import java.net.URL;
import java.util.ResourceBundle;

public class Classes_item implements Initializable {

    @FXML public Label txtTitle;
    @FXML public ImageView imgDelete;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        imgDelete.setImage(new Image(Index.class.getResourceAsStream("delete.png")));
    }

    @FXML public void onClickDelete(){
        System.out.println("onClickDelete");
        Inc.action.classes.delete.deleteClasses(model);
    }

    private ClassesModel model;
    public void setTitle(ClassesModel model){
        this.model = model;
        if(txtTitle.textProperty().isBound()){
            txtTitle.textProperty().unbind();
        }
        txtTitle.textProperty().bind(this.model.class_nameProperty());
    }
}
