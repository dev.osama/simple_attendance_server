package sa.osama_alharbi.sas.controller;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.layout.HBox;
import sa.osama_alharbi.sas.inc.Inc;
import sa.osama_alharbi.sas.model.ClassesModel;
import sa.osama_alharbi.sas.model.SectionModel;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class Classes_Section implements Initializable {
    @FXML public ListView<Classes_Section_listItem> list;
    public ObservableList<Classes_Section_listItem> obsList;
    public Data data;
    public Filter filter;

    @FXML public Label lblStudentsClassName,lblNumOfStudents;

    @FXML public Button btnDeleteStudentsClass;

    public Classes_Section(){
        obsList = FXCollections.observableArrayList();
        data = new Data(obsList);
        filter = new Filter();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        list.setItems(obsList);
        list.setCellFactory(param -> {
            return new ListCell<>(){
                @Override
                protected void updateItem(Classes_Section_listItem item, boolean empty) {
                    super.updateItem(item, empty);
                    if(item != null && !empty){
                        setGraphic(item.root);
                    }else{
                        setGraphic(null);
                    }
                }
            };
        });

        list.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if(lblStudentsClassName.textProperty().isBound()){
                lblStudentsClassName.textProperty().unbind();
            }
            if(newValue != null){
                lblStudentsClassName.textProperty().bind(newValue.getSectionModel().section_nameProperty());
                lblNumOfStudents.setText(""+Inc.model.studentsSection.get.getAllBySection_id(newValue.getSectionModel().getSection_id()).size());
            }else{
                lblNumOfStudents.setText("");
                lblStudentsClassName.setText("");
            }
        });
    }

    public void init_after_classes_GUI_created(){
        Inc.gui_controller.classes.lst.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            filter.list_autofilter(newValue);
        });
        filter.list_autofilter(null);
    }

    public class Filter{
        public void list_autofilter(ClassesModel classesModel){
            FilteredList<Classes_Section_listItem> filteredData = new FilteredList<Classes_Section_listItem>(obsList, s -> true);
            list.setItems(filteredData);
            filteredData.setPredicate(data -> {
                if(classesModel != null){
                    return data.getSectionModel().getClass_id() == classesModel.getClass_id();
                }else{
                    return false;
                }
            });
        }
    }

    public class Data{

        private ObservableList<Classes_Section_listItem> obsList = null;

        public Data(ObservableList<Classes_Section_listItem> obsList){
            this.obsList = obsList;
        }
        public void addSectionModel(SectionModel model){
            FXMLLoader looder = Inc.gui.getLoader("Classes_Section_listItem");
            HBox pan = (HBox) Inc.gui.getPain(looder);
            Classes_Section_listItem cont = looder.<Classes_Section_listItem>getController();
            cont.setSectionModel(model);

            this.obsList.add(cont);
        }
        public void deleteSectionModel(SectionModel model){
            Classes_Section_listItem itm = null;
            for (Classes_Section_listItem i: obsList) {
                if(i.getSectionModel().getSection_id() == model.getSection_id()){
                    itm = i;
                    break;
                }
            }

            if(itm != null){
                if(this.obsList.contains(itm)){
                    Platform.runLater(() -> {
                        this.obsList.remove(model);
                    });
                }
            }

        }
    }
}
