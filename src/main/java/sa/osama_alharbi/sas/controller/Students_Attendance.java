package sa.osama_alharbi.sas.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.StringConverter;
import sa.osama_alharbi.sas.inc.Inc;
import sa.osama_alharbi.sas.model.*;

import java.net.URL;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class Students_Attendance implements Initializable {
    @FXML public ComboBox<ClassesModel> boxSelectClass;
    public ObservableList<ClassesModel> obsBoxSelectClass;

    @FXML public ListView<AttendanceModel> list;
    public ObservableList<AttendanceModel> obsList;

    @FXML public ComboBox<SectionModel> boxSelectSection;
    public ObservableList<SectionModel> obsBoxSelectSection;

    public Data data;
    public Filter filterr;
    private DateTimeFormatter formatterDate = DateTimeFormatter.ofPattern("yyyy/MM/dd");
    private DateTimeFormatter formatterTime = DateTimeFormatter.ofPattern("HH:mm:ss");

    @FXML public Label lblClassName,lblStudentsClassName,lblDate,lblTime, lblIsAbsent;

    public Students_Attendance(){
        obsList = FXCollections.observableArrayList();
        obsBoxSelectClass = FXCollections.observableArrayList();
        obsBoxSelectSection = FXCollections.observableArrayList();

        data = new Data(obsBoxSelectClass,obsBoxSelectSection,obsList);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        filterr = new Filter();
        list.setItems(obsList);
        list.setCellFactory(param -> new ListCell<>(){
                @Override
                protected void updateItem(AttendanceModel item, boolean empty) {
                    super.updateItem(item, empty);
                    if(item != null && !empty){
                        setText(item.getAttendance_dateTime().toString());
                    }else{
                        setText(null);
                        setGraphic(null);
                    }
                }
            }
        );
        list.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if(lblClassName.textProperty().isBound()){
                lblClassName.textProperty().unbind();
            }
            if(lblStudentsClassName.textProperty().isBound()){
                lblStudentsClassName.textProperty().unbind();
            }
            if(newValue != null){
                ClassesModel classesModel = boxSelectClass.getSelectionModel().getSelectedItem();
                SectionModel sectionModel = boxSelectSection.getSelectionModel().getSelectedItem();
                if(classesModel != null && sectionModel != null){
                    lblClassName.textProperty().bind(classesModel.class_nameProperty());
                    lblStudentsClassName.textProperty().bind(sectionModel.section_nameProperty());
                    lblDate.setText(formatterDate.format(newValue.getAttendance_dateTime()));
                    lblTime.setText(formatterTime.format(newValue.getAttendance_dateTime()));
                    if(newValue.isAttendance_isAbsent()){
                        //absent
                        lblIsAbsent.setText("غائب");
                    }else{
                        if(newValue.isAttendance_isLate()){
                            //Late
                            lblIsAbsent.setText("متأخر");
                        }else {
                            //presint
                            lblIsAbsent.setText("حاظر");
                        }
                    }
                }
            }else {
                lblClassName.setText("");
                lblStudentsClassName.setText("");
                lblDate.setText("");
                lblTime.setText("");
                lblIsAbsent.setText("");
            }
        });


        boxSelectClass.setItems(obsBoxSelectClass);
        boxSelectClass.setCellFactory(param -> {
            return new ListCell<>(){
                @Override
                protected void updateItem(ClassesModel item, boolean empty) {
                    super.updateItem(item, empty);
                    if(textProperty().isBound()){
                        textProperty().unbind();
                    }
                    if(item != null && !empty){
                        textProperty().bind(item.class_nameProperty());
                    }else{
                        setText(null);
                        setGraphic(null);
                    }
                }
            };
        });
        boxSelectClass.setConverter(new StringConverter<ClassesModel>() {
            @Override
            public String toString(ClassesModel item) {
                if(item == null){
                    return null;
                }
                return item.getClass_name();
            }

            @Override
            public ClassesModel fromString(String string) {
                return null;
            }
        });
        boxSelectClass.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            filterr.filter_classesSelected(newValue);
            boxSelectSection.getSelectionModel().selectFirst();
            showList();
        });



        boxSelectSection.setItems(obsBoxSelectSection);
        boxSelectSection.setCellFactory(param -> {
            return new ListCell<>(){
                @Override
                protected void updateItem(SectionModel item, boolean empty) {
                    super.updateItem(item, empty);
                    if(textProperty().isBound()){
                        textProperty().unbind();
                    }
                    if(item != null && !empty){
                        textProperty().bind(item.section_nameProperty());
                    }else{
                        setText(null);
                        setGraphic(null);
                    }
                }
            };
        });
        boxSelectSection.setConverter(new StringConverter<SectionModel>() {
            @Override
            public String toString(SectionModel item) {
                if(item == null){
                    return null;
                }
                return item.getSection_name();
            }

            @Override
            public SectionModel fromString(String string) {
                return null;
            }
        });
        boxSelectSection.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            showList();
        });



    }

    public void showList(){
        data.deleteAllAttendanceModel();
        StudentsModel studentsModel = Inc.gui_controller.students.list.getSelectionModel().getSelectedItem();
        ClassesModel classesModel = boxSelectClass.getSelectionModel().getSelectedItem();
        SectionModel sectionModel = boxSelectSection.getSelectionModel().getSelectedItem();
        if(studentsModel != null && classesModel != null && sectionModel != null){
            data.addAllAttendanceModel(Inc.db.tables.attendance.select.selectAllViaStudents_idAndClasses_idAndSections_id(studentsModel.getStudents_id(),classesModel.getClass_id(),sectionModel.getSection_id()));
        }
    }

    public void init_after_classes_GUI_created(){
        boxSelectSection.getSelectionModel().selectFirst();
        Inc.gui_controller.students.list.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            showList();
        });
    }

    public class Filter{
        //TODO filter on students selected
        //filter_studentsSelected
        //TODO filter on classes selected
        //filter_classesSelected
        public void filter_classesSelected(ClassesModel classesModel){
            FilteredList<SectionModel> filteredData = new FilteredList<SectionModel>(obsBoxSelectSection, s -> true);
            boxSelectSection.setItems(filteredData);
            filteredData.setPredicate(data -> {
                if(classesModel != null){
                    return data.getClass_id() == classesModel.getClass_id();
                }else{
                    return false;
                }
            });
        }
        //TODO filter on sections selected
        //filter_sectionsSelected
    }

    public class Data{
        private ObservableList<ClassesModel> obsBoxSelectClass = null;

        private ObservableList<SectionModel> obsBoxSelectSection = null;

        private ObservableList<AttendanceModel> obsList = null;

        public Data(ObservableList<ClassesModel> obsBoxSelectClass, ObservableList<SectionModel> obsBoxSelectSection,ObservableList<AttendanceModel> obsList){
            this.obsBoxSelectClass = obsBoxSelectClass;

            this.obsBoxSelectSection = obsBoxSelectSection;

            this.obsList = obsList;
        }
        public void addClassesModel(ClassesModel model){
            this.obsBoxSelectClass.add(model);
            boxSelectClass.getSelectionModel().selectFirst();
        }
        public void deleteClassesModel(ClassesModel model){
            this.obsBoxSelectClass.remove(model);
            boxSelectClass.getSelectionModel().selectFirst();
        }

        public void addSectionModel(SectionModel model){
            this.obsBoxSelectSection.add(model);
        }
        public void deleteSectionModel(SectionModel model){
            this.obsBoxSelectSection.remove(model);
        }
        public void addAllAttendanceModel(ArrayList<AttendanceModel> arr){
            this.obsList.addAll(arr);
        }
        public void deleteAllAttendanceModel(){
            this.obsList.clear();

        }
    }
}
