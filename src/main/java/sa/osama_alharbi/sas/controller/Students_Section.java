package sa.osama_alharbi.sas.controller;

import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.util.StringConverter;
import sa.osama_alharbi.sas.inc.Inc;
import sa.osama_alharbi.sas.model.ClassesModel;
import sa.osama_alharbi.sas.model.SectionModel;
import sa.osama_alharbi.sas.model.StudentsModel;
import sa.osama_alharbi.sas.model.StudentsSectionModel;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ResourceBundle;

public class Students_Section implements Initializable {
    @FXML public ComboBox<ClassBoxItemModel> boxSelectClass;
    public ObservableList<ClassBoxItemModel> obsBoxSelectClass;

    @FXML public ListView<Students_Section_listItem> list;
    public ObservableList<Students_Section_listItem> obsList;
    public Data data;
    public Filter filter;

    @FXML public Label lblClassName,lblStudentsClassName,lblNumPresint,lblNumAbsent,lblNumTotalPresintAbsent,lblNumLate;

    @FXML public Button btnDeleteStudentsClass;

    private BooleanProperty isAddedToSection = null;

    public Students_Section(){
        obsBoxSelectClass = FXCollections.observableArrayList();
        obsList = FXCollections.observableArrayList();
        data = new Data(obsBoxSelectClass,obsList);

        isAddedToSection = new SimpleBooleanProperty(false);
        isAddedToSection.addListener((observable, oldValue, newValue) -> {
            if(newValue){
                btnDeleteStudentsClass.setText("حذف من الشعبة");
            }else{
                btnDeleteStudentsClass.setText("إضافة للشعبة");
            }
        });
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        filter = new Filter();
        list.setItems(obsList);
        list.setCellFactory(param -> {
            return new ListCell<>(){
                @Override
                protected void updateItem(Students_Section_listItem item, boolean empty) {
                    super.updateItem(item, empty);
                    if(item != null && !empty){
                        setGraphic(item.root);
                    }else{
                        setGraphic(null);
                    }
                }
            };
        });
        list.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            show(newValue,studentsModel);
        });


        boxSelectClass.setItems(obsBoxSelectClass);
        boxSelectClass.setCellFactory(param -> {
            return new ListCell<>(){
                @Override
                protected void updateItem(ClassBoxItemModel item, boolean empty) {
                    super.updateItem(item, empty);
                    if(item != null && !empty){
                        if(textProperty().isBound()){
                            textProperty().unbind();
                        }
                        textProperty().bind(item.title);
                    }else{
                        if(textProperty().isBound()){
                            textProperty().unbind();
                        }
                        setText(null);
                        setGraphic(null);
                    }
                }
            };
        });
        boxSelectClass.setConverter(new StringConverter<ClassBoxItemModel>() {
            @Override
            public String toString(ClassBoxItemModel item) {
                if(item == null){
                    return null;
                }
                return item.getTitle();
            }

            @Override
            public ClassBoxItemModel fromString(String string) {
                return null;
            }
        });
        boxSelectClass.getSelectionModel().select(data.allBoxModel);

        boxSelectClass.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue != null){
                if(newValue.isAll){
                    filter.filterSection_all();
                }else{
                    filter.filterSection(newValue);
                }
            }else{
                //newValue is null
                filter.filterSection_all();
            }
        });
    }

    private void show(Students_Section_listItem item,StudentsModel studentsModel){
        if(lblClassName.textProperty().isBound()){lblClassName.textProperty().unbind();}
        if(lblStudentsClassName.textProperty().isBound()){lblStudentsClassName.textProperty().unbind(); }

        if(item != null){
            if(studentsModel != null){
                lblClassName.textProperty().bind(item.getSectionModel().getClassesModel().class_nameProperty());
                lblStudentsClassName.textProperty().bind(item.getSectionModel().section_nameProperty());

                ArrayList<StudentsSectionModel> studentsSectionModelArrayList = Inc.model.studentsSection.get.getAllByStudents_idAndSection_id(studentsModel.getStudents_id(),item.getSectionModel().getSection_id());
                ArrayList<Integer> studentsSection_idsArr = new ArrayList<>();
                for (StudentsSectionModel i:studentsSectionModelArrayList) {
                    studentsSection_idsArr.add(i.getStudents_section_id());
                }

                int present = Inc.db.tables.attendance.select.selectCountIsPresentViaStudents_section_ids(studentsSection_idsArr);
                int late = Inc.db.tables.attendance.select.selectCountIsLateViaStudents_section_ids(studentsSection_idsArr);
                int absent = Inc.db.tables.attendance.select.selectCountIsAbsentViaStudents_section_ids(studentsSection_idsArr);
                int total = present + late + absent;


                lblNumPresint.setText(present+"");
                lblNumLate.setText(late+"");
                lblNumAbsent.setText(absent+"");
                lblNumTotalPresintAbsent.setText(total+"");


            }else {

                lblClassName.setText("");
                lblStudentsClassName.setText("");
                lblNumPresint.setText("");
                lblNumLate.setText("");
                lblNumAbsent.setText("");
                lblNumTotalPresintAbsent.setText("");
                btnDeleteStudentsClass.setText("");
                btnDeleteStudentsClass.setDisable(true);
            }

            isAddedToSection.set(item.isInSection());

            btnDeleteStudentsClass.setDisable(false);
        }else{

            lblClassName.setText("");
            lblStudentsClassName.setText("");
            lblNumPresint.setText("");
            lblNumLate.setText("");
            lblNumAbsent.setText("");
            lblNumTotalPresintAbsent.setText("");
            btnDeleteStudentsClass.setText("");
            btnDeleteStudentsClass.setDisable(true);
        }
    }

    private StudentsModel studentsModel = null;
    public void setStudentsModel(StudentsModel studentsModel){
        this.studentsModel = studentsModel;
        if(this.studentsModel != null){
            ArrayList<StudentsSectionModel> sectionModels = Inc.model.studentsSection.get.getAllByStudents_id(studentsModel.getStudents_id());
            HashMap<Integer,StudentsSectionModel> toHash = new HashMap<>();
            for (StudentsSectionModel i:sectionModels) {
                toHash.put(i.getSection_id(),i);
            }
            //TODO update listview
            for (Students_Section_listItem itemList:obsList) {
                if(toHash.containsKey(itemList.getSectionModel().getSection_id())){
                    itemList.setStudentsSectionModel(toHash.get(itemList.getSectionModel().getSection_id()));
                }else{
                    itemList.setStudentsSectionModel(null);
                }
            }

        }else{
//            filter.filterClasses(null);
            for (Students_Section_listItem itemList:obsList) {
                itemList.setStudentsSectionModel(null);
            }
        }
        show(list.getSelectionModel().selectedItemProperty().get(),studentsModel);
    }

    @FXML public void onClickDeleteStudentsClass(ActionEvent event) {
        if(this.studentsModel != null){
            Students_Section_listItem item = list.getSelectionModel().selectedItemProperty().get();
            if(item != null){
                if(isAddedToSection.get()){
                    if(Inc.action.studentsSection.delete.deleteStudentsSection(item.getStudentsSectionModel())){
                        item.setStudentsSectionModel(null);
                        isAddedToSection.set(false);
                    }
                }else{
                    StudentsSectionModel newModel = Inc.action.studentsSection.add.addNewStudentsSection(this.studentsModel,item.getSectionModel());
                    if(newModel != null){
                        item.setStudentsSectionModel(newModel);
                        isAddedToSection.set(true);
                    }
                }
            }
        }
    }



    public class Data{
        public ClassBoxItemModel allBoxModel = new ClassBoxItemModel(null,true);
        public ObservableList<ClassBoxItemModel> obsBoxSelectClass = null;

        public ObservableList<Students_Section_listItem> obsList = null;

        public Data(ObservableList<ClassBoxItemModel> obsBoxSelectClass,ObservableList<Students_Section_listItem> obsList){
            this.obsBoxSelectClass = obsBoxSelectClass;
            this.obsBoxSelectClass.add(allBoxModel);

            this.obsList = obsList;
        }
        public void addClassesModel(ClassesModel model){
            this.obsBoxSelectClass.add(new ClassBoxItemModel(model,false));
        }
        public void deleteClassesModel(ClassesModel model){
            ClassBoxItemModel delete = null;
            for (ClassBoxItemModel itm:obsBoxSelectClass) {
                if(!itm.isAll){
                    if(itm.classesModel.getClass_id() == model.getClass_id()){
                        delete = itm;
                        break;
                    }
                }
            }
            if(delete != null){
                this.obsBoxSelectClass.remove(delete);
            }
        }
        public void addSectionModel(SectionModel model){
            boolean isThereSectionModel = false;
            for (Students_Section_listItem itm: obsList) {
                if(itm.getSectionModel().getSection_id() == model.getSection_id()){
                    isThereSectionModel = true;
                }
            }

            if(!isThereSectionModel){
                FXMLLoader looder = Inc.gui.getLoader("Students_Section_listItem");
                HBox pan = (HBox) Inc.gui.getPain(looder);
                Students_Section_listItem cont = looder.<Students_Section_listItem>getController();
                cont.setSectionModel(model);

                this.obsList.add(cont);
            }
        }
        public void deleteSectionModel(SectionModel model){
            Students_Section_listItem itm = null;
            for (Students_Section_listItem i: obsList) {
                if (i.getSectionModel().getSection_id() == model.getSection_id()) {
                    itm = i;
                    break;
                }
            }

            if(itm != null){
                this.obsList.remove(itm);
            }
        }
    }

    public class Filter{
        public void filterSection_all(){
            list.setItems(obsList);
        }

        public void filterSection(ClassBoxItemModel classBoxItemModel){
            if (classBoxItemModel == null){
                filterSection_all();
            }else{
                FilteredList<Students_Section_listItem> filteredData = new FilteredList<Students_Section_listItem>(obsList, s -> true);
                list.setItems(filteredData);
                filteredData.setPredicate(data -> {
                    return data.getSectionModel().getClass_id() == classBoxItemModel.classesModel.getClass_id();
                });
            }
        }

//        public void filterClasses_all(){
//            boxSelectClass.setItems(obsBoxSelectClass);
//        }
//
//        public void filterClasses(ArrayList<Integer> arrClasses_ids){
//            if (arrClasses_ids == null){
//                filterClasses_all();
//            }else{
//                FilteredList<ClassBoxItemModel> filteredData = new FilteredList<ClassBoxItemModel>(obsBoxSelectClass, s -> true);
//                boxSelectClass.setItems(filteredData);
//                filteredData.setPredicate(data -> {
//                    if(data.isAll()){
//                        return true;
//                    }else{
//                        return arrClasses_ids.contains(data.classesModel.getClass_id());
//                    }
//                });
//                Platform.runLater(()->{
//                    boxSelectClass.getSelectionModel().select(data.allBoxModel);
//                });
//            }
//        }
    }

    protected class ClassBoxItemModel{
        private ClassesModel classesModel;
        private boolean isAll;
        private StringProperty title;
        public ClassBoxItemModel(ClassesModel classesModel, boolean isAll) {
            this.classesModel = classesModel;
            this.isAll = isAll;
            this.title = new SimpleStringProperty();
            if(isAll){
                this.title.setValue("الكل");
            }else{
                this.title.bind(classesModel.class_nameProperty());
            }
        }
        public ClassesModel getClassesModel() {
            return classesModel;
        }
        public void setClassesModel(ClassesModel classesModel) {
            this.classesModel = classesModel;
        }
        public boolean isAll() {
            return isAll;
        }
        public void setAll(boolean all) {
            isAll = all;
        }
        public String getTitle() {
            return title.get();
        }
        public StringProperty titleProperty() {
            return title;
        }
        public void setTitle(String title) {
            this.title.set(title);
        }
    }
}
