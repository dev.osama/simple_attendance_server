package sa.osama_alharbi.sas.controller;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import sa.osama_alharbi.sas.inc.Inc;
import sa.osama_alharbi.sas.model.SectionModel;
import sa.osama_alharbi.sas.model.StudentsModel;
import sa.osama_alharbi.sas.model.StudentsSectionModel;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class Section_Students implements Initializable {

    @FXML
    public ListView<StudentsSectionModel> list;
    public ObservableList<StudentsSectionModel> obsList;

    @FXML
    public Label lblClassName, lblStudentsClassesName, lblNumPresint, lblNumAbsent, lblNumTotalPresintAbsent, lblNumLate;

    @FXML
    public Button btnDeleteStudentsClass;
    public Data data = null;
    public Filter filter = null;

    public Section_Students() {
        obsList = FXCollections.observableArrayList();
        data = new Data(obsList);
        filter = new Filter();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        list.setItems(obsList);
        list.setCellFactory(param -> {
            return new ListCell<>() {
                @Override
                protected void updateItem(StudentsSectionModel item, boolean empty) {
                    super.updateItem(item, empty);
                    if (item != null && !empty) {
                        if (textProperty().isBound()) {
                            textProperty().unbind();
                        }
                        textProperty().bind(item.getStudentsModel().students_nameProperty());
                    } else {
                        if (textProperty().isBound()) {
                            textProperty().unbind();
                        }
                        setText(null);
                        setGraphic(null);
                    }
                }
            };
        });
        list.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            show(newValue);
        });
//        obsList.addAll("Osama","AAAAAA","bbbbbb");
    }

    private void show(StudentsSectionModel studentsSectionModel) {

        if (lblClassName.textProperty().isBound()) {
            lblClassName.textProperty().unbind();
        }
        if (lblStudentsClassesName.textProperty().isBound()) {
            lblStudentsClassesName.textProperty().unbind();
        }

        if (studentsSectionModel != null) {
            lblClassName.textProperty().bind(studentsSectionModel.getSectionModel().getClassesModel().class_nameProperty());
            lblStudentsClassesName.textProperty().bind(studentsSectionModel.getSectionModel().section_nameProperty());

            ArrayList<StudentsSectionModel> studentsSectionModelArrayList = Inc.model.studentsSection.get.getAllByStudents_idAndSection_id(studentsSectionModel.getStudents_id(), studentsSectionModel.getSection_id());
            ArrayList<Integer> studentsSection_idsArr = new ArrayList<>();
            for (StudentsSectionModel i : studentsSectionModelArrayList) {
                studentsSection_idsArr.add(i.getStudents_section_id());
            }

            int present = Inc.db.tables.attendance.select.selectCountIsPresentViaStudents_section_ids(studentsSection_idsArr);
            int late = Inc.db.tables.attendance.select.selectCountIsLateViaStudents_section_ids(studentsSection_idsArr);
            int absent = Inc.db.tables.attendance.select.selectCountIsAbsentViaStudents_section_ids(studentsSection_idsArr);
            int total = present + late + absent;


            lblNumPresint.setText(present + "");
            lblNumLate.setText(late + "");
            lblNumAbsent.setText(absent + "");
            lblNumTotalPresintAbsent.setText(total + "");
            return;
        } else {

            lblClassName.setText("");
            lblStudentsClassesName.setText("");
            lblNumPresint.setText("");
            lblNumLate.setText("");
            lblNumAbsent.setText("");
            lblNumTotalPresintAbsent.setText("");
            btnDeleteStudentsClass.setText("");
            btnDeleteStudentsClass.setDisable(true);
        }
    }

    @FXML
    public void onClickDelete(ActionEvent event) {

    }

    private SectionModel sectionModel = null;

    public void init_after_classes_GUI_created() {
        Inc.gui_controller.section.lst.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            sectionModel = newValue;
            filter.section_autoFilter(newValue);
        });
        filter.section_autoFilter(null);
    }

    public class Filter {

        public void section_autoFilter(SectionModel sectionModel) {
            if (sectionModel != null) {
                FilteredList<StudentsSectionModel> filteredData = new FilteredList<StudentsSectionModel>(obsList, s -> true);
                list.setItems(filteredData);
                filteredData.setPredicate(data -> {
                    return data.getSection_id() == sectionModel.getSection_id();
                });
            } else {
                FilteredList<StudentsSectionModel> filteredData = new FilteredList<StudentsSectionModel>(obsList, s -> true);
                list.setItems(filteredData);
                filteredData.setPredicate(data -> {
                    return false;
                });
            }
        }
    }

    public class Data {
        private ObservableList<StudentsSectionModel> obsList;

        public Data(ObservableList<StudentsSectionModel> obsList) {
            this.obsList = obsList;
        }

        public void addStudentsSectionModel(StudentsSectionModel model) {
            obsList.add(model);
        }

        public void deleteStudentsSectionModel(StudentsSectionModel model) {
            Platform.runLater(() -> obsList.remove(model));
        }
    }
}
